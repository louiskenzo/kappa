/*******************************************************************************
*                                                                              *
*                               Kyoto University                               *
*                               Okuno laboratory                               *
*                                    Kappa                                     *
*                                                                              *
*******************************************************************************/

// Standard headers
#include <vector>
// Project headers
#include "ppm_estimates.h"
#include "ppm.h"

namespace kappa {
	//_______________ Constructors
	template <class Point,
	          class Vector,
	          class FacetHandle>
	PPMEstimates<Point,Vector,FacetHandle>::
	PPMEstimates(const Point& source,
	             std::vector<Point>*  points,
	             unsigned int n_cols,
	             unsigned int n_rows) 
		: source(source),
		  points(points),
		  confidences(NULL),
		  intensities(NULL),
		  colors(NULL),
		  n_cols(n_cols),
		  n_rows(n_rows) {
			
	}
	
	template <class Point,
	          class Vector,
	          class FacetHandle>
	PPMEstimates<Point,Vector,FacetHandle>::
	PPMEstimates(const Point& source,
	             std::vector<Point>*  points,
	             std::vector<double>* confidences,
	             std::vector<double>* intensities,
	             unsigned int n_cols,
	             unsigned int n_rows) 
		: source(source),
		  points(points),
		  confidences(confidences),
		  intensities(intensities),
		  colors(NULL),
		  n_cols(n_cols),
		  n_rows(n_rows) {
			
	}
	
	template <class Point,
	          class Vector,
	          class FacetHandle>
	PPMEstimates<Point,Vector,FacetHandle>::
	PPMEstimates(const Point& source,
	             std::vector<Point>*        points,
	             std::vector<double>*       confidences,
	             std::vector<double>*       intensities,
	             std::vector<unsigned int>* colors,
	             unsigned int n_cols,
	             unsigned int n_rows) 
		: source(source),
		  points(points),
		  confidences(confidences),
		  intensities(intensities),
		  colors(colors),
		  n_cols(n_cols),
		  n_rows(n_rows) {
			
	}
	
	//_______________ Destructors
	template <class Point,
	          class Vector,
	          class FacetHandle>
	PPMEstimates<Point,Vector,FacetHandle>::
	~PPMEstimates() {
		if(points != NULL)      delete points;
		if(confidences != NULL) delete confidences;
		if(intensities != NULL) delete intensities;
	}

	//_______________ Public methods
	template <class Point,
	          class Vector,
	          class FacetHandle>
	bool PPMEstimates<Point,Vector,FacetHandle>::has_confidence() const {
		return confidences != NULL;
	}
	
	template <class Point,
	          class Vector,
	          class FacetHandle>
	bool PPMEstimates<Point,Vector,FacetHandle>::has_intensity() const {
		return intensities != NULL;
	}
	
	template <class Point,
	          class Vector,
	          class FacetHandle>
	bool PPMEstimates<Point,Vector,FacetHandle>::has_rgb() const {
		return colors != NULL;
	}
	
	template <class Point,
	          class Vector,
	          class FacetHandle>
	void PPMEstimates<Point,Vector,FacetHandle>::add_confidence_data(std::vector<double>* confidences) {
		confidences = confidences;
	}
	
	template <class Point,
	          class Vector,
	          class FacetHandle>
	void PPMEstimates<Point,Vector,FacetHandle>::add_intensity_data(std::vector<double>* intensities) {
		intensities = intensities;
	}
	
	template <class Point,
	          class Vector,
	          class FacetHandle>
	void PPMEstimates<Point,Vector,FacetHandle>::add_color_data(std::vector<unsigned int>* colors) {
		colors = colors;
	}
	
	// Explicit template instantiation
	template class PPMEstimates<PPM::Point,PPM::Vector,PPM::FacetHandle>;
}

/******************************************************************************/


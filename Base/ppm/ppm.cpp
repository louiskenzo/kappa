/*******************************************************************************
*                                                                              *
*                               Kyoto University                               *
*                               Okuno laboratory                               *
*                                    Kappa                                     *
*                                                                              *
*******************************************************************************/

// Standard headers
#include <limits>
#include <algorithm>
#include <sstream>
#include <cmath>
#include <climits>
#include <unistd.h>
// Boost headers
#include "boost/date_time/posix_time/posix_time.hpp"		
// CGAL headers
#include <CGAL/linear_least_squares_fitting_3.h>
#include <CGAL/squared_distance_3.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/Surface_mesh_simplification/HalfedgeGraph_Polyhedron_3.h>
#include <CGAL/Surface_mesh_simplification/edge_collapse.h>
#include <CGAL/Surface_mesh_simplification/Policies/Edge_collapse/Count_stop_predicate.h>
#include <CGAL/Surface_mesh_simplification/Policies/Edge_collapse/Midpoint_and_length.h> 
// Project headers
#include "log_forwarder.h"
#include "sound_source_localization_estimate.h"
#include "ppm.h"
#include "ppm_estimates.h"
#include "ppm_processing.h"

namespace SMS = CGAL::Surface_mesh_simplification;
using namespace boost::posix_time;

namespace kappa {
	//_______________ Constructors
	PPM::PPM() 
		: LogPublisher(Log::INFO),
		  LogForwarder(Log::INFO),
		  _mesh(),
		  _search_tree(),
		  _onChangeSubscribers(),
		  _onVertexCountChangeSubscribers(),
		  _onHalfedgeCountChangeSubscribers(),
		  _onFaceCountChangeSubscribers(),
		  _vertex_id_count(0),
		  _halfedge_id_count(0),
		  _face_id_count(0),
		  _processing_DS(NULL),
		  _mutex() {
		log("Setting up empty PPM");
		// Even if it's empty, we need to normalize so the data structure won't 
		// crash if it's required to iterate over border edges, as can be the 
		// case when visualizing border edges on initialization.
		_mesh.normalize_border();
		
		// Setting up mutex
		pthread_mutex_init(&_mutex, NULL);
	}
	
	//_______________ Destructors
	PPM::~PPM() {
		pthread_mutex_destroy(&_mutex);
	}
	
	//_______________ Public methods
	PPM::Processing* PPM::addEstimates(Estimates* estimates) {		
		std::stringstream entry;
		entry << "Adding estimates to PPM: " << estimates->n_cols << " by " << estimates->n_rows << " points";
		log(entry.str());
		
		// Acquire mutex for thread safety
		lock();
		ptime processing_start(microsec_clock::local_time());
		
		// Setup the processing data structure that will store all the 
		// information relevant to the processing phase
		if (_processing_DS != NULL) delete _processing_DS;
		_processing_DS = new Processing(estimates);
		
		// Classify each point depending on whether it intersects anything
		ptime ray_casting_start(microsec_clock::local_time());
		
		for (unsigned int i=0 ; i < _processing_DS->estimates->points->size() ; ++i) {
			//std::cerr << (*_processing_DS->estimates->points)[i] << std::endl;
			// Filter points that are undefined
			if (isnan((*_processing_DS->estimates->points)[i].z())) {
				_processing_DS->estimate_intersects_PPM.push_back(false);
				_processing_DS->estimate_idx_to_intersection_idx.push_back(0);
				_processing_DS->estimate_light_paths.push_back(Vector(0.0,0.0,0.0));
				_processing_DS->estimate_distances.push_back(std::numeric_limits<double>::signaling_NaN());
				
				if (maintain_statistics) _processing_DS->n_input_undefined++;
				
				continue;
			}
			
			// Set ray of light direction for estimate
			Ray light_path(_processing_DS->estimates->source, (*_processing_DS->estimates->points)[i]);
			Vector light_path_vector = light_path.to_vector();
			double estimate_distance = std::sqrt(light_path_vector.squared_length());
			_processing_DS->estimate_distances.push_back(estimate_distance);
			light_path_vector = (light_path_vector / estimate_distance);
			
			_processing_DS->estimate_light_paths.push_back(light_path_vector);
			
			if(phase_wrapping_intensity_filtering && 
			   _processing_DS->estimates->has_intensity()) {
				// Filter points that are too low in intensity; this is to
				// get rid of points too far away, that would cause phase 
				// wrapping.
				if ((*_processing_DS->estimates->intensities)[i] < phase_wrapping_intensity_threshold) {
					_processing_DS->estimate_intersects_PPM.push_back(false);
					_processing_DS->estimate_idx_to_intersection_idx.push_back(0);
					
					if (maintain_statistics) _processing_DS->n_filtered_intensity++;
					
					continue;
				}
			}
			
			if (!_search_tree.empty()) {
				// Prepare ray intersection query following the path of light to
				// the estimate
				boost::optional<ObjectAndPrimitiveID> ray_cast_result;
				PrimitiveID intersection_facet;
				Vector intersection_facet_normal;
				Point  intersection_point;
				double intersection_angle;
				
				// Perform the query using the AABB tree, and check if the result 
				// is a point
				ray_cast_result = _search_tree.any_intersection(light_path);
				if (ray_cast_result && 
					CGAL::assign(intersection_point, ray_cast_result->first)) 
				{
					intersection_facet = ray_cast_result->second;
					
					// Compute the normal of the intersected facet and angle of
					// incidence of the ray
					VertexHandle vertex_1 = intersection_facet->halfedge()->vertex();
					VertexHandle vertex_2 = intersection_facet->halfedge()->next()->vertex();
					VertexHandle vertex_3 = intersection_facet->halfedge()->next()->next()->vertex();
					Point point_1 = vertex_1->point();
					Point point_2 = vertex_2->point();
					Point point_3 = vertex_3->point();
					intersection_facet_normal = CGAL::unit_normal(point_1,point_2,point_3);
					
					intersection_angle = std::acos(std::max(std::min(intersection_facet_normal * light_path_vector,1.0),-1.0)) * 180 / CGAL_PI;
					
					// Store data in processing data structure
					_processing_DS->estimate_idx_to_intersection_idx.push_back(_processing_DS->intersection_points.size());
					_processing_DS->estimate_intersects_PPM.push_back(true);
					_processing_DS->intersection_idx_to_estimate_idx.push_back(i);
					_processing_DS->intersection_points.push_back(intersection_point);
					_processing_DS->intersection_facet_handles.push_back(intersection_facet);
					_processing_DS->intersection_facet_normals.push_back(intersection_facet_normal);
					_processing_DS->intersection_incidence_angles.push_back(intersection_angle);
					
					if (maintain_statistics) _processing_DS->n_input_intersect++;
				} else {
					// Store data in processing data structure
					_processing_DS->estimate_idx_to_intersection_idx.push_back(0);
					_processing_DS->estimate_intersects_PPM.push_back(false);
				}
			} else {
				_processing_DS->estimate_idx_to_intersection_idx.push_back(0);
				_processing_DS->estimate_intersects_PPM.push_back(false);
			}
		}
		logDuration(ray_casting_start, "Ray casting");
		
		// Triangulate estimates
		ptime triangulation_start(microsec_clock::local_time());
		if (triangulate_new_estimates) {
			PPMTriangulator<PPM> triangulator(this, &_mesh, *_processing_DS);
			forwardFrom(triangulator);
			_mesh.delegate(triangulator);
			stopForwardingFrom(triangulator);
		}
		logDuration(triangulation_start, "Triangulation");
		
		// Probabilistic update of vertices
		ptime vertex_updating_start(microsec_clock::local_time());
		if (update_vertices) {
			double average_error_strength = 0;
			// Integrate new estimates by propagating error in vertices
			for (unsigned int i=0 ; i < _processing_DS->intersection_points.size() ; ++i) {
				// For each intersection ray, find the vertex closest to the intersection
				double       closest_distance = std::numeric_limits<double>::infinity();
				VertexHandle closest_vertex;
				double       smallest_angle = std::numeric_limits<double>::infinity();
				VertexHandle smallest_angle_vertex;
				Vector       smallest_angle_vertex_ray;
				Vector       smallest_angle_vertex_unit_ray;
				double       smallest_angle_vertex_distance;
			
				const Point& intersection_point = _processing_DS->intersection_points[i];
				const Point& estimate_point     = (*_processing_DS->estimates->points)[_processing_DS->intersection_idx_to_estimate_idx[i]];
				const FacetHandle& intersected_facet = _processing_DS->intersection_facet_handles[i];
				Vector intersection_ray = intersection_point - _processing_DS->estimates->source;
				double intersection_ray_distance = std::sqrt(intersection_ray.squared_length());
				Vector unit_intersection_ray = intersection_ray / intersection_ray_distance;
			
				//double intersection_point_distance = sqrt(squared_distance(source,intersection_point));
				//double estimate_distance = std::sqrt(squared_distance(_processing_DS->estimates->source,estimate_point));
				double estimate_distance = _processing_DS->estimate_distances[_processing_DS->intersection_idx_to_estimate_idx[i]];
				double error_strength = estimate_distance-intersection_ray_distance;
				average_error_strength += fabs(error_strength);
			
				// Iterate over the vertices around the intersected facet
				PPM::HalfedgeAroundFacetCirculator halfedge = intersected_facet->facet_begin();
				do {
					VertexHandle vertex = halfedge->vertex();
					Point& vertex_point = vertex->point();
					
					double distance = sqrt(squared_distance(vertex->point(),intersection_point));
					if (distance < closest_distance) {
						closest_distance = distance;
						closest_vertex   = vertex;
					}
					
					Vector vertex_ray = vertex_point - _processing_DS->estimates->source;
					double vertex_ray_distance = std::sqrt(vertex_ray.squared_length());
					Vector unit_vertex_ray       = vertex_ray / vertex_ray_distance;
					double angle = std::acos(std::max(std::min(unit_vertex_ray * unit_intersection_ray,1.0),-1.0)) * 180 / CGAL_PI;
					if (angle < smallest_angle) {
						smallest_angle        = angle;
						smallest_angle_vertex = vertex;
						smallest_angle_vertex_distance = vertex_ray_distance;
						smallest_angle_vertex_ray      = vertex_ray;
						smallest_angle_vertex_unit_ray = unit_vertex_ray;
					}
				} while (++halfedge != intersected_facet->facet_begin());
			
				// If the distance to this vertex is sufficiently small, consider it a new estimate
				if (smallest_angle < tof_angular_resolution) {
					VertexHandle& adjusted_vertex = smallest_angle_vertex;
					double& adjusted_vertex_angle = smallest_angle;
					//Vector& adjusted_vertex_ray   = smallest_angle_vertex_ray;
					//double& adjusted_vertex_distance = smallest_angle_vertex_distance;
					Vector& adjusted_vertex_unit_ray = smallest_angle_vertex_unit_ray;
					
					// Compute the "error signal"
					double error_ratio    = adjusted_vertex_angle/tof_angular_resolution;
					double error_gain     = errorGainFunction(error_ratio);
					Vector error_vector   = error_strength * adjusted_vertex_unit_ray;
					
					double updated_effective_observation;
					if (!model_change_belief) {
						updated_effective_observation = adjusted_vertex->observations + error_gain;
					} else {
						// Compute the belief that our model is correct and the 
						// latest estimate comes from it (model hasn't changed)
						double model_belief;
						double model_belief_threshold = sensor_variance * zero_certainty;
						if (fabs(error_strength) <= model_belief_threshold) {
							// We believe the latest measurement is reasonably 
							// consistent with the current model.
							double linear_belief = 1 - (fabs(error_strength)/model_belief_threshold);
							model_belief = pow(linear_belief,1/2);
							//std::cerr << "Belief same:" << 100*model_belief << "%" << std::endl;
							assert(model_belief>=0 && model_belief<=1);
							
							updated_effective_observation = adjusted_vertex->observations + model_belief*error_gain;
						} else {
							// We suspect the latest measurement indicates the model
							// has changed.
							double linear_belief = zero_certainty + (fabs(error_strength)-model_belief_threshold)/zero_certainty;
							model_belief = 0.5 + 0.5 * pow(1/linear_belief,1);
							//std::cerr << "Belief changed:" << 100*model_belief << "%" << std::endl;
							assert(model_belief>=0 && model_belief<=1);
							
							// Divide the effective number of observation, ie. 
							// certainty in the model, to a fraction of its 
							// value
							updated_effective_observation = std::max(adjusted_vertex->observations * model_belief,1.0);
						}
					}
					
					double error_signal   = error_gain/updated_effective_observation;
					
					// Move the vertex by a fraction of the error signal
					Point current_position = adjusted_vertex->point();
					Point updated_position = current_position + error_signal * error_vector;
					adjusted_vertex->point()      = updated_position;
					adjusted_vertex->observations = updated_effective_observation;
				}
			}
			
			average_error_strength /= _processing_DS->intersection_points.size();
			std::stringstream entry;
			entry << "Average error strength: " << 1000*average_error_strength << " mm";
			log(entry.str(), kappa::Log::INFO);
		}
		logDuration(vertex_updating_start, "Vertex updating");
		
		consolidate();
		
		if (estimate_planarity) {
			ptime plane_fitting_start(microsec_clock::local_time());
			// Fit a plane with least squares to the vertices of the mesh
			Plane fitted_plane;
			double quality = linear_least_squares_fitting_3(_mesh.points_begin(),
				                                          _mesh.points_end(),
				                                          fitted_plane,
				                                          CGAL::Dimension_tag<0>());
			std::stringstream entry;
			entry << "Correlation with linear least squares fitted plane: " << quality;
			log(entry.str());
			logDuration(plane_fitting_start, "Linear least squares plane-fitting");
		}
		
		logDuration(processing_start, "Estimates processing");
		// Release mutex for thread safety
		unlock();
				
		if (maintain_statistics) {
			std::stringstream entry;
			entry << "Processed all points; there were: " 
			      << _processing_DS->n_input_undefined << " (" << 100*_processing_DS->n_input_undefined/_processing_DS->n_input() << "%) undefined points; " 
			      << _processing_DS->n_filtered_intensity << " (" << 100*_processing_DS->n_filtered_intensity/_processing_DS->n_input() << "%) filtered by intensity; " 
			      << _processing_DS->n_input_intersect  << " (" << 100*_processing_DS->n_input_intersect/_processing_DS->n_input() << "%) intersecting the PPM.";
			log(entry.str());
		}
		
		return _processing_DS;
	}
	
	void PPM::addSoundSourceLocalizationEstimate(SoundSourceLocalizationEstimate<Point>* estimate) {
		ptime sound_source_localization_estimate_integration_start(microsec_clock::local_time());
		
		for (VertexIterator vertex = vertexBegin() ; vertex!=vertexEnd() ; ++vertex) {
			// Update vertex' sum of log-likelihoods by adding newest 
			// log-likelihood
			vertex->sound_log_likelihood += (*estimate)(vertex->point());
		}
		
		logDuration(sound_source_localization_estimate_integration_start, "Sound source localization estimate integration");
	}
	
	void PPM::consolidate() {
		ptime consolidation_start(microsec_clock::local_time());
		
		// Eliminate small connected components
		if (prune_smaller_connected_components) {
			ptime connected_component_pruning_start(microsec_clock::local_time());
			_mesh.keep_largest_connected_components(8);
			logDuration(connected_component_pruning_start, "Smaller connected component pruning");
		}
		
		if (simplify_mesh) {
			ptime mesh_simplification_start(microsec_clock::local_time());
			
			unsigned int old_mesh_n_vertex = nVertex();
			// Run mesh simplification
			if (!_mesh.empty()) {
				int index = 0;
				for(HalfedgeIterator eb = _mesh.halfedges_begin(), ee = _mesh.halfedges_end(); 
				    eb != ee; 
				    ++eb) {
					eb->id() = index++;
				}

				index = 0;
				for(VertexIterator vb = _mesh.vertices_begin(), ve = _mesh.vertices_end(); 
				    vb != ve; 
				    ++vb) {
					vb->id() = index++;
				}
				
				SMS::Count_stop_predicate<Mesh> stop(100);
				MeshSimplificationVisitor simplification_visitor;
				SMS::edge_collapse(_mesh, 
				                   stop,
				                   CGAL::get_placement(SMS::Midpoint_placement<Mesh>())
				                        .visitor(simplification_visitor));
			}
			
			logDuration(mesh_simplification_start, "Mesh simplification");
			std::stringstream entry;
			entry << "Simplified mesh from " << old_mesh_n_vertex << " to " << nVertex() << " vertices";
			log(entry.str());
		}
		
		if (batch_compute_normals) {
			ptime normal_computation_start(microsec_clock::local_time());
			computeNormals();
			logDuration(normal_computation_start, "Normals computation");
		}
		
		// Compute intersection-finding spatial tree
		if (!_mesh.empty()) {
			ptime AABB_tree_building_start(microsec_clock::local_time());
			_search_tree.rebuild(_mesh.facets_begin(),_mesh.facets_end());
			logDuration(AABB_tree_building_start, "AABB tree building");
			
			ptime kd_tree_building_start(microsec_clock::local_time());
			_search_tree.accelerate_distance_queries();
			logDuration(kd_tree_building_start, "Kd tree building");
		}
		
		if (!_mesh.empty()) {
			ptime mesh_normalization_start(microsec_clock::local_time());
			_mesh.normalize_border();
			logDuration(mesh_normalization_start, "Mesh normalization");
		}
		
		logDuration(consolidation_start, "Consolidation");
		// Notify subscribers
		notifyChanges();
	}
	
	unsigned int PPM::nVertex() const {
		return _mesh.size_of_vertices();
	}
	
	unsigned int PPM::nFace() const {
		return _mesh.size_of_facets();
	}
	
	unsigned int PPM::nHalfedge() const {
		return _mesh.size_of_halfedges();
	}
	
	PPM::VertexIterator PPM::vertexBegin() {
		return _mesh.vertices_begin();
	}
	
	PPM::VertexIterator PPM::vertexEnd() {
		return _mesh.vertices_end();
	}
	
	PPM::EdgeIterator PPM::edgeBegin() {
		return _mesh.edges_begin();
	}
	
	PPM::EdgeIterator PPM::borderEdgeBegin() {
		return _mesh.border_edges_begin();
	}
	
	PPM::EdgeIterator PPM::edgeEnd() {
		return _mesh.edges_end();
	}
	
	PPM::FacetIterator PPM::facetBegin() {
		return _mesh.facets_begin();
	}
	
	PPM::FacetIterator PPM::facetEnd() {
		return _mesh.facets_end();
	}
	
	void PPM::subscribeOnChange(PPMAdapter* object) {
		_onChangeSubscribers.push_back(object);
	}
	
	void PPM::subscribeOnVertexCountChange(PPMAdapter* object) {
		_onVertexCountChangeSubscribers.push_back(object);
	}
	
	void PPM::subscribeOnHalfedgeCountChange(PPMAdapter* object) {
		_onHalfedgeCountChangeSubscribers.push_back(object);
	}
	
	void PPM::subscribeOnFaceCountChange(PPMAdapter* object) {
		_onFaceCountChangeSubscribers.push_back(object);
	}
	
	void PPM::clear() {
		// Acquire mutex for thread safety
		lock();
		ptime clearing_start(microsec_clock::local_time());
		
		_search_tree.clear();
		_mesh.clear();
		_mesh.normalize_border();
		notifyChanges();
		
		logDuration(clearing_start, "Clearing");
		// Release mutex for thread safety
		unlock();
	}
	
	boost::tuple<std::pair<double,double>,
			     std::pair<double,double>,
			     std::pair<double,double> > PPM::boundingBox() const {
		if (_search_tree.empty()) {
			return boost::make_tuple(std::make_pair(-5.0, 5.0),
				                     std::make_pair(-5.0, 5.0),
				                     std::make_pair(-5.0, 5.0));
		} else {
			CGAL::Bbox_3 bounding_box = _search_tree.bbox();
			return boost::make_tuple(std::make_pair(bounding_box.xmin(), bounding_box.xmax()),
				                     std::make_pair(bounding_box.ymin(), bounding_box.ymax()),
				                     std::make_pair(bounding_box.zmin(), bounding_box.zmax()));
		}
	}
	
	void PPM::lock() {
		// TODO Fix thread problem with drawImplementation, prevents clear signal from working
		//pthread_mutex_lock(&_mutex);
	}
	
	void PPM::unlock() {
		// TODO Fix thread problem with drawImplementation, prevents clear signal from working
		//pthread_mutex_unlock(&_mutex);
	}
	/*
	unsigned int PPM::generateVertexID() {
		return _vertex_id_count++;
	}
	
	unsigned int PPM::generateHalfedgeID() {
		return _halfedge_id_count++;
	}
	
	unsigned int PPM::generateFaceID() {
		return _vertex_id_count++;
	}
	*/
	
	PPM::Processing* PPM::processingDS() {
		return _processing_DS;
	}
	
	//_______________ Helpers
	void PPM::onChange() const {
		for (unsigned int i=0 ; i<_onChangeSubscribers.size() ; ++i) {
			_onChangeSubscribers[i]->onPPMChange();
		}
	}
	
	void PPM::onVertexCountChange() const {
		unsigned int vertex_count = nVertex();
		for (unsigned int i=0 ; i<_onVertexCountChangeSubscribers.size() ; ++i) {
			_onVertexCountChangeSubscribers[i]->onPPMVertexCountChange(vertex_count);
		}
	}
	
	void PPM::onHalfedgeCountChange() const {
		unsigned int halfedge_count = nHalfedge();
		for (unsigned int i=0 ; i<_onHalfedgeCountChangeSubscribers.size() ; ++i) {
			_onHalfedgeCountChangeSubscribers[i]->onPPMHalfedgeCountChange(halfedge_count);
		}
	}
	
	void PPM::onFaceCountChange() const {
		unsigned int face_count = nFace();
		for (unsigned int i=0 ; i<_onFaceCountChangeSubscribers.size() ; ++i) {
			_onFaceCountChangeSubscribers[i]->onPPMFaceCountChange(face_count);
		}
	}
	
	void PPM::notifyChanges() const {
		onChange();
		onVertexCountChange();
		onHalfedgeCountChange();
		onFaceCountChange();
	}
	
	double PPM::errorGainFunction(const double& error_ratio) const {
		double clipped_ratio = std::min(std::max(error_ratio,0.0),1.0);
		
		return pow((1-clipped_ratio),2);
	}
	
	void PPM::computeNormals() {
		ptime normals_computation_start(microsec_clock::local_time());
		
		// Iterate over all faces and set their normals
		ptime face_normals_computation_start(microsec_clock::local_time());
		for (FacetIterator facet = facetBegin() ; facet!=facetEnd() ; ++facet) {
			HalfedgeAroundFacetCirculator he = facet->facet_begin();
			const Point& p1 = he->vertex()->point();
			const Point& p2 = he->next()->vertex()->point();
			const Point& p3 = he->next()->next()->vertex()->point();
			
			facet->normal = CGAL::unit_normal(p1,p2,p3);
		}
		logDuration(face_normals_computation_start, "Face normals computation");
		
		// Iterate over all vertices and set their normals
		ptime vertex_normals_computation_start(microsec_clock::local_time());
		for (VertexIterator vertex = vertexBegin() ; vertex!=vertexEnd() ; ++vertex) {
			Vector vertex_normal(0.0,0.0,0.0);
			HalfedgeAroundVertexCirculator he = vertex->vertex_begin();
			
			StorageType n_facets = 0;
			do {
				if (he->facet()!=0) {
					vertex_normal = vertex_normal + he->facet()->normal;
					n_facets++;
				}
			} while(++he != vertex->vertex_begin());
			
			if (n_facets > 0) {
				vertex->normal = vertex_normal / n_facets;
			} else {
				vertex->normal = Vector(0.0,0.0,1.0);
			}
		}
		logDuration(vertex_normals_computation_start, "Vertex normals computation");
		
		logDuration(normals_computation_start, "Normals computation");
	}
}

/******************************************************************************/


/*******************************************************************************
*                                                                              *
*                               Kyoto University                               *
*                               Okuno laboratory                               *
*                                    Kappa                                     *
*                                                                              *
*******************************************************************************/

#ifndef KAPPA_PPM_H
#define KAPPA_PPM_H

// Standard headers
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <cmath>
#include <functional>
#include <pthread.h>
// Boost headers
#include "boost/tuple/tuple.hpp"
#include "boost/date_time/posix_time/posix_time.hpp"
// CGAL headers
#include <CGAL/AABB_tree.h> // Must be included before the kernel header
#include <CGAL/AABB_traits.h>
#include <CGAL/AABB_polyhedron_triangle_primitive.h>
#include <CGAL/Simple_cartesian.h> // Kernel header
#include <CGAL/Polyhedron_3.h>
#include <CGAL/HalfedgeDS_vertex_base.h>
#include <CGAL/HalfedgeDS_face_base.h>
#include <CGAL/Surface_mesh_simplification/HalfedgeGraph_Polyhedron_3.h>
#include <CGAL/Surface_mesh_simplification/edge_collapse.h>
// Project headers
#include "log_forwarder.h"
#include "sound_source_localization_estimate.h"
#include "ppm_visualization.h"
#include "ppm_adapter.h"
#include "ppm_triangulator.h"
#include "ppm_estimates.h"
#include "ppm_processing.h"

using namespace boost::posix_time;
namespace SMS = CGAL::Surface_mesh_simplification;

namespace kappa {
	// Forward declarations
	class PPMVisualization;
	template <class HDS> class PPMTriangulator;
	
	class PPM : public LogForwarder {
		public:
			//_______________ Types
			// CGAL computation kernel
			typedef double                              StorageType;
			typedef CGAL::Simple_cartesian<StorageType> Kernel;
			typedef CGAL::Point_3<Kernel>               Point;
			typedef CGAL::Ray_3<Kernel>                 Ray;
			typedef CGAL::Vector_3<Kernel>              Vector;
			typedef CGAL::Plane_3<Kernel>               Plane;
			
			// Custom vertex and face payload
			template <class Refs, class Traits>
			struct PPMVertex : public CGAL::HalfedgeDS_vertex_base<Refs,CGAL::Tag_true,typename Traits::Point_3> {
				typedef CGAL::HalfedgeDS_vertex_base<Refs,CGAL::Tag_true,typename Traits::Point_3> Base;
				typedef typename Traits::Point_3 Point;
				PPMVertex() 
					: ID(-1),observations(1),normal(),sound_log_likelihood(0) {}
				PPMVertex(const Point& point) 
					: Base(point),ID(-1),observations(1),normal(),sound_log_likelihood(0) {}
				PPMVertex(const Point& point, double observations) 
					: Base(point),ID(-1),observations(observations),normal(),sound_log_likelihood(0) {}
				
				Vector compute_normal() const {
					return Vector();
				}
				
				int& id() {return ID;}
				const int& id() const {return ID;}
				
				int    ID;
				double observations;
				Vector normal;
				double sound_log_likelihood;
			};
			
			template <class Refs, class Traits>
			struct PPMHalfedge : public CGAL::HalfedgeDS_halfedge_base<Refs,CGAL::Tag_true,CGAL::Tag_true,CGAL::Tag_true> {
				typedef CGAL::HalfedgeDS_halfedge_base<Refs,CGAL::Tag_true,CGAL::Tag_true,CGAL::Tag_true> Base;
				typedef typename Base::Base_base Base_base;
				
				PPMHalfedge() 
					: ID(-1) {}
				
				int& id() {return ID;}
				const int& id() const {return ID;}
				
				int ID;
			};
			
			template <class Refs, class Traits>
			struct PPMFace : public CGAL::HalfedgeDS_face_base<Refs> {
				PPMFace() : ID(-1), normal() {}
				PPMFace(Vector normal) : ID(-1), normal(normal) {}
				
				int& id() {return ID;}
				const int& id() const {return ID;}
				
				int ID;
				Vector       normal;
			};
			
			struct PPMPayload : public CGAL::Polyhedron_items_3 {
				template <class Refs, class Traits>
				struct Vertex_wrapper {
					typedef PPMVertex<Refs,Traits> Vertex;
				};
				
				template < class Refs, class Traits>
				struct Halfedge_wrapper {
					typedef PPMHalfedge<Refs,Traits> Halfedge;
				};
				
				template <class Refs, class Traits>
				struct Face_wrapper {
					typedef typename Traits::Plane_3 Plane;
					typedef PPMFace<Refs, Traits> Face;
				};
			};
			
			// Mesh types
			typedef CGAL::Polyhedron_3<Kernel,PPMPayload> Mesh;
			typedef Mesh::HalfedgeDS        HalfedgeDS;
			typedef Mesh::Facet_iterator    FacetIterator;
			typedef Mesh::Edge_iterator     EdgeIterator;
			typedef Mesh::Halfedge_iterator HalfedgeIterator;
			typedef Mesh::Vertex_iterator   VertexIterator;
			typedef Mesh::Point_iterator    PointIterator;
			typedef Mesh::Facet_handle      FacetHandle;
			typedef Mesh::Halfedge_handle   HalfedgeHandle;
			typedef Mesh::Vertex_handle     VertexHandle;
			typedef Mesh::Halfedge_around_facet_circulator  HalfedgeAroundFacetCirculator;
			typedef Mesh::Halfedge_around_vertex_circulator HalfedgeAroundVertexCirculator;
			typedef CGAL::Segment_3<Kernel> Segment;
			
			// AABB tree types
			typedef CGAL::AABB_polyhedron_triangle_primitive<Kernel,Mesh> AABBPrimitive;
			typedef CGAL::AABB_traits<Kernel, AABBPrimitive>              AABBTraits;
			typedef CGAL::AABB_tree<AABBTraits>       AABBTree;
			typedef AABBTree::Object_and_primitive_id ObjectAndPrimitiveID;
			typedef AABBTree::Point_and_primitive_id  PointAndPrimitiveID;
			typedef AABBTree::Primitive_id            PrimitiveID;
			
			// Auxiliary structures
			typedef PPMEstimates<Point,
			                     Vector,
			                     FacetHandle>  Estimates;
			typedef PPMProcessing<Point,
			                      Vector,
			                      FacetHandle,
			                      VertexHandle> Processing;
			
			// Mesh simplification
			typedef SMS::Edge_profile<Mesh> Profile;
			
			//_______________ Constants
			//Processing options
			const static bool triangulate_new_estimates = true;
			const static bool seam_new_estimates        = false;
			const static bool update_vertices           = true;
			const static bool simplify_mesh             = false;
			const static bool batch_compute_normals     = true;
			const static bool prune_smaller_connected_components = true;
			const static bool model_change_belief       = true;
			const static bool estimate_planarity        = false;
			// Filtering
			const static bool facet_filtering           = true;
			const static bool facet_clipping            = false;
			const static bool confidence_filtering      = true;
			const static bool phase_wrapping_intensity_filtering = true;
			const static bool incidence_filtering       = false;
			const static bool continuity_filtering      = false;
			const static bool shape_criteria_filtering  = false;
			
			const static bool maintain_statistics = true;
			
			// Processing parameters
			/// Maximum ratio (dmax-dmin)/dmax of 2 points' distances from the 
			/// camera to consider them part of the same surface
			const static double continuity_threshold = 0.5;
			/// Minimum SwissRanger confidence rate acceptable
			const static double confidence_threshold = 30000; 
			const static double incidence_threshold  = 75;
			/// Minimum distance for a point to be considered not noise
			const static double clipping_distance      = 0.2;
			const static double tof_angular_resolution = 0.23;
			const static double phase_wrapping_intensity_threshold = 1000;
			const static double sensor_variance        = 0.045; // 1 sigma = 45 mm
			// Multiple of sigma at after which estimates have a negative 
			// multiplicative effect, reducing certainty in the current model
			const static double zero_certainty = 2;
			/// Maximum ratio between shortest and longest edge of a triangle
			const static double shape_criteria_triangle_edge_ratio = 2;
			/// Minimum angle of the angles of a triangle
			const static double shape_criteria_triangle_minimum_angle = 15;
		
			//_______________ Constructors
			PPM();
		
			//_______________ Destructors
			~PPM();
		
			//_______________ Public methods
			Processing* addEstimates(Estimates* estimates);
			void addSoundSourceLocalizationEstimate(SoundSourceLocalizationEstimate<Point>* estimate);
			void consolidate();
			unsigned int nVertex() const;
			unsigned int nFace() const;
			unsigned int nHalfedge() const;
			VertexIterator vertexBegin();
			VertexIterator vertexEnd();
			EdgeIterator   edgeBegin();
			EdgeIterator   borderEdgeBegin();
			EdgeIterator   edgeEnd();
			FacetIterator  facetBegin();
			FacetIterator  facetEnd();
			void subscribeOnChange(PPMAdapter* object);
			void subscribeOnVertexCountChange(PPMAdapter* object);
			void subscribeOnHalfedgeCountChange(PPMAdapter* object);
			void subscribeOnFaceCountChange(PPMAdapter* object);
			void clear();
			boost::tuple<std::pair<double,double>,
			             std::pair<double,double>,
			             std::pair<double,double> > boundingBox() const;
			void lock();
			void unlock();
			inline unsigned int generateVertexID()   {return _vertex_id_count++;};
			inline unsigned int generateHalfedgeID() {return _halfedge_id_count++;};
			inline unsigned int generateFaceID()     {return _face_id_count++;};
			Processing* processingDS();
		
		private:
			//_______________ Helpers
			void onChange() const;
			void onVertexCountChange() const;
			void onHalfedgeCountChange() const;
			void onFaceCountChange() const;
			void notifyChanges() const;
			double errorGainFunction(const double& error_ratio) const;
			void computeNormals();
			
			//_______________ Fields
			Mesh     _mesh;        ///< The geometric mesh underlying the ppm.
			AABBTree _search_tree; ///< Spatial search structure to find intersections and closest points quickly.
			std::vector<PPMAdapter*> _onChangeSubscribers;
			std::vector<PPMAdapter*> _onVertexCountChangeSubscribers;
			std::vector<PPMAdapter*> _onHalfedgeCountChangeSubscribers;
			std::vector<PPMAdapter*> _onFaceCountChangeSubscribers;
			
			unsigned int _vertex_id_count;
			unsigned int _halfedge_id_count;
			unsigned int _face_id_count;
			
			Processing* _processing_DS;
			
			// Thread safety
			pthread_mutex_t _mutex;
	};
	
	// Custom mesh simplification visitor
	class MeshSimplificationVisitor {
		public:
			MeshSimplificationVisitor() 
				: collected(0)
				, processed(0)
				, collapsed(0)
				, non_collapsable(0)
				, cost_uncomputable(0) 
				, placement_uncomputable(0) {
				
			}
		
			// Called on algorithm entry  
			void OnStarted(PPM::Mesh& mesh) {
			
			}
		
			// Called on algorithm exit  
			void OnFinished(PPM::Mesh& mesh) {
				std::cerr << "\n" << std::flush;
			}
			  
			// Called when the stop condition returned true
			void OnStopConditionReached(PPM::Profile const& profile) {
			
			}
		
			// Called during the collecting phase for each edge collected.
			void OnCollected(PPM::Profile const&            profile, 
			                 boost::optional<double> const& ) {
				++ collected ;
				std::cerr << "\rEdges collected: " << collected << std::flush;
			}
		
			// Called during the processing phase for each edge selected.
			// If cost is absent the edge won't be collapsed.
			void OnSelected(PPM::Profile const&     profile, 
				            boost::optional<double> cost, 
				            std::size_t initial, 
				            std::size_t current) {
				++ processed;
				if ( !cost ) {
					++ cost_uncomputable;
				}
			
				if (current == initial) {
					std::cerr << "\n" << std::flush;
				}
				std::cerr << "\r" << current << std::flush ;
			}
			
			// Called during the processing phase for each edge being collapsed.
			// If placement is absent the edge is left uncollapsed.
			void OnCollapsing(PPM::Profile const&    profile,
			                  boost::optional<PPM::Point> placement) {
				if (placement) {
					++collapsed;
				} else {
					++placement_uncomputable;
				}
			}
			
			// Called for each edge which failed the so called link-condition,
			// that is, which cannot be collapsed because doing so would
			// turn the surface into a non-manifold.
			void OnNonCollapsable(PPM::Profile const& profile)	{
				++non_collapsable;
			}
			
			///  
			void OnCollapsed(PPM::Profile const& profile, PPM::VertexHandle hv) {
				++collapsed;
			}          
			
		private:
			std::size_t collected;
			std::size_t processed;
			std::size_t collapsed;
			std::size_t non_collapsable;
			std::size_t cost_uncomputable;
			std::size_t placement_uncomputable;
	};
}

#endif

/******************************************************************************/


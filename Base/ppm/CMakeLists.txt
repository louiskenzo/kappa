################################################################################
#                                                                              #
#                               Kyoto University                               #
#                               Okuno laboratory                               #
#                                Boomer project                                #
#                                                                              #
################################################################################

# Add include and library directories for Boost
find_package(Boost 1.51.0 COMPONENTS date_time REQUIRED)
set(Boost_USE_MULTITHREADED ON)
include_directories(${Boost_INCLUDE_DIRS})
link_directories(${Boost_LIBRARY_DIRS})
# Add include and library directories for CGAL
find_package(CGAL 4.1 COMPONENTS Core REQUIRED)
include_directories(${CGAL_INCLUDE_DIRS})
link_directories(${CGAL_LIBRARY_DIRS})
# Add include and library directories for GTKmm
find_package(PkgConfig)
pkg_check_modules(GTKMM gtkmm-2.4)
include_directories(${GTKMM_INCLUDE_DIRS})
link_directories(${GTKMM_LIBRARY_DIRS})
# Add include and library directories for osgGtk
find_package(osgGtk REQUIRED)
include_directories(${osgGtk_INCLUDE_DIRS})
link_directories(${osgGtk_LIBRARY_DIR})
# Call the Threads finder, and include the header and library directories found
find_package(Threads REQUIRED)
include_directories(${Threads_INCLUDE_DIRS})
link_directories(${Threads_LIBRARY_DIRS})
# Add include and library directories for OpenSceneGraph
find_package(OpenSceneGraph REQUIRED)
include_directories(${OpenSceneGraph_INCLUDE_DIRS})
link_directories(${OpenSceneGraph_LIBRARY_DIR})

# Include project headers
include_directories(${PROJECT_SOURCE_DIR}/visualization)
include_directories(${PROJECT_SOURCE_DIR}/tools)
include_directories(${PROJECT_SOURCE_DIR}/audition)

# Compile to a shared library
add_library(kappa-ppm SHARED ppm.cpp 
                             ppm_visualization.cpp 
                             ppm_adapter.cpp 
                             ppm_triangulator.cpp 
                             ppm_estimates.cpp 
                             ppm_processing.cpp)
target_link_libraries(kappa-ppm kappa-audition 
                                osgGtkmm
                                pthread
                                ${Boost_LIBRARIES}
                                ${CGAL_LIBRARY}
                                ${OpenSceneGraph_LIBRARIES})
#add_executable(ppm-tests ppm_tests.cpp)
#target_link_libraries(ppm-tests kappa-ppm kappa-command-center)

# Deploy library
install(TARGETS kappa-ppm
        LIBRARY DESTINATION ${INSTALLATION_LIB_DIR})
# Deploy headers
install(FILES ppm.h 
              ppm_visualization.h 
              ppm_adapter.h 
              ppm_triangulator.h 
              ppm_estimates.h 
              ppm_processing.h 
        DESTINATION ${INSTALLATION_INC_DIR})

################################################################################


/*******************************************************************************
*                                                                              *
*                               Kyoto University                               *
*                               Okuno laboratory                               *
*                                    Kappa                                     *
*                                                                              *
*******************************************************************************/

// Standard headers
#include <algorithm>
#include <iostream>
#include <utility>
#include <unistd.h>
// Boost headers
#include "boost/tuple/tuple.hpp"
#include "boost/tuple/tuple_io.hpp"
// OpenSceneGraph headers
#include <osg/CopyOp>
#include <osg/Object>
#define GL_GLEXT_PROTOTYPES 1
#include <osg/BoundingBox>
#include <osg/GL>
#include <osg/GLExtensions>
// Project headers
#include "log_publisher.h"
#include "ppm.h"
#include "ppm_estimates.h"
#include "ppm_processing.h"
#include "ppm_visualization.h"

namespace kappa {
	//_______________ Constants
	float PPMVisualization::_vertex_color_constant[4] = {0.0, 0.4, 0.0, 0.8};
	float PPMVisualization::_vertex_color_low[4]      = {1.0, 0.0, 0.0, 0.8};
	float PPMVisualization::_vertex_color_high[4]     = {0.0, 1.0, 0.0, 0.8};
	
	//_______________ Constructors
	PPMVisualization::PPMVisualization(PPM* ppm) 
		: Visualization(),
		  LogPublisher(Log::DEBUG),
		  _ppm(ppm),
		  _show_ppm(true),
		  _show_ppm_vertices(true),
		  _show_ppm_edges(true),
		  _show_ppm_faces(true),
		  _differentiate_border_edges(false),
		  _dynamic_vertex_color(true),
		  _dynamic_vertex_color_property(SoundsourceLikelihood),
		  _dynamic_vertex_color_min(0),
		  _dynamic_vertex_color_max(10),
		  _dynamic_vertex_color_transparency(true),
		  _show_latest_estimates(false),
		  _show_latest_estimates_vertices(true),
		  _show_latest_estimates_edges(false),
		  _show_latest_estimates_faces(false) {
		ppm->subscribeOnChange(this);
		publishTo(ppm);
	}
	
	PPMVisualization::PPMVisualization(const PPMVisualization& viz,
	                                   const osg::CopyOp&      copyop) 
		: Visualization(*this,copyop),
		  LogPublisher(*this),
		  _ppm(viz._ppm),
		  _show_ppm(viz._show_ppm),
		  _show_ppm_vertices(viz._show_ppm_vertices),
		  _show_ppm_edges(viz._show_ppm_edges),
		  _show_ppm_faces(viz._show_ppm_faces),
		  _differentiate_border_edges(viz._differentiate_border_edges),
		  _dynamic_vertex_color(viz._dynamic_vertex_color),
		  _show_latest_estimates(viz._show_latest_estimates),
		  _show_latest_estimates_vertices(viz._show_latest_estimates_vertices),
		  _show_latest_estimates_edges(viz._show_latest_estimates_edges),
		  _show_latest_estimates_faces(viz._show_latest_estimates_faces) {
		
	}
	
	//_______________ Destructors
	PPMVisualization::~PPMVisualization() {
		// Do not delete the ppm because it doesn't belong to the visualization
	}
	
	//_______________ Public methods
	void PPMVisualization::drawImplementation(osg::RenderInfo& renderInfo) const {
		// Acquire mutex for thread safety
		_ppm->lock();
		ptime openGL_generation_start(microsec_clock::local_time());
		
		// Fog
		glEnable(GL_FOG);
		glFogi(GL_FOG_MODE, GL_EXP);
		// Exponential fog
		glFogf(GL_FOG_DENSITY, 0.01);
		// Linear fog
		glFogf(GL_FOG_START, 10);
		glFogf(GL_FOG_END, 150);
		// Fog color
		float fog_color[3]={0.2,0.2,0.4};
		glFogfv(GL_FOG_COLOR, fog_color);
		
		// Alpha blending/transparency
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		
		if (_show_ppm) {
			ptime ppm_openGL_generation_start(microsec_clock::local_time());
			
			// Draw facets
			if (_show_ppm_faces) {
				ptime facets_openGL_generation_start(microsec_clock::local_time());
				
				glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
				glShadeModel(GL_FLAT);
				
				// Avoid z-fighting between faces and edges if both are to be 
				// rendered
				if (_show_ppm_edges) {
					glEnable(GL_POLYGON_OFFSET_FILL);
					glPolygonOffset(1.0, 1.0);
				}
				
				glBegin(GL_TRIANGLES);
				glColor4f(0.5, 0.5, 0.5, 0.8);
				PPM::FacetIterator facet;
				// Iterate over all facets, drawing it each time
				for (facet=_ppm->facetBegin() ; facet!=_ppm->facetEnd() ; ++facet) {
					PPM::HalfedgeAroundFacetCirculator halfedge = facet->facet_begin();
				
					// Iterate over all the vertices around the facet to issue 
					// vertices and normals; all should be triangular 
					// (facet->degree()==0)
					do {
						const PPM::VertexHandle& v      = halfedge->vertex();
						const PPM::Point&        p      = v->point();
						const PPM::Vector&       normal = v->normal;
						glNormal3d(normal.x(), normal.y(), normal.z());
						glVertex3d(p.x(),      p.y(),      p.z());
					} while (++halfedge != facet->facet_begin());
				}
				glEnd();
				
				logDuration(facets_openGL_generation_start, "OpenGL facets generation");
			}
			
			glDisable(GL_LIGHTING);
			
			// Draw edges
			if (_show_ppm_edges) {
				ptime edges_openGL_generation_start(microsec_clock::local_time());
				
				glLineWidth(0.5);
				glEnable(GL_LINE_SMOOTH);
				glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
				glColor4f(0.0, 0.0, 0.0, 1.0);
				
				glBegin(GL_LINES);
				
				// Iterate over all non-border edges first
				PPM::EdgeIterator edge;
				for (edge=_ppm->edgeBegin() ; edge!=_ppm->borderEdgeBegin() ; ++edge) {
					const PPM::Point& beg = edge->opposite()->vertex()->point();
					const PPM::Point& end = edge->vertex()->point();
					glVertex3d(beg.x(), beg.y(), beg.z());
					glVertex3d(end.x(), end.y(), end.z());
				}
				
				// Then continue over all border edges
				if (_differentiate_border_edges) glColor4f(0.8, 0.0, 0.0, 1.0);
				for (edge=_ppm->borderEdgeBegin() ; edge!=_ppm->edgeEnd() ; ++edge) {
					const PPM::Point& beg = edge->opposite()->vertex()->point();
					const PPM::Point& end = edge->vertex()->point();
					glVertex3d(beg.x(), beg.y(), beg.z());
					glVertex3d(end.x(), end.y(), end.z());
				}
				glEnd();
				
				logDuration(edges_openGL_generation_start, "OpenGL edges generation");
			}
			
			// Draw vertices
			if (_show_ppm_vertices) {
				ptime vertices_openGL_generation_start(microsec_clock::local_time());
				
				// Point antialiasing
				glEnable(GL_POINT_SMOOTH);
				glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
				// Attenuation so that points don't overwhelm the display from far
				// away viewpoints
				float point_attenuation[3];
				if (_show_ppm_faces || _show_ppm_edges) {
					glColor4fv(_vertex_color_constant);
					glPointSize(10);
					glPointParameterf(GL_POINT_SIZE_MIN, 0.0);
					glPointParameterf(GL_POINT_SIZE_MAX, 10.0);
					point_attenuation[0] = 0.0;
					point_attenuation[1] = 1.5;
					point_attenuation[2] = 5.0;
				} else {
					// Vertices are the main show, make them more visible
					glColor4fv(_vertex_color_constant);
					glPointSize(25);
					glPointParameterf(GL_POINT_SIZE_MIN, 0.0);
					glPointParameterf(GL_POINT_SIZE_MAX, 25.0);
					point_attenuation[0] = 0.0;
					point_attenuation[1] = 1.0;
					point_attenuation[2] = 5.0;
				}
				glPointParameterfv(GL_POINT_DISTANCE_ATTENUATION, point_attenuation);
				
				// Display points
				glBegin(GL_POINTS);
				for (PPM::VertexIterator vertex=_ppm->vertexBegin() ; vertex!=_ppm->vertexEnd() ; ++vertex) {
					// Compute and set dynamic vertex color
					if (_dynamic_vertex_color) {
						// Obtain scalar value
						double scalar_value = 0.0;
						switch (_dynamic_vertex_color_property) {
							case Precision: 
								scalar_value = vertex->observations;
								break;
							case SoundsourceLikelihood:
								scalar_value = vertex->sound_log_likelihood;
								break;
						}
						
						// Map to 0-1 range
						double scaled = (scalar_value - _dynamic_vertex_color_min) /
						                       _dynamic_vertex_color_max;
						double clipped = std::max(std::min(scaled,1.0),0.0);
						double high_ratio = clipped;
						double low_ratio  = 1-high_ratio;
						
						// Set vertex color
						float  vertex_color[4];
						for (unsigned int i=0 ; i<4 ; ++i) {
							vertex_color[i] = low_ratio  * _vertex_color_low[i] 
								            + high_ratio * _vertex_color_high[i];
						}						
						if (_dynamic_vertex_color_transparency) 
							vertex_color[3] *= high_ratio;
						glColor4fv(vertex_color);
					}
					
					const PPM::Point& p = vertex->point();
					glVertex3d(p.x(), p.y(), p.z());
				}
				glEnd();
				
				logDuration(vertices_openGL_generation_start, "OpenGL vertices generation");
			}
			
			logDuration(ppm_openGL_generation_start, "PPM OpenGL generation");
		}
		
		if (_show_latest_estimates) {
			ptime estimates_openGL_generation_start(microsec_clock::local_time());
			
			if (_ppm->processingDS()            != NULL &&
			    _ppm->processingDS()->estimates != NULL) {
				const PPM::Processing& processing_DS = *_ppm->processingDS();
				const PPM::Estimates&  estimate_DS   = *processing_DS.estimates;
				const std::vector<PPM::Point>& estimates = *estimate_DS.points;
				
				unsigned int n_rows = estimate_DS.n_rows;
				unsigned int n_cols = estimate_DS.n_cols;
				
				if (_show_latest_estimates_faces) {			
					glEnable(GL_LIGHTING);
					glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
					glShadeModel(GL_FLAT);
					
					// Avoid z-fighting between faces and edges if both are to be 
					// rendered
					if (_show_latest_estimates_edges) {
						glEnable(GL_POLYGON_OFFSET_FILL);
						glPolygonOffset(1.0, 1.0);
					}
					
					glColor4f(0.5, 0.5, 0.5, 0.8);
					
					glBegin(GL_TRIANGLES);
					for (unsigned int i = 0 ; i<n_rows-1 ; ++i) {
						for (unsigned int j = 0 ; j<n_cols-1 ; ++j) {
							const PPM::Point& curr  = estimates[  i   * n_cols + j];
							const PPM::Point& right = estimates[  i   * n_cols + j + 1];
							const PPM::Point& down  = estimates[(i+1) * n_cols + j];
							const PPM::Point& diag  = estimates[(i+1) * n_cols + j +1];
							
							glVertex3d(curr. x(), curr. y(), curr. z());
							glVertex3d(right.x(), right.y(), right.z());
							glVertex3d(down. x(), down. y(), down. z());
							
							glVertex3d(right.x(), right.y(), right.z());
							glVertex3d(diag. x(), diag. y(), diag. z());
							glVertex3d(down. x(), down. y(), down. z());
						}
					}
					glEnd();
				}
				
				glDisable(GL_LIGHTING);
				
				if (_show_latest_estimates_edges) {
					glLineWidth(0.5);
					glEnable(GL_LINE_SMOOTH);
					glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
					glColor4f(0.0, 0.0, 0.0, 1.0);
					
					glBegin(GL_LINES);
					for (unsigned int i = 0 ; i<n_rows-1 ; ++i) {
						for (unsigned int j = 0 ; j<n_cols-1 ; ++j) {
							const PPM::Point& curr  = estimates[  i   * n_cols + j];
							const PPM::Point& right = estimates[  i   * n_cols + j + 1];
							const PPM::Point& down  = estimates[(i+1) * n_cols + j];
							const PPM::Point& diag  = estimates[(i+1) * n_cols + j +1];
							
							glVertex3d(curr. x(), curr. y(), curr. z());
							glVertex3d(right.x(), right.y(), right.z());
							
							glVertex3d(right.x(), right.y(), right.z());
							glVertex3d(down. x(), down. y(), down. z());
							
							glVertex3d(down. x(), down. y(), down. z());
							glVertex3d(curr. x(), curr. y(), curr. z());
							
							glVertex3d(right.x(), right.y(), right.z());
							glVertex3d(diag. x(), diag. y(), diag. z());
							
							glVertex3d(diag. x(), diag. y(), diag. z());
							glVertex3d(down. x(), down. y(), down. z());
						}
					}
					glEnd();
				}
				
				if (_show_latest_estimates_vertices) {
					// Setup OpenGL state for point display
					glEnable(GL_POINT_SMOOTH);
					glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
					float estimate_color[4] = {0.0, 0.0, 0.0, 1.0};
					glColor4fv(estimate_color);
					glPointParameterf(GL_POINT_SIZE_MIN, 0.0);
					glPointParameterf(GL_POINT_SIZE_MAX, 10.0);
					float point_attenuation[3] = {0.0, 1.0, 5.0};
					glPointParameterfv(GL_POINT_DISTANCE_ATTENUATION, point_attenuation);
					
					glBegin(GL_POINTS);
					for (unsigned int i = 0 ; i<estimates.size() ; ++i) {
						const PPM::Point& estimate = estimates[i];
						glVertex3d(estimate.x(), estimate.y(), estimate.z());
					}
					glEnd();
				}
			}
			
			logDuration(estimates_openGL_generation_start, "Estimates OpenGL generation");
		}
		
		logDuration(openGL_generation_start, "OpenGL generation");
		// Release mutex for thread safety
		_ppm->unlock();
	}
	
	osg::BoundingBox PPMVisualization::computeBound() const {
		boost::tuple<std::pair<double,double>,
		             std::pair<double,double>,
		             std::pair<double,double> > bounding_box = _ppm->boundingBox();
		std::ostringstream msg; msg.precision(2); msg.width(8);
		msg << "Computing PPM bounding box for visualization: (" << std::fixed
		    << bounding_box.get<0>().first << " -> " << bounding_box.get<0>().second << ", "
		    << bounding_box.get<1>().first << " -> " << bounding_box.get<1>().second << ", "
		    << bounding_box.get<2>().first << " -> " << bounding_box.get<2>().second << ")";
		log(msg.str());
		return osg::BoundingBox(bounding_box.get<0>().first, bounding_box.get<1>().first,  bounding_box.get<2>().first,
		                        bounding_box.get<0>().second,bounding_box.get<1>().second, bounding_box.get<2>().second);
	}
	
	osg::Object* PPMVisualization::cloneType() const {
		return new PPMVisualization(_ppm);
	}
	
	osg::Object* PPMVisualization::clone(const osg::CopyOp& copyop) const {
		return new PPMVisualization(*this,copyop);
	}
	
	void PPMVisualization::onPPMChange() {
		log("PPM visualization set to dirty; will regenerate OpenGL soon");
		dirtyDisplayList();
		dirtyBound();
	}	
}

/******************************************************************************/


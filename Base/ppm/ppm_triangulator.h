/*******************************************************************************
*                                                                              *
*                               Kyoto University                               *
*                               Okuno laboratory                               *
*                                    Kappa                                     *
*                                                                              *
*******************************************************************************/

#ifndef PPM_TRIANGULATOR_H
#define PPM_TRIANGULATOR_H

// Standard headers
#include <algorithm>
#include <sstream>
#include <assert.h>
// Boost headers
#include "boost/tuple/tuple.hpp"
// CGAL headers
#include <CGAL/Polyhedron_incremental_builder_3.h>
// Project headers/cpp
#include "log_publisher.h"
#include "ppm.h"
#include "ppm_estimates.h"

namespace kappa {
	template <typename PPM>
	class PPMTriangulator : public CGAL::Modifier_base<typename PPM::HalfedgeDS>,
	                        public LogPublisher
	{
		public:
			//_______________ Constants and enumerations
			typedef typename PPM::Mesh       Mesh;
			typedef typename PPM::Point      Point;
			typedef typename PPM::Vector     Vector;
			typedef typename PPM::Segment    Segment;
			typedef typename PPM::Estimates  Estimates;
			typedef typename PPM::Processing Processing;
			typedef typename PPM::VertexHandle     VertexHandle;
			typedef typename PPM::FacetHandle      FacetHandle;
			typedef typename PPM::HalfedgeHandle   HalfedgeHandle;
			typedef typename PPM::HalfedgeIterator HalfedgeIterator;
			typedef typename PPM::HalfedgeDS       HalfedgeDS;
			typedef typename PPM::HalfedgeAroundFacetCirculator        HalfedgeAroundFacetCirculator;
			typedef CGAL::Polyhedron_incremental_builder_3<HalfedgeDS> MeshBuilder;
			
			// Processing options
			const static bool set_ids            =  PPM::simplify_mesh;
			const static bool compute_normals    = !PPM::batch_compute_normals;
			const static bool seam_free_vertices =  PPM::seam_new_estimates;
			
			//_______________ Constructors
			PPMTriangulator(PPM*  ppm,
			                Mesh* mesh, 
			                Processing& processing_DS) 
				: LogPublisher(Log::INFO),
				  _ppm(ppm),
				  _mesh(mesh), 
				  _processing_DS(processing_DS),
				  _extension_facets() {
				
			}
			
			//_______________ Public methods
			void operator()(HalfedgeDS& hds) {
				log("Triangulating new estimates");
				MeshBuilder mesh_builder(hds,true);
				mesh_builder.begin_surface((_processing_DS.estimates->n_rows+1) * (_processing_DS.estimates->n_cols+1),
				                           2 * _processing_DS.estimates->n_rows * _processing_DS.estimates->n_cols);
				
				// Add vertices to mesh builder
				log("Recording vertex indices");
				for (unsigned int i=0; i<_processing_DS.estimates->points->size(); ++i) {
					VertexHandle new_vertex = mesh_builder.add_vertex((*_processing_DS.estimates->points)[i]);
					if (set_ids) new_vertex->id() = _ppm->generateVertexID();
				}
				
				// Add facets using vertex indices
				log("Processing potential faces and triangulating new ones");
				for (unsigned int i=0; i<_processing_DS.estimates->n_rows-1; ++i) {
					for (unsigned int j=0; j<_processing_DS.estimates->n_cols-1; ++j) {
						// |-----------------------------------------|
						// |...                                      |
						// |-----------------------------------------|
						// |     | ... |  curr  |  right | ... |     |
						// |-----------------------------------------|
						// |     | ... |  down  |   diag | ... |     |
						// |-----------------------------------------|
						// |...                                      |
						// |-----------------------------------------|
						unsigned int curr_idx  = i*_processing_DS.estimates->n_cols + j;
						unsigned int right_idx = curr_idx  + 1;
						unsigned int down_idx  = curr_idx  + _processing_DS.estimates->n_cols;
						unsigned int diag_idx  = down_idx  + 1;
						
						facetProcessor(mesh_builder, curr_idx,  right_idx, down_idx);
						facetProcessor(mesh_builder, right_idx, diag_idx,  down_idx);
					}
				}
				
				mesh_builder.end_surface();
				mesh_builder.remove_unconnected_vertices();
				
				if (seam_free_vertices) {
					// Add new facets now we've finished deciding
					log("Adding extension facets");
					for (unsigned int i=0 ; i<_extension_facets.size() ; ++i) {
						// Extract handles of the halfedges that point to the 
						// extremities of the border edge the new point should form
						// a triangular facet with
						Point new_point       = boost::get<0>(_extension_facets[i]);
						HalfedgeHandle beg_he = boost::get<1>(_extension_facets[i]);
						HalfedgeHandle end_he = boost::get<2>(_extension_facets[i]);
						
						// Build the new facet and vertex
						HalfedgeHandle new_he  = _mesh->add_vertex_and_facet_to_border(beg_he,end_he);
						
						if (set_ids) {
							new_he->vertex()->id() = _ppm->generateVertexID();
							new_he->face()->id()   = _ppm->generateFaceID();
						}
						
						// Set the new point's position
						new_he->vertex()->point() = new_point;
						
						// Set the new face's normal
						if (compute_normals) {
							new_he->face()->normal = CGAL::unit_normal(beg_he->vertex()->point(),
									                                   end_he->vertex()->point(),
									                                   new_point);
						}
					}
					
					_extension_facets.clear();
				}
				
				//std::cerr << "Triangulation finished; PPM has " << _ppm->nVertex() << " vertices, " << _ppm->nFace() << " faces" << std::endl;
				
				log("Triangulation finished");
			}
		
		private:
			//_______________ Helpers
			void facetProcessor(MeshBuilder& mesh_builder, 
			                    unsigned int p1_idx, 
			                    unsigned int p2_idx, 
			                    unsigned int p3_idx) {
				// References for easier syntactical access
				const double& p1_distance   = _processing_DS.estimate_distances[p1_idx];
				const double& p2_distance   = _processing_DS.estimate_distances[p2_idx];
				const double& p3_distance   = _processing_DS.estimate_distances[p3_idx];
				const bool&   p1_intersects_PPM = _processing_DS.estimate_intersects_PPM[p1_idx];
				const bool&   p2_intersects_PPM = _processing_DS.estimate_intersects_PPM[p2_idx];
				const bool&   p3_intersects_PPM = _processing_DS.estimate_intersects_PPM[p3_idx];
				const Point&  p1 = (*_processing_DS.estimates->points)[p1_idx];
				const Point&  p2 = (*_processing_DS.estimates->points)[p2_idx];
				const Point&  p3 = (*_processing_DS.estimates->points)[p3_idx];
				
				// No data filtering
				if (isnan(p1_distance) || isnan(p2_distance) || isnan(p3_distance)) {

					return; // Filtered out because some points are undefined
				} else {
					//std::cerr << "Did not nan-filter facet " << p1 << " | " << p2 << " | " << p3 << " with distances " << p1_distance << " | " << p2_distance << " | " << p3_distance  << std::endl;
				}
				
				// Clipping filtering
				if (PPM::facet_clipping && 
				    PPM::facet_filtering) {
					if (p1_distance < PPM::clipping_distance || 
					    p2_distance < PPM::clipping_distance || 
					    p3_distance < PPM::clipping_distance) {
						if (_ppm->maintain_statistics) _processing_DS.n_filtered_clipping++;
						return; // Filtered out because too close -- probably noise
					}
				}
				
				// Confidence filtering
				if (PPM::confidence_filtering && 
				    _processing_DS.estimates->has_confidence() && PPM::facet_filtering) {
					// References for easier syntactical access
					const double& p1_confidence = (*_processing_DS.estimates->confidences)[p1_idx];
					const double& p2_confidence = (*_processing_DS.estimates->confidences)[p2_idx];
					const double& p3_confidence = (*_processing_DS.estimates->confidences)[p3_idx];
					
					if (p1_confidence < PPM::confidence_threshold || 
					    p2_confidence < PPM::confidence_threshold || 
					    p3_confidence < PPM::confidence_threshold) {
						if (_ppm->maintain_statistics) _processing_DS.n_filtered_confidence++;
						return; // Filtered out because low confidence
					}
				}
				
				// Phase wraping filtering
				if (PPM::phase_wrapping_intensity_filtering &&
				    _processing_DS.estimates->has_intensity() && PPM::facet_filtering) {
					// References for easier syntactical access
					const double& p1_intensity  = (*_processing_DS.estimates->intensities)[p1_idx];
					const double& p2_intensity  = (*_processing_DS.estimates->intensities)[p2_idx];
					const double& p3_intensity  = (*_processing_DS.estimates->intensities)[p3_idx];
					
					if (p1_intensity < PPM::phase_wrapping_intensity_threshold || 
					    p2_intensity < PPM::phase_wrapping_intensity_threshold || 
					    p3_intensity < PPM::phase_wrapping_intensity_threshold) {
						if (_ppm->maintain_statistics) _processing_DS.n_filtered_intensity++;
						return; // Filtered out because one of the points is probably phase wrapped
					}
				}
				
				// TEMP
				if (false && 
				    PPM::facet_filtering) {
					if ((!p1_intersects_PPM &&  p2_intersects_PPM &&  p3_intersects_PPM) || 
				          ( p1_intersects_PPM && !p2_intersects_PPM &&  p3_intersects_PPM) || 
				          ( p1_intersects_PPM &&  p2_intersects_PPM && !p3_intersects_PPM)) {
						if (seam_free_vertices) {
							// If 2 of the three points intersect, and one is floating, proceed to seaming
							if (!p1_intersects_PPM &&  p2_intersects_PPM &&  p3_intersects_PPM) {
								freePointProcessor(mesh_builder, p1_idx, p2_idx, p3_idx);
							} else if (p1_intersects_PPM && !p2_intersects_PPM &&  p3_intersects_PPM) {
								freePointProcessor(mesh_builder, p1_idx, p2_idx, p3_idx);
							} else if (p1_intersects_PPM &&  p2_intersects_PPM && !p3_intersects_PPM) {
								freePointProcessor(mesh_builder, p1_idx, p2_idx, p3_idx);
							}
						}
					}
				}
				
				// Intersecting facet filtering
				if (p1_intersects_PPM || p2_intersects_PPM || p3_intersects_PPM) {
					return; // Filtered out because some points intersected
				}
				
				// Incidence filtering
				if (PPM::incidence_filtering && 
				    PPM::facet_filtering) {
					// References for easier syntactical access
					const double& p1_incidence = _processing_DS.intersection_incidence_angles[_processing_DS.estimate_idx_to_intersection_idx[p1_idx]];
					const double& p2_incidence = _processing_DS.intersection_incidence_angles[_processing_DS.estimate_idx_to_intersection_idx[p2_idx]];
					const double& p3_incidence = _processing_DS.intersection_incidence_angles[_processing_DS.estimate_idx_to_intersection_idx[p3_idx]];
					if (p1_incidence > PPM::incidence_threshold || 
					    p2_incidence > PPM::incidence_threshold || 
					    p3_incidence > PPM::incidence_threshold) {
						if (_ppm->maintain_statistics) _processing_DS.n_filtered_incidence++;
						return; // Filtered out because incidence angle too high
					}
				}
				
				// Continuity filtering
				if (PPM::continuity_filtering && 
				    PPM::facet_filtering) {
					if (continuity_factor(p1_distance,p2_distance) > PPM::continuity_threshold ||
				          continuity_factor(p2_distance,p3_distance) > PPM::continuity_threshold ||
				          continuity_factor(p1_distance,p3_distance) > PPM::continuity_threshold) {
						if (_ppm->maintain_statistics) _processing_DS.n_filtered_continuity++;
						return; // Filtered out because surface not continuous
					}
				}
				
				// Shape criteria filtering
				if (PPM::shape_criteria_filtering && 
				    PPM::facet_filtering) {
					double e1_length = sqrt(squared_distance(p1,p2));
					double e2_length = sqrt(squared_distance(p2,p3));
					double e3_length = sqrt(squared_distance(p3,p1));
					double p1_angle = std::acos(std::max(std::min(Vector(p1,p2) * Vector(p1,p3),1.0),-1.0)) * 180 / CGAL_PI;
					double p2_angle = std::acos(std::max(std::min(Vector(p2,p1) * Vector(p2,p3),1.0),-1.0)) * 180 / CGAL_PI;
					double p3_angle = std::acos(std::max(std::min(Vector(p3,p1) * Vector(p3,p2),1.0),-1.0)) * 180 / CGAL_PI;
					double min_e_length = std::min(e1_length,std::min(e2_length,e3_length));
					double max_e_length = std::max(e1_length,std::max(e2_length,e3_length));
					double max_min_edge_ratio = max_e_length / min_e_length;
					
					if (max_min_edge_ratio > PPM::shape_criteria_triangle_edge_ratio) {
						if (_ppm->maintain_statistics) _processing_DS.n_filtered_shape++;
						return; // Filtered out because of bad edge ratio shape
					}
					
					if (p1_angle < PPM::shape_criteria_triangle_minimum_angle || 
					    p2_angle < PPM::shape_criteria_triangle_minimum_angle || 
					    p3_angle < PPM::shape_criteria_triangle_minimum_angle) {
						if (_ppm->maintain_statistics) _processing_DS.n_filtered_shape++;
						return; // Filtered out because of bad angle shape
					}
				}
				
				// Finally, if the facet hasn't been filtered yet, just 
				// create it
				make_facet(mesh_builder, p1_idx, p2_idx, p3_idx);
			}
			
			/// @param in p1 The free point
			void freePointProcessor(MeshBuilder& mesh_builder, 
			                        unsigned int p1_idx, 
			                        unsigned int p2_idx, 
			                        unsigned int p3_idx) {
				// References for easier syntactical access
				const Point&  p1 = (*_processing_DS.estimates->points)[p1_idx];
				//const Point&  p2 = (*_processing_DS.estimates->points)[p2_idx];
				//const Point&  p3 = (*_processing_DS.estimates->points)[p3_idx];
				FacetHandle p2_facet = _processing_DS.intersection_facet_handles[_processing_DS.estimate_idx_to_intersection_idx[p2_idx]];
				FacetHandle p3_facet = _processing_DS.intersection_facet_handles[_processing_DS.estimate_idx_to_intersection_idx[p3_idx]];
				
				// Search for all edges around the intersected point
				HalfedgeHandle closest_halfedge;
				double         closest_halfedge_distance = std::numeric_limits<double>::infinity();
				
				// Process candidate vertices around the p2 facet
				HalfedgeAroundFacetCirculator he = p2_facet->facet_begin();
				do {
					// We're only looking for border edges anyway
					if (he->is_border_edge()) {
						// Find the closest edge
						Point halfedge_1 = he->vertex()->point();
						Point halfedge_2 = he->prev()->vertex()->point();
						double distance = squared_distance(p1,Segment(halfedge_1,halfedge_2));
						if (distance < closest_halfedge_distance) {
							closest_halfedge_distance = distance;
							closest_halfedge = he;
						}
					}
				} while (++he != p2_facet->facet_begin());
				
				// Process candidate vertices around the p3 facet
				he = p3_facet->facet_begin();
				do {
					// We're only looking for border edges anyway
					if (he->is_border_edge()) {
						// Find the closest edge
						Point halfedge_1 = he->vertex()->point();
						Point halfedge_2 = he->prev()->vertex()->point();
						double distance = squared_distance(p1,Segment(halfedge_1,halfedge_2));
						if (distance < closest_halfedge_distance) {
							closest_halfedge_distance = distance;
							closest_halfedge = he;
						}
					}
				} while (++he != p3_facet->facet_begin());
				
				// Make a triangle with the best edge if any valid
				if (closest_halfedge_distance != std::numeric_limits<double>::infinity()) {
					_extension_facets.push_back(boost::make_tuple(p1,closest_halfedge,closest_halfedge->prev()));
				}
			}
			
			double continuity_factor(double d1, double d2) const {
				double dmin = std::min(d1,d2);
				double dmax = std::max(d1,d2);
				
				return (dmax-dmin)/dmin;
			}
			
			void make_facet(MeshBuilder& mesh_builder, 
			                unsigned int p1_idx,
			                unsigned int p2_idx,
			                unsigned int p3_idx) const {
				
				FacetHandle new_facet = mesh_builder.begin_facet();
				mesh_builder.add_vertex_to_facet(p1_idx);
				mesh_builder.add_vertex_to_facet(p2_idx);
				mesh_builder.add_vertex_to_facet(p3_idx);
				HalfedgeHandle new_halfedge = mesh_builder.end_facet();
				
				if (set_ids) {
					// Set the ID of this new face, as well as all the IDs of 
					// halfedges around it
					new_facet->id() = _ppm->generateFaceID();
					HalfedgeIterator halfedge_iterator = new_halfedge;
					do {
						halfedge_iterator->id() = _ppm->generateHalfedgeID();
					} while (++halfedge_iterator != new_halfedge);
				}
				
				if (compute_normals) {
					// Set the new facet's normal
					Point& p1 = mesh_builder.vertex(p1_idx)->point();
					Point& p2 = mesh_builder.vertex(p2_idx)->point();
					Point& p3 = mesh_builder.vertex(p3_idx)->point();
					
					new_facet->normal = CGAL::unit_normal(p1,p2,p3);
				}
				/*std::cerr << "Added facet: " << mesh_builder.vertex(p1_idx)->point() << " | " 
				                             << mesh_builder.vertex(p2_idx)->point() << " | " 
				                             << mesh_builder.vertex(p3_idx)->point() << std::endl;*/
				
				if (_ppm->maintain_statistics) _processing_DS.n_faces_added++;
			}
			
			//_______________ Fields
			PPM*       _ppm;
			Mesh*      _mesh;
			Processing& _processing_DS;
			std::vector< boost::tuple<Point,HalfedgeHandle,HalfedgeHandle> > _extension_facets;
	};
}

#endif

/******************************************************************************/


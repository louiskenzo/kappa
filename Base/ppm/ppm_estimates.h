/*******************************************************************************
*                                                                              *
*                               Kyoto University                               *
*                               Okuno laboratory                               *
*                                    Kappa                                     *
*                                                                              *
*******************************************************************************/

#ifndef PPM_ESTIMATES_H
#define PPM_ESTIMATES_H

#include <vector>

namespace kappa {
	template <class Point, class Vector, class FacetHandle>
	class PPMEstimates {
		public:
			//_______________ Constructors
			PPMEstimates(const Point& source,
			             std::vector<Point>*  points,
			             unsigned int n_cols,
			             unsigned int n_rows);
			
			PPMEstimates(const Point& source,
			             std::vector<Point>*  points,
			             std::vector<double>* confidences,
			             std::vector<double>* intensities,
			             unsigned int n_cols,
			             unsigned int n_rows);
			
			PPMEstimates(const Point& source,
			             std::vector<Point>*        points,
			             std::vector<double>*       confidences,
			             std::vector<double>*       intensities,
			             std::vector<unsigned int>* colors,
			             unsigned int n_cols,
			             unsigned int n_rows);
			
			//_______________ Destructors
			~PPMEstimates();
			
			//_______________ Public methods
			bool has_confidence() const;
			bool has_intensity() const;
			bool has_rgb() const;
			void add_confidence_data(std::vector<double>*  confidences);
			void add_intensity_data(std::vector<double>*   intensities);
			void add_color_data(std::vector<unsigned int>* colors);
		
		public:
			//_______________ Public fields
			// Optical center of estimates
			const Point& source;
			// Raw data
			std::vector<Point>*        points;
			std::vector<double>*       confidences;
			std::vector<double>*       intensities;
			std::vector<unsigned int>* colors;
			// Size of the pixel 
			unsigned int n_cols;
			unsigned int n_rows;
			
	};
}

#endif

/******************************************************************************/


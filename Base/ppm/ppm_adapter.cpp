/*******************************************************************************
*                                                                              *
*                               Kyoto University                               *
*                               Okuno laboratory                               *
*                                    Kappa                                     *
*                                                                              *
*******************************************************************************/

#include <iostream>
#include "log.h"
#include "ppm_adapter.h"

namespace kappa {
	//_______________ Constructors
	PPMAdapter::PPMAdapter() {
		
	}

	//_______________ Destructors
	PPMAdapter::~PPMAdapter() {
		
	}

	//_______________ Public methods
	void PPMAdapter::onPPMChange() {
		
	}
	
	void PPMAdapter::onPPMVertexCountChange(unsigned int new_vertex_count) {
		
	}
	
	void PPMAdapter::onPPMHalfedgeCountChange(unsigned int new_halfedge_count) {
		
	}
	
	void PPMAdapter::onPPMFaceCountChange(unsigned int new_face_count) {
		
	}
	
	void PPMAdapter::onLog(const std::string& msg, Log::SeverityLevel level) {
		
	}
}

/******************************************************************************/


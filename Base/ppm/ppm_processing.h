/*******************************************************************************
*                                                                              *
*                               Kyoto University                               *
*                               Okuno laboratory                               *
*                                    Kappa                                     *
*                                                                              *
*******************************************************************************/

#ifndef PPM_PROCESSING_H
#define PPM_PROCESSING_H

// Standard headers
#include <vector>
// Project headers
#include "ppm_estimates.h"

namespace kappa {
	template <class Point, 
	          class Vector, 
	          class FacetHandle, 
	          class VertexHandle>
	class PPMProcessing {
		public:
			//_______________ Types
			typedef PPMEstimates<Point,Vector,FacetHandle> Estimates;
			
			//_______________ Enumerations
			enum EstimateType {Intersection,
			                   Border,
			                   Floating
			                   };
			
			//_______________ Constructors
			PPMProcessing(Estimates* estimates);
			
			//_______________ Destructors
			~PPMProcessing();
			
			//____________________ Public methods
			double n_input() const;
			void recompute_estimate_distances();
		
		public:
			//_______________ Public fields
			Estimates* estimates;
			
			// Estimate-centric data
			std::vector<EstimateType> estimate_types;
			std::vector<bool>         estimate_intersects_PPM;
			std::vector<Vector>       estimate_light_paths;
			std::vector<unsigned int> estimate_idx_to_intersection_idx;
			std::vector<unsigned int> estimate_idx_to_floating_idx;
			std::vector<double>       estimate_distances;
			// Intersection-centric data
			std::vector<Point>        intersection_points;
			std::vector<FacetHandle>  intersection_facet_handles;
			std::vector<Vector>       intersection_facet_normals;
			std::vector<double>       intersection_incidence_angles;
			std::vector<unsigned int> intersection_idx_to_estimate_idx;
			// Floating-centric data
			std::vector<VertexHandle> floating_closest_vertex;
			std::vector<double>       floating_closest_vertex_distance;
			std::vector<unsigned int> floating_idx_to_estimate_idx;
			// Candidate facet-centric data
			
			// Statistics
			unsigned int n_input_undefined;
			unsigned int n_input_intersect;
			unsigned int n_vertices_added;
			unsigned int n_vertices_updated;
			unsigned int n_vertices_removed;
			unsigned int n_faces_added;
			unsigned int n_filtered_nan;
			unsigned int n_filtered_clipping;
			unsigned int n_filtered_confidence;
			unsigned int n_filtered_intensity;
			unsigned int n_filtered_incidence;
			unsigned int n_filtered_continuity;
			unsigned int n_filtered_shape;
	};
}

#endif

/******************************************************************************/


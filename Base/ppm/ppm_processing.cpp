/*******************************************************************************
*                                                                              *
*                               Kyoto University                               *
*                               Okuno laboratory                               *
*                                    Kappa                                     *
*                                                                              *
*******************************************************************************/

// Standard headers
#include <vector>
// Project headers
#include "ppm_processing.h"
#include "ppm.h"
#include "ppm_estimates.h"

namespace kappa {
	//_______________ Constructors
	template <class Point,
	          class Vector,
	          class FacetHandle, 
	          class VertexHandle>
	PPMProcessing<Point,Vector,FacetHandle,VertexHandle>::
	PPMProcessing(Estimates* estimates) 
		: estimates(estimates),
		  n_input_undefined(0),
		  n_input_intersect(0),
		  n_filtered_intensity(0),
		  n_vertices_added(0),
		  n_vertices_updated(0),
		  n_vertices_removed(0) {
		
	}
	
	//_______________ Destructors
	template <class Point,
	          class Vector,
	          class FacetHandle, 
	          class VertexHandle>
	PPMProcessing<Point,Vector,FacetHandle,VertexHandle>::
	~PPMProcessing() {
		if (estimates != NULL) delete estimates;
	}
	
	
	//____________________ Public methods
	template <class Point,
	          class Vector,
	          class FacetHandle, 
	          class VertexHandle>
	double PPMProcessing<Point,Vector,FacetHandle,VertexHandle>::
	n_input() const {
		return static_cast<double>(estimates->n_cols * estimates->n_rows);
	}
	
	template <class Point,
	          class Vector,
	          class FacetHandle, 
	          class VertexHandle>
	void PPMProcessing<Point,Vector,FacetHandle,VertexHandle>::recompute_estimate_distances() {
		estimate_distances.clear();
		
		for (unsigned int i=0; i<estimates->n_rows-1; ++i) {
			for (unsigned int j=0; j<estimates->n_cols-1; ++j) {
				const Point& p = (*estimates->points)[i*estimates->n_cols + j];
				estimate_distances.push_back(sqrt(squared_distance(estimates->source,p)));			
			}
		}
	}
	
	// Explicit template instantiation
	template class PPMProcessing<PPM::Point,
	                             PPM::Vector,
	                             PPM::FacetHandle,
	                             PPM::VertexHandle>;
}

/******************************************************************************/


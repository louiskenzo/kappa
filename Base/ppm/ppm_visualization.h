/*******************************************************************************
*                                                                              *
*                               Kyoto University                               *
*                               Okuno laboratory                               *
*                                    Kappa                                     *
*                                                                              *
*******************************************************************************/

#ifndef PPM_VISUALIZATION_H
#define PPM_VISUALIZATION_H

#include <osg/Object>
#include <osg/Drawable>
#include <osg/RenderInfo>
#include <osg/CopyOp>
#include <osg/BoundingBox>
#include "visualization.h"
#include "ppm.h"
#include "ppm_adapter.h"
#include "log_publisher.h"

namespace kappa {
	class PPM;
	
	/// 
	class PPMVisualization : public Visualization, 
	                         public PPMAdapter,
	                         public LogPublisher
	{
		public:
			//_______________ Constants and enumerations
			enum VertexColoringScheme {Constant,
			                           Observations};
			enum PPMScalarProperty {Precision,
			                        SoundsourceLikelihood};
			
			static float _vertex_color_constant[4];
			static float _vertex_color_low[4];
			static float _vertex_color_high[4];
			
			//_______________ Constructors
			PPMVisualization(PPM* ppm);
			PPMVisualization(const PPMVisualization& viz,
			                 const osg::CopyOp&      copyop = osg::CopyOp::SHALLOW_COPY);
		
			//_______________ Destructors
			~PPMVisualization();
		
			//_______________ Public methods
			virtual void drawImplementation(osg::RenderInfo &renderInfo) const;
			virtual osg::BoundingBox computeBound() const;
			virtual osg::Object* cloneType() const;
			virtual osg::Object* clone(const osg::CopyOp& copyop) const;
			virtual void onPPMChange();
	
		private:
			//_______________ Helpers
			
		
			//_______________ Fields
			PPM* _ppm;
			
			// PPM display preferences
			bool _show_ppm;
			bool _show_ppm_vertices;
			bool _show_ppm_edges;
			bool _show_ppm_faces;
			bool _differentiate_border_edges;
			// Dynamic vertex coloring
			bool              _dynamic_vertex_color;
			PPMScalarProperty _dynamic_vertex_color_property;
			double            _dynamic_vertex_color_min;
			double            _dynamic_vertex_color_max;
			bool              _dynamic_vertex_color_transparency;
			// Processing display preferences
			bool _show_latest_estimates;
			bool _show_latest_estimates_vertices;
			bool _show_latest_estimates_edges;
			bool _show_latest_estimates_faces;
	};
}

#endif

/******************************************************************************/


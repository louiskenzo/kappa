/*******************************************************************************
*                                                                              *
*                               Kyoto University                               *
*                               Okuno laboratory                               *
*                                    Kappa                                     *
*                                                                              *
*******************************************************************************/

#ifndef PPM_ADAPTER_H
#define PPM_ADAPTER_H

#include <string>
#include "log_subscriber.h"

namespace kappa {
	/// 
	class PPMAdapter : public virtual LogSubscriber {
		public:
			//_______________ Constants and enumerations
			
			
			//_______________ Static functions
			
			
			//_______________ Constructors
			PPMAdapter();
		
			//_______________ Destructors
			virtual ~PPMAdapter();
		
			//_______________ Public methods
			virtual void onPPMChange();
			virtual void onPPMVertexCountChange(unsigned int new_vertex_count);
			virtual void onPPMHalfedgeCountChange(unsigned int new_halfedge_count);
			virtual void onPPMFaceCountChange(unsigned int new_face_count);
			virtual void onLog(const std::string& msg, Log::SeverityLevel level);
	
		private:
			//_______________ Helpers
			
		
			//_______________ Fields
			
	};
}

#endif

/******************************************************************************/


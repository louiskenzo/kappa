#include <cstdlib>
#include <string>
#include "ppm.h"
#include "ppm_visualization.h"
#include "kappa/command_center.h"

using namespace kappa;

void onCommandCenterClose();
int main(int argc, char* argv[]) {
	CommandCenter command_center(argc,argv,onCommandCenterClose);
	PPM ppm;
	
	command_center.addVisualization(std::string("PPM"), new kappa::PPMVisualization(&ppm));
	command_center.start();
	
	return EXIT_SUCCESS;
}

void onCommandCenterClose() {

}


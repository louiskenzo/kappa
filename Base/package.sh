################################################################################
#                                                                              #
#                               Kyoto University                               #
#                               Okuno laboratory                               #
#                                Boomer project                                #
#                                                                              #
################################################################################

# Save current directory to restore at the end
cwd=$(pwd)
apt_basedir=${1:-~/www/kappa/apt}
src_basedir=${2:-~/www/kappa/src}
build_dir=${3:-./build/release}

# Build in release mode
./release.sh $build_dir

# Package as deb file and import into apt repository
cd $build_dir
echo -e "\033[1mBuilding packages\033[0m"
sudo make package
echo -e "\033[1mInstalling deb package on apt web repository\033[0m"
for package in *.deb; do
	reprepro --basedir $apt_basedir includedeb lucid $package
done

# Restore working directory
cd ${cwd}

# Copy source to web server
echo -e "\033[1mCopying source to web server...\033[0m"
rm -rf $src_basedir
cp -R * $src_basedir
echo "Options Indexes" > $src_basedir/.htaccess

################################################################################

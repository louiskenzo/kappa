################################################################################
#                                                                              #
#                               Kyoto University                               #
#                               Okuno laboratory                               #
#                                Boomer project                                #
#                                                                              #
################################################################################

# Save current directory to restore at the end
cwd=$(pwd)
build_dir=${1:-./build}
release_build_dir=$build_dir/release

# Make the build directories if necessary
echo -e "\033[1mSetting up target build directory to $build_dir\033[0m"
mkdir -p $release_build_dir 2> /dev/null

# Build in release mode
echo -e "\033[1mBuilding in release mode\033[0m"
cd $release_build_dir
cmake ${2:-${cwd}} -DCMAKE_BUILD_TYPE=Release
make

# Restore working directory
cd ${cwd}

################################################################################


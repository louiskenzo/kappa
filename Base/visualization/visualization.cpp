/*******************************************************************************
*                                                                              *
*                               Kyoto University                               *
*                               Okuno laboratory                               *
*                                    Kappa                                     *
*                                                                              *
*******************************************************************************/

#include <cstdlib>
#include <osg/CopyOp>
#include "visualization.h"

#include <osg/Group>

namespace kappa {
	//_______________ Static functions
	
	
	//_______________ Constructors
	Visualization::Visualization() {
		
	}
	
	Visualization::Visualization(const Visualization& viz, 
	                             const osg::CopyOp& copyop)
		: Drawable(*this,copyop) {
		
	}
	
	//_______________ Destructors
	
	
	//_______________ Public methods
	
	
	//_______________ Helpers
	
}

/******************************************************************************/


/*******************************************************************************
*                                                                              *
*                               Kyoto University                               *
*                               Okuno laboratory                               *
*                                    Kappa                                     *
*                                                                              *
*******************************************************************************/

#ifndef KAPPA_VISUALIZATION_H
#define KAPPA_VISUALIZATION_H

#include <osg/Node>
#include <osg/Drawable>
#include <osg/RenderInfo>
#include <osg/CopyOp>

namespace kappa {
	/// A visualization scene for Kappa concepts.
	class Visualization : public osg::Drawable {
		public:
			//_______________ Constants and enumerations
			
			
			//_______________ Static functions
			
			
			//_______________ Constructors
			Visualization();
			
			Visualization(const Visualization& viz, 
			              const osg::CopyOp& copyop = osg::CopyOp::SHALLOW_COPY);
			
			//_______________ Destructors
			
			
			//_______________ Public methods
			virtual void drawImplementation(osg::RenderInfo &renderInfo) const = 0;
		
		private:
			//_______________ Helpers
			
			
			//_______________ Fields
			
	};
}

#endif

/******************************************************************************/


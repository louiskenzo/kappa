/*******************************************************************************
*                                                                              *
*                               Kyoto University                               *
*                               Okuno laboratory                               *
*                                    Kappa                                     *
*                                                                              *
*******************************************************************************/

// Standard headers
#include <iostream>
#include <utility>
#include <cstdlib>
// OSG headers
#include <gtkmm.h>
#include <osgGtkmm/ViewerGtkmm.h>
#include <osg/Group>
#include <osg/Geometry>
#include <osg/Light>
#include <osg/Camera>
// Project headers
#include "log.h"
#include "log_publisher.h"
#include "command_center.h"

namespace kappa {
	//_______________ Constructors
	CommandCenter::CommandCenter(int    argc, 
	                             char*  argv[], 
	                             void (*close_hook)())
		: LogPublisher(Log::DEBUG),
		  _scene(new osg::Group()),
		  _visualizations(),
		  _viewer(),
		  _close_hook(close_hook),
		  _gtk_kit(argc,argv),
		  _main_window(Gtk::WINDOW_TOPLEVEL),
		  _viewer_widget(NULL) {
		Gtk::GL::init(argc,argv);
		setup_gui();
	}
	
	//_______________ Destructors
	CommandCenter::~CommandCenter() {
		stop();
	}
	
	//_______________ Public methods
	void CommandCenter::start() {
		log("Starting command center");
		_main_window.show_all();
		_viewer.run();                // Non-blocking, spawns a new thread
		Gtk::Main::run(_main_window); // Blocking
	}
	
	void CommandCenter::stop() {
		log("Stoping command center cleanly");
		_main_window.hide();
		_close_hook();
		log("Command center stopped",Log::INFO);
	}
	
	void CommandCenter::onClose(void (*close_hook)()) {
		_close_hook = close_hook;
	}
	
	void CommandCenter::addVisualization(std::string name, Visualization* viz) {
		log("Adding new visualization to command center");
		_visualizations.insert(std::pair<const std::string, Visualization*>(name,viz));
		
		// For now, just add any new visualization to the scene
		osg::Geode* visualizationGeode = new osg::Geode();
		visualizationGeode->addDrawable(_visualizations.find(name)->second);
		_scene->addChild(visualizationGeode);
		
		// TODO Code a grid to be displayed on the z=0 plane with spacing 1
		
	}
	
	//_______________ Helpers
	void CommandCenter::setup_gui() {
		log("Setting up command center GUI");
		
		// Set main window properties
		_main_window.set_title("Kappa command center");
		
		// Setup callbacks
		_main_window.signal_delete_event().connect(sigc::mem_fun(*this, &CommandCenter::on_delete_event));
		
		// Set up the OSG viewer
		log("Setting up OSG viewer");
		_viewer.set_fps(60);
		_viewer.setSceneData(_scene);
		_viewer_widget = _viewer.setup_viewer_in_gtkmm_window(800, 600);
		//_viewer.getCamera()->setClearColor(osg::Vec4f(0.1,0.1,0.1,1));
		_viewer.setLightingMode(osg::View::SKY_LIGHT);
		//_viewer.getCamera()->setViewMatrixAsLookAt(osg::Vec3d(0.0,0.0,0.0),
		//                                           osg::Vec3d(0.0,0.0,1.0),
		//                                           osg::Vec3d(0.0,1.0,0.0));
		
		// Add OSG viewer to GTK window
		_main_window.add(*_viewer_widget);
		
		// Setup a default grid to visualize the coordinate space
		
	}
	
	//_______________ GTK+ signal handlers
	bool CommandCenter::on_delete_event(GdkEventAny* event) {
		log("Command center received close action");
		_close_hook();
		
		return false; // Causes window to close
	}
}

/******************************************************************************/


/*******************************************************************************
*                                                                              *
*                               Kyoto University                               *
*                               Okuno laboratory                               *
*                                    Kappa                                     *
*                                                                              *
*******************************************************************************/

#ifndef KAPPA_COMMAND_CENTER_H
#define KAPPA_COMMAND_CENTER_H

// Standard headers
#include <string>
#include <map>
#include <cstdlib>
// OSG headers
#include <gtkmm.h>
#include <osgGtkmm/ViewerGtkmm.h>
#include <osg/Group>
// Project headers
#include "log_publisher.h"
#include "visualization.h"

namespace kappa {
	/// 
	class CommandCenter : public LogPublisher {
		public:
			//_______________ Constants and enumerations
			
			
			//_______________ Constructors
			CommandCenter(int    argc, 
			              char*  argv[], 
	                      void (*close_hook)() = NULL);
			
			//_______________ Destructors
			~CommandCenter();
			
			//_______________ Public methods
			void start();
			void stop();
			void onClose(void (*callback)());
			void addVisualization(std::string name, Visualization* viz);
		
		private:
			//_______________ Helpers
			void setup_gui();
			
			//_______________ GTK+ signal handlers
			bool on_delete_event(GdkEventAny* event);
			
			//_______________ Fields
			// 3D scene graph and data for visualization objects
			osg::Group* _scene;
			std::multimap<std::string,Visualization*> _visualizations;
			
			// Maintains cameras, provides widgets, traverses 3D scene graph
			osgViewer::ViewerGtkmm _viewer;
			
			// External event callbacks for use by CommandCenter users
			void (*_close_hook)();
			
			// GTK elements
			Gtk::Main   _gtk_kit;
			Gtk::Window _main_window;
			osgViewer::GraphicsWindowGtkmm* _viewer_widget;
	};
}

#endif

/******************************************************************************/


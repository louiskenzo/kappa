################################################################################
#                                                                              #
#                               Kyoto University                               #
#                               Okuno laboratory                               #
#                                Boomer project                                #
#                                                                              #
################################################################################

# Add include and library directories for GTKmm
find_package(PkgConfig)
pkg_check_modules(GTKMM gtkmm-2.4)
include_directories(${GTKMM_INCLUDE_DIRS})
link_directories(${GTKMM_LIBRARY_DIRS})
# Add include and library directories for osgGtk
find_package(osgGtk)
include_directories(${osgGtk_INCLUDE_DIRS})
link_directories(${osgGtk_LIBRARY_DIR})
# Include project headers
include_directories(${PROJECT_SOURCE_DIR}/visualization)
include_directories(${PROJECT_SOURCE_DIR}/tools)

# Build the command center module
add_library(kappa-command-center SHARED command_center.cpp)
target_link_libraries(kappa-command-center kappa-tools
                                           kappa-visualization
                                           osgGtkmm)

# Deploy libraries
install(TARGETS kappa-command-center
        LIBRARY DESTINATION ${INSTALLATION_LIB_DIR})
# Deploy headers
install(FILES command_center.h
        DESTINATION ${INSTALLATION_INC_DIR})

################################################################################


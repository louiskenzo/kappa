################################################################################
#                                                                              #
#                               Kyoto University                               #
#                               Okuno laboratory                               #
#                                Boomer project                                #
#                                                                              #
################################################################################

# Build the Blackship module
add_library(kappa-blackship SHARED blackship.cpp 
                                   blackship_motor.cpp)

# Deploy libraries
install(TARGETS kappa-blackship
        LIBRARY DESTINATION ${INSTALLATION_LIB_DIR})
# Deploy headers
install(FILES blackship.h blackship_motor.h
        DESTINATION ${INSTALLATION_INC_DIR})

################################################################################


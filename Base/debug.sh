################################################################################
#                                                                              #
#                               Kyoto University                               #
#                               Okuno laboratory                               #
#                                Boomer project                                #
#                                                                              #
################################################################################

# Save current directory to restore at the end
cwd=$(pwd)
build_dir=${1:-./build}
debug_build_dir=$build_dir/debug

# Make the build directories if necessary
echo -e "\033[1mSetting up target build directory to $build_dir\033[0m"
mkdir -p $debug_build_dir 2> /dev/null

# Build in debug mode
echo -e "\033[1mBuilding in debug mode\033[0m"
cd $debug_build_dir
cmake ${2:-${cwd}} -DCMAKE_BUILD_TYPE=Debug
make

# Restore working directory
cd ${cwd}

################################################################################


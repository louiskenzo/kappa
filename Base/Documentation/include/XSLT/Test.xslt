<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format decimal-separator=","
	                    grouping-separator=" "
	                    infinity="∞"
	                    minus-sign="-"
	                    NaN="PuN"
	                    percent="%"
	                    per-mille="‰"
	                    zero-digit="0"
	                    digit="#"
	                    pattern-separator=";"/> 
	
	<!-- General information on tests -->
	<xsl:variable name="totalNumberTestSuites"       select="count(//TestResult//TestSuite)"/>
	<xsl:variable name="totalNumberTestSuitesFailed" select="count(//TestResult//TestSuite[@result='failed'])"/>
	<xsl:variable name="totalNumberTestSuitesPassed" select="count(//TestResult//TestSuite[@result='passed'])"/>
	<xsl:variable name="totalNumberTestCases"        select="count(//TestResult//TestCase)"/>
	<xsl:variable name="totalNumberTestCasesFailed"  select="count(//TestResult//TestCase[@result='failed'])"/>
	<xsl:variable name="totalNumberTestCasesPassed"  select="count(//TestResult//TestCase[@result='passed'])"/>
	<xsl:variable name="testingTime" select="sum(//TestingTime)"/>
	
	<!-- Total number of test suites -->
	<xsl:variable name="totalNumberTestSuitesString">
		<xsl:if test="$totalNumberTestSuites >= 2">
			<xsl:value-of select="$totalNumberTestSuites"/> test suites
		</xsl:if>
		<xsl:if test="$totalNumberTestSuites = 1">
			<xsl:value-of select="$totalNumberTestSuites"/> test suite
		</xsl:if>
		<xsl:if test="$totalNumberTestSuites = 0">
			No test suite
		</xsl:if>
	</xsl:variable>
	
	<!-- Total number of test suites failed -->
	<xsl:variable name="totalNumberTestSuitesFailedString">
		<xsl:if test="$totalNumberTestSuitesFailed >= 2">
			<xsl:value-of select="$totalNumberTestSuitesFailed"/> failed
		</xsl:if>
		<xsl:if test="$totalNumberTestSuitesFailed = 1">
			<xsl:value-of select="$totalNumberTestSuitesFailed"/> failed
		</xsl:if>
		<xsl:if test="$totalNumberTestSuitesFailed = 0">
			none failed
		</xsl:if>
	</xsl:variable>
	
	<!-- Total number of test suites passed -->
	<xsl:variable name="totalNumberTestSuitesPassedString">
		<xsl:if test="$totalNumberTestSuitesPassed >= 2">
			<xsl:value-of select="$totalNumberTestSuitesPassed"/> passed
		</xsl:if>
		<xsl:if test="$totalNumberTestSuitesPassed = 1">
			<xsl:value-of select="$totalNumberTestSuitesPassed"/> passed
		</xsl:if>
		<xsl:if test="$totalNumberTestSuitesPassed = 0">
			none passed
		</xsl:if>
	</xsl:variable>
	
	<!-- Total number of tests cases -->
	<xsl:variable name="totalNumberTestCasesString">
		<xsl:if test="$totalNumberTestCases >= 2">
			<xsl:value-of select="$totalNumberTestCases"/> test cases
		</xsl:if>
		<xsl:if test="$totalNumberTestCases = 1">
			<xsl:value-of select="$totalNumberTestCases"/> test case
		</xsl:if>
		<xsl:if test="$totalNumberTestCases = 0">
			No test case
		</xsl:if>
	</xsl:variable>
	
	<!-- Total number of test cases failed -->
	<xsl:variable name="totalNumberTestCasesFailedString">
		<xsl:if test="$totalNumberTestCasesFailed >= 2">
			<xsl:value-of select="$totalNumberTestCasesFailed"/> failed
		</xsl:if>
		<xsl:if test="$totalNumberTestCasesFailed = 1">
			<xsl:value-of select="$totalNumberTestCasesFailed"/> failed
		</xsl:if>
		<xsl:if test="$totalNumberTestCasesFailed = 0">
			none failed
		</xsl:if>
	</xsl:variable>
	
	<!-- Total number of test cases passed -->
	<xsl:variable name="totalNumberTestCasesPassedString">
		<xsl:if test="$totalNumberTestCasesPassed >= 2">
			<xsl:value-of select="$totalNumberTestCasesPassed"/> passed
		</xsl:if>
		<xsl:if test="$totalNumberTestCasesPassed = 1">
			<xsl:value-of select="$totalNumberTestCasesPassed"/> passed
		</xsl:if>
		<xsl:if test="$totalNumberTestCasesPassed = 0">
			none passed
		</xsl:if>
	</xsl:variable>
	
	<!-- Testing time -->
	<xsl:variable name="testingTimeString">
		<xsl:choose>
			<xsl:when test="$testingTime = 0"> negligible</xsl:when>
			<xsl:when test="$testingTime &lt; 1000000">≈<xsl:value-of select="format-number($testingTime div 1000,'#')"/> ms</xsl:when>
			<xsl:otherwise>≈<xsl:value-of select="format-number($testingTime div 1000000,'#,##')"/> s</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	
	<!-- Report template -->
	<xsl:template match="TestReportRoot">
		<html>
			<head>
				<link rel="stylesheet" type="text/css" href="CSS/Test.css"/>
				<link rel="stylesheet" type="text/css" href="CSS/cupertiono/jquery-ui-1.8.1.custom.css"/>	
				<script type="text/javascript" src="javascripts/jquery-1.4.2.min.js"/>
				<script type="text/javascript" src="javascripts/jquery-ui-1.8.1.custom.min.js"/>
				<script type="text/javascript" src="javascripts/jquery.tools.min.js"/>
				<script type="text/javascript">
					function toggleSlide(e) {
						$(e).next().toggle('slide',{direction: "right"},250);
						$(e).toggleClass('Visible');
					}
					function toggleTestSuite(e) {
						$(e).next().toggle('blind',500);
						$(e).toggleClass('Hidden');
					}
				</script>
			</head>
			<body>
				<div class="TestRoot">
					<div class="TestSuite TestSummary">
						<div class="Header">
							<span class="Name">Summary</span>
							
							<div class="Summary">
								<div class="Headline">
									Success rate: <span class="SuccessRate"><xsl:value-of select="format-number($totalNumberTestCasesPassed div $totalNumberTestCases * 100,'#,#')"/>%</span> (<xsl:value-of select="$totalNumberTestCasesPassed"/>/<xsl:value-of select="$totalNumberTestCases"/>),  cumulative time: <span class="Time"><xsl:value-of select="$testingTimeString"/></span>
								</div>
								<div>
									Ran <xsl:value-of select="$totalNumberTestSuitesString"/>, <xsl:value-of select="$totalNumberTestSuitesPassedString"/>, <xsl:value-of select="$totalNumberTestSuitesFailedString"/>. Success rate <xsl:value-of select="format-number($totalNumberTestSuitesPassed div $totalNumberTestSuites * 100,'#,#')"/>% (<xsl:value-of select="$totalNumberTestSuitesPassed"/>/<xsl:value-of select="$totalNumberTestSuites"/>). Fail rate <xsl:value-of select="format-number($totalNumberTestSuitesFailed div $totalNumberTestSuites * 100,'#,#')"/>% (<xsl:value-of select="$totalNumberTestSuitesFailed"/>/<xsl:value-of select="$totalNumberTestSuites"/>).
								</div>
								<div>
									Ran <xsl:value-of select="$totalNumberTestCasesString"/>, <xsl:value-of select="$totalNumberTestCasesPassedString"/>, <xsl:value-of select="$totalNumberTestCasesFailedString"/>. Success rate <xsl:value-of select="format-number($totalNumberTestCasesPassed div $totalNumberTestCases * 100,'#,#')"/>% (<xsl:value-of select="$totalNumberTestCasesFailed"/>/<xsl:value-of select="$totalNumberTestCases"/>). Fail rate <xsl:value-of select="format-number($totalNumberTestCasesFailed div $totalNumberTestCases * 100,'#,#')"/>% (<xsl:value-of select="$totalNumberTestCasesFailed"/>/<xsl:value-of select="$totalNumberTestCases"/>).
								</div>
							</div>
						</div>
					</div>
					<xsl:apply-templates select="TestReport"/>
				</div>
			</body>
		  </html>
	</xsl:template>
	
	<!-- Boomer test log - group a Test result and a Test log -->
	<xsl:template match="TestReport">
		<xsl:apply-templates select="TestLog"/>
	</xsl:template>
	
	<!-- Test result -->
	<xsl:template match="TestResult">
		<xsl:apply-templates/>
	</xsl:template>
	
	<!-- Test log -->
	<xsl:template match="TestLog">
		<div class="TestLog">
			<xsl:apply-templates/>
		</div>
	</xsl:template>
	
	<!-- Build info -->
	<xsl:template match="BuildInfo">
		<div class="BuildInfo">
			<span class="Title" onClick="toggleSlide(this);return false;">Build info</span>
			<div class="Info" style="display:none;">
				<div><span class="Label">Platform: </span><xsl:value-of select="@platform"/></div>
				<div><span class="Label">Compiler: </span><xsl:value-of select="@compiler"/></div>
				<div><span class="Label">STL: </span><xsl:value-of select="@stl"/></div>
				<div><span class="Label">Boost: </span><xsl:value-of select="@boost"/></div>
			</div>
		</div>
	</xsl:template>
	
	<!-- Test suite -->
	<xsl:template match="TestSuite">
		<xsl:variable name="name" select="@name"/>
		<xsl:variable name="result" 
		              select="ancestor::TestReport[1]/TestResult//TestSuite[@name=$name]/@result"/>
		<xsl:variable name="assertions_passed" 
		              select="ancestor::TestReport[1]/TestResult//TestSuite[@name=$name]/@assertions_passed"/>
		<xsl:variable name="assertions_failed" 
		              select="ancestor::TestReport[1]/TestResult//TestSuite[@name=$name]/@assertions_failed"/>
		<xsl:variable name="expected_failures" 
		              select="ancestor::TestReport[1]/TestResult//TestSuite[@name=$name]/@expected_failures"/>
		<xsl:variable name="test_cases_passed" 
		              select="count(ancestor::TestReport[1]/TestResult//TestSuite[@name=$name]//TestCase[@result='passed'])"/>
		<xsl:variable name="test_cases_failed" 
		              select="count(ancestor::TestReport[1]/TestResult//TestSuite[@name=$name]//TestCase[@result='failed'])"/>
		<xsl:variable name="test_cases_skipped" 
		              select="ancestor::TestReport[1]/TestResult//TestSuite[@name=$name]/@test_cases_skipped"/>
		<xsl:variable name="test_cases_aborted" 
		              select="ancestor::TestReport[1]/TestResult//TestSuite[@name=$name]/@test_cases_aborted"/>
		<xsl:variable name="numberTestCases" 
		              select="count(.//TestCase)"/>
		<xsl:variable name="numberTestCasesPassed" 
		              select="$test_cases_passed"/>
		<xsl:variable name="numberTestCasesFailed" 
		              select="$test_cases_failed"/>
		<xsl:variable name="testingTime" select="sum(//TestingTime)"/>
		
		<!-- Number of tests cases -->
		<xsl:variable name="numberTestCasesString">
			<xsl:if test="$numberTestCases >= 2">
				<xsl:value-of select="$numberTestCases"/> test cases
			</xsl:if>
			<xsl:if test="$numberTestCases = 1">
				<xsl:value-of select="$numberTestCases"/> test case
			</xsl:if>
			<xsl:if test="$numberTestCases = 0">
				No test case
			</xsl:if>
		</xsl:variable>
		
		<!-- Number of test cases failed -->
		<xsl:variable name="numberTestCasesFailedString">
			<xsl:if test="$test_cases_failed >= 2">
				<xsl:value-of select="$test_cases_failed"/> failed
			</xsl:if>
			<xsl:if test="$test_cases_failed = 1">
				<xsl:value-of select="$test_cases_failed"/> failed
			</xsl:if>
			<xsl:if test="$test_cases_failed = 0">
				none failed
			</xsl:if>
		</xsl:variable>
		
		<!-- Number of test cases passed -->
		<xsl:variable name="numberTestCasesPassedString">
			<xsl:if test="$test_cases_passed >= 2">
				<xsl:value-of select="$test_cases_passed"/> passed
			</xsl:if>
			<xsl:if test="$test_cases_passed = 1">
				<xsl:value-of select="$test_cases_passed"/> passed
			</xsl:if>
			<xsl:if test="$test_cases_passed = 0">
				none passed
			</xsl:if>
		</xsl:variable>
		
		<!-- Testing time -->
		<xsl:variable name="testingTimeString">
			<xsl:choose>
				<xsl:when test="$testingTime = 0"> negligible time</xsl:when>
				<xsl:when test="$testingTime &lt; 1000000">≈<xsl:value-of select="format-number($testingTime div 1000,'#')"/> ms</xsl:when>
				<xsl:otherwise>≈<xsl:value-of select="format-number($testingTime div 1000000,'#,##')"/> s</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		
		<div class="TestSuite">
			<div class="Header" onclick="toggleTestSuite(this);return false;">
				<span class="Name"><xsl:value-of select="@name"/></span>
				<div class="Summary">
					<xsl:choose>
						<xsl:when test="$numberTestCases = 0">
							No test to run
						</xsl:when>
						<xsl:when test="$numberTestCasesPassed = $numberTestCases">
							All tests passed
						</xsl:when>
						<xsl:otherwise>
							<div>
								<xsl:value-of select="$numberTestCasesString"/>, <xsl:value-of select="$numberTestCasesPassedString"/>, <xsl:value-of select="$numberTestCasesFailedString"/>. Success rate <xsl:value-of select="format-number($numberTestCasesPassed div $numberTestCases * 100,'#,#')"/>% (<xsl:value-of select="$numberTestCasesFailed"/>/<xsl:value-of select="$numberTestCases"/>). Fail rate <xsl:value-of select="format-number($numberTestCasesFailed div $numberTestCases * 100,'#,#')"/>% (<xsl:value-of select="$numberTestCasesFailed"/>/<xsl:value-of select="$numberTestCases"/>).
							</div>
						</xsl:otherwise>
					</xsl:choose>
				</div>
			</div>
			<div class="TestCases">
				<!-- If there are many test cases to display, we can try to prehide some things to save space:
				     * Hide top level tests suites with 100% success rate
				     * Hide test suites with 100% success that have more than 10 sub-cases -->
				<xsl:if test="$totalNumberTestCases > 25
				              and
				              $numberTestCasesPassed = $numberTestCases 
				              and (count(ancestor::TestSuite) = 0 
				                   or $numberTestCases >= 10)">
					<xsl:attribute name="style">
						display:none;
					</xsl:attribute>
				</xsl:if>
				<div class="TestCaseColumns" style="display:none;">
					<div>Test name</div><div>Messages</div><div>Run time</div>
				</div>
				<xsl:apply-templates/>
			</div>
		</div>
	</xsl:template>
	
	<!-- Failed test case -->
	<xsl:template match="TestCase">
		<xsl:variable name="name" select="@name"/>
		<xsl:variable name="testingTime" select="TestingTime"/>
		<xsl:variable name="result" 
		              select="ancestor::TestReport[1]/TestResult//TestCase[@name=$name]/@result"/>
		<xsl:variable name="assertions_passed" 
				      select="ancestor::TestReport[1]/TestResult//TestCase[@name=$name]/@assertions_passed"/>
		<xsl:variable name="assertions_failed" 
				      select="ancestor::TestReport[1]/TestResult//TestCase[@name=$name]/@assertions_failed"/>
		<xsl:variable name="expected_failure" 
				      select="ancestor::TestReport[1]/TestResult//TestCase[@name=$name]/@expected_failure"/>
				      
		<xsl:variable name="isFailed" select="$result='failed' or $result='aborted'"/>
		<xsl:variable name="isPassed" select="$result='passed'"/>
		
		<!-- Number of assertions failed -->
		<xsl:variable name="assertionsFailedString">
			<xsl:if test="$assertions_failed >= 2">
				<xsl:value-of select="$assertions_failed"/> assertions failed
			</xsl:if>
			<xsl:if test="$assertions_failed = 1">
				<xsl:value-of select="$assertions_failed"/> assertion failed
			</xsl:if>
			<xsl:if test="$assertions_failed = 0">
				No assertion failed
			</xsl:if>
		</xsl:variable>

		<!-- Number of assertions passed -->
		<xsl:variable name="assertionsPassedString">
			<xsl:if test="$assertions_passed >= 2">
				<xsl:value-of select="$assertions_passed"/> assertions passed
			</xsl:if>
			<xsl:if test="$assertions_passed = 1">
				<xsl:value-of select="$assertions_passed"/> assertion passed
			</xsl:if>
			<xsl:if test="$assertions_passed = 0">
				No assertion passed
			</xsl:if>
		</xsl:variable>

		<!-- Number of assertions expected to fail -->
		<xsl:variable name="assertionsExpectedFailString">
			<xsl:if test="$expected_failure >= 2">
				<xsl:value-of select="$expected_failure"/> assertions expected to fail
			</xsl:if>
			<xsl:if test="$expected_failure = 1">
				<xsl:value-of select="$expected_failure"/> assertion expected to fail
			</xsl:if>
			<xsl:if test="$expected_failure = 0">
				No assertion expected to fail
			</xsl:if>
		</xsl:variable>

		<!-- Testing time -->
		<xsl:variable name="testingTimeString">
			<xsl:choose>
				<xsl:when test="$testingTime &lt; 1000000"><xsl:value-of select="$testingTime div 1000"/> ms</xsl:when>
				<xsl:otherwise><xsl:value-of select="$testingTime div 1000000"/> s</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:choose>
			<xsl:when test="$isFailed">
				<div class="TestCase Failed">
					<div class="Name"><xsl:value-of select="@name"/></div>
					<xsl:value-of select="@result"/>
					<xsl:value-of select="@assertions_passed"/>
					<xsl:value-of select="@assertions_failed"/>
					<xsl:value-of select="@expected_failures"/>
					<div class="Messages">
						<xsl:apply-templates select="FatalError|Error|Info|Message"/>
					</div>
					<xsl:apply-templates select="TestingTime"/>
				</div>
			</xsl:when>
			<xsl:when test="$isPassed">
				<div class="TestCase Passed">
					<div class="Name"><xsl:value-of select="@name"/></div>
					<xsl:value-of select="@result"/>
					<xsl:value-of select="@assertions_passed"/>
					<xsl:value-of select="@assertions_failed"/>
					<xsl:value-of select="@expected_failures"/>
					<div class="Messages">
						<xsl:apply-templates select="FatalError|Error|Info|Message"/>
					</div>
					<xsl:apply-templates select="TestingTime"/>
				</div>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	
		<!-- Fatal error -->
	<xsl:template match="FatalError">
		<div class="Error">
			<span class="Message">
				<xsl:attribute name="title">
					<xsl:value-of select="@file"/> at line <xsl:value-of select="@line"/>
				</xsl:attribute>
				<xsl:value-of select="."/>
			</span>
		</div>
	</xsl:template>
	
	<!-- Error -->
	<xsl:template match="Error">
		<div class="Error">
			<span class="Message">
				<xsl:attribute name="title">
					<xsl:value-of select="@file"/> at line <xsl:value-of select="@line"/>
				</xsl:attribute>
				<xsl:value-of select="."/>
			</span>
		</div>
	</xsl:template>
	
	<!-- Info -->
	<xsl:template match="Info">
		<div class="Info">
			<span class="Message">
				<xsl:attribute name="title">
					<xsl:value-of select="@file"/> at line <xsl:value-of select="@line"/>
				</xsl:attribute>
				<xsl:value-of select="."/>
			</span>
		</div>
	</xsl:template>
	
	<!-- Message -->
	<xsl:template match="Message">
		<div class="Message">
			<span class="Message">
				<xsl:attribute name="title">
					<xsl:value-of select="@file"/> at line <xsl:value-of select="@line"/>
				</xsl:attribute>
				<xsl:value-of select="."/>
			</span>
		</div>
	</xsl:template>
	
	<!-- Testing time -->
	<xsl:template match="TestingTime">
		<xsl:variable name="testingTime" select="."/>
		<xsl:variable name="testingTimeString">
			<xsl:choose>
				<xsl:when test="$testingTime = 0"> negligible time</xsl:when>
				<xsl:when test="$testingTime &lt; 1000000">≈<xsl:value-of select="$testingTime div 1000"/> ms</xsl:when>
				<xsl:otherwise>≈<xsl:value-of select="$testingTime div 1000000"/> s</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		
		<div class="TestingTime">
			Ran in <xsl:value-of select="$testingTimeString"/>
		</div>
	</xsl:template>
</xsl:stylesheet>


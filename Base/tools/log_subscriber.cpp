/*******************************************************************************
*                                                                              *
*                               Kyoto University                               *
*                               Okuno laboratory                               *
*                                    Kappa                                     *
*                                                                              *
*******************************************************************************/

#include "log_subscriber.h"

namespace kappa {
	//_______________ Constants and enumerations
	
	
	//_______________ Static functions
	
	
	//_______________ Constructors
	LogSubscriber::LogSubscriber() {
		
	}
	
	//_______________ Destructors
	LogSubscriber::~LogSubscriber() {
		
	}
	
	//_______________ Public methods
	
	
	//_______________ Helpers
	
}

/******************************************************************************/


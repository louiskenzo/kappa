/*******************************************************************************
*                                                                              *
*                               Kyoto University                               *
*                               Okuno laboratory                               *
*                                    Kappa                                     *
*                                                                              *
*******************************************************************************/

#ifndef KAPPA_LOG_FORWARDER_H
#define KAPPA_LOG_FORWARDER_H

#include "log_subscriber.h"
#include "log_publisher.h"

namespace kappa {
	class LogForwarder : public virtual LogPublisher,
	                     public virtual LogSubscriber
	{
		public:
			//_______________ Constructors
			LogForwarder(Log::SeverityLevel level);
			
			//_______________ Destructors
			~LogForwarder();
			
			//_______________ Public methods
			void forwardFrom(       LogPublisher& new_source_to_forward);
			void stopForwardingFrom(LogPublisher& new_source_to_forward);
		
		private:
			//_______________ Helpers
			/// Implement LogSubscriber's method to forward messages as a LogPublisher
			void onLog(const std::string& msg, Log::SeverityLevel level);
			
			//_______________ Fields
			Log _log;
	};
}

#endif

/******************************************************************************/


/*******************************************************************************
*                                                                              *
*                               Kyoto University                               *
*                               Okuno laboratory                               *
*                                    Kappa                                     *
*                                                                              *
*******************************************************************************/

// Standard headers
#include <string>
#include <vector>
#include <iostream>
#include <sstream>
// Project headers
#include "log.h"
#include "log_subscriber.h"

namespace kappa {
	//_______________ Constants and enumerations
	
	
	//_______________ Static functions
	
	
	//_______________ Constructors
	Log::Log(SeverityLevel default_severity) 
		: _default_severity(default_severity),
		  _subscribers(),
		  _sinks(),
		  _backlog() {
		
	}
	
	//_______________ Destructors
	Log::~Log() {
		
	}
	
	//_______________ Public methods
	void Log::log(const std::ostream& msg) {
		log(msg, _default_severity);
	}
	
	void Log::log(const std::string& msg) {
		log(msg, _default_severity);
	}
	
	void Log::log(const std::ostream& msg, SeverityLevel level) {
		std::ostringstream string_msg;
		string_msg << msg;
		log(string_msg.str(),level);
	}
	
	void Log::log(const std::string& msg, SeverityLevel level) {
		if (numberSubscribers() > 0) {
			// Forward to all subscribers
			for (unsigned int i=0 ; i<_subscribers.size() ; ++i) {
				_subscribers[i]->onLog(msg, level);
			}
		
			// Send to all outgoing stream sinks (eg. std::cout)
			for (unsigned int i=0 ; i<_sinks.size() ; ++i) {
				(*_sinks[i]) << msg << std::endl;
			}
		} else {
			_backlog.push_back(std::make_pair(msg,level));
		}
	}
	
	void Log::log(const std::ostream& msg) const {
		log(msg, _default_severity);
	}
	
	void Log::log(const std::string& msg) const {
		log(msg, _default_severity);
	}
	
	void Log::log(const std::ostream& msg, SeverityLevel level) const {
		std::ostringstream string_msg;
		string_msg << msg;
		log(string_msg.str(),level);
	}
	
	void Log::log(const std::string& msg, SeverityLevel level) const {
		// Forward to all subscribers
		for (unsigned int i=0 ; i<_subscribers.size() ; ++i) {
			_subscribers[i]->onLog(msg, level);
		}
	
		// Send to all outgoing stream sinks (eg. std::cout)
		for (unsigned int i=0 ; i<_sinks.size() ; ++i) {
			(*_sinks[i]) << msg << std::endl;
		}
	}
	
	void Log::publishTo(LogSubscriber* new_subscriber) {
		_subscribers.push_back(new_subscriber);
		
		if (_subscribers.size() == 1) emptyBacklog();
	}
	
	void Log::publishTo(std::ostream* new_sink) {
		_sinks.push_back(new_sink);
		
		if (_subscribers.size() == 1) emptyBacklog();
	}
	
	void Log::stopPublishingTo(LogSubscriber* old_subscriber) {
		std::vector<LogSubscriber*>::iterator i = _subscribers.begin();
		
		while (i != _subscribers.end()) {
			if (*i == old_subscriber) {
				// Erase returns an iterator pointing to the next element, so we
				// just have to continue from that element in the next loop
				i = _subscribers.erase(i);
			} else {
				++i;
			}
		}
	}
	
	void Log::stopPublishingTo(std::ostream* old_sink) {
		std::vector<std::ostream*>::iterator i = _sinks.begin();
		
		while (i != _sinks.end()) {
			if (*i == old_sink) {
				// Erase returns an iterator pointing to the next element, so we
				// just have to continue from that element in the next loop
				i = _sinks.erase(i);
			} else {
				++i;
			}
		}
	}
	
	Log::SeverityLevel Log::getDefaultSeverity() const {
		return _default_severity;
	}
	
	void Log::setDefaultSeverity(SeverityLevel level) {
		_default_severity = level;
	}
	
	unsigned int Log::numberSubscribers() const {
		return _subscribers.size() + _sinks.size();
	}
	
	void Log::emptyBacklog() {
		for (unsigned int i=0 ; i<_backlog.size() ; ++i) {
			log(_backlog[i].first, _backlog[i].second);
		}
		
		_backlog.clear();
	}
}

/******************************************************************************/


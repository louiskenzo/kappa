/*******************************************************************************
*                                                                              *
*                               Kyoto University                               *
*                               Okuno laboratory                               *
*                                    Kappa                                     *
*                                                                              *
*******************************************************************************/

#ifndef KAPPA_LOG_H
#define KAPPA_LOG_H

#include <string>
#include <vector>

namespace kappa {
	// Forward declarations
	class LogSubscriber;	
	
	/// 
	class Log {
		public:
			//_______________ Types
			enum SeverityLevel {DEBUG, 
			                    INFO, 
			                    WARN, 
			                    ERROR, 
			                    FATAL
			                    };
			
			//_______________ Constants
			
			//_______________ Constructors
			Log(SeverityLevel default_severity = INFO);
			
			//_______________ Destructors
			~Log();
			
			//_______________ Public methods
			void log(const std::string&  msg);
			void log(const std::ostream& msg);
			void log(const std::string&  msg, SeverityLevel level);
			void log(const std::ostream& msg, SeverityLevel level);
			void log(const std::string&  msg) const;
			void log(const std::ostream& msg) const;
			void log(const std::string&  msg, SeverityLevel level) const;
			void log(const std::ostream& msg, SeverityLevel level) const;
			void publishTo(LogSubscriber* new_subscriber);
			void publishTo(std::ostream * new_sink);
			void stopPublishingTo(LogSubscriber* old_subscriber);
			void stopPublishingTo(std::ostream * old_sink);
			SeverityLevel getDefaultSeverity() const;
			void setDefaultSeverity(SeverityLevel level);
			unsigned int numberSubscribers() const;
		
		private:
			//_______________ Helpers
			void emptyBacklog();
			
			//_______________ Fields
			SeverityLevel               _default_severity;
			std::vector<LogSubscriber*> _subscribers;
			std::vector<std::ostream*>  _sinks;
			std::vector< std::pair<std::string,SeverityLevel> > _backlog;
	};
}

#endif

/******************************************************************************/


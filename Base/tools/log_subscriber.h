/*******************************************************************************
*                                                                              *
*                               Kyoto University                               *
*                               Okuno laboratory                               *
*                                    Kappa                                     *
*                                                                              *
*******************************************************************************/

#ifndef KAPPA_LOG_SUBSCRIBER_H
#define KAPPA_LOG_SUBSCRIBER_H

#include "log.h"

namespace kappa {
	// Forward declarations
	class Log;
	
	/// 
	class LogSubscriber {
		public:
			//_______________ Types
			
			
			//_______________ Constants
			
			
			//_______________ Constructors
			LogSubscriber();
			
			//_______________ Destructors
			~LogSubscriber();
			
			//_______________ Public methods
			virtual void onLog(const std::string& msg, Log::SeverityLevel level) = 0;
		
		private:
			//_______________ Helpers
			
			
			//_______________ Fields
			
	};
}

#endif

/******************************************************************************/


/*******************************************************************************
*                                                                              *
*                               Kyoto University                               *
*                               Okuno laboratory                               *
*                                    Kappa                                     *
*                                                                              *
*******************************************************************************/

#include <iostream>
#include "log_publisher.h"
#include "log_subscriber.h"
#include "log_forwarder.h"

namespace kappa {
	//_______________ Constructors
	LogForwarder::LogForwarder(Log::SeverityLevel level)
		: LogPublisher(level),
		  LogSubscriber() {
		
	}
	
	//_______________ Destructors
	LogForwarder::~LogForwarder() {
		
	}
	
	//_______________ Public methods
	void LogForwarder::forwardFrom(LogPublisher& new_source_to_forward) {
		new_source_to_forward.publishTo(this);
	}
	
	void LogForwarder::stopForwardingFrom(LogPublisher& new_source_to_forward) {
		new_source_to_forward.stopPublishingTo(this);
	}
	
	//_______________ Helpers
	void LogForwarder::onLog(const std::string& msg, Log::SeverityLevel level) {
		log(msg,level);
	}
	
}

/******************************************************************************/


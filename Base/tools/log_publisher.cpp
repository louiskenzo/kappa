/*******************************************************************************
*                                                                              *
*                               Kyoto University                               *
*                               Okuno laboratory                               *
*                                    Kappa                                     *
*                                                                              *
*******************************************************************************/

// Standard headers
#include <iostream>
#include <sstream>
// Boost headers
#include "boost/date_time/posix_time/posix_time.hpp"
// Project headers
#include "log_publisher.h"

using namespace boost::posix_time;

namespace kappa {
	//_______________ Constructors
	LogPublisher::LogPublisher(Log::SeverityLevel level)
		: _log(level) {
		
	}
	
	//_______________ Destructors
	LogPublisher::~LogPublisher() {
		
	}
	
	//_______________ Public methods
	void LogPublisher::publishTo(LogSubscriber* new_subscriber) {
		_log.publishTo(new_subscriber);
	}
	
	void LogPublisher::publishTo(std::ostream* new_sink) {
		_log.publishTo(new_sink);
	}
	
	void LogPublisher::stopPublishingTo(LogSubscriber* old_subscriber) {
		_log.stopPublishingTo(old_subscriber);
	}
	
	void LogPublisher::stopPublishingTo(std::ostream* old_sink) {
		_log.stopPublishingTo(old_sink);
	}
	
	void LogPublisher::log(const std::string& msg) {
		log(msg,_log.getDefaultSeverity());
	}
	
	void LogPublisher::log(const std::string& msg) const {
		log(msg,_log.getDefaultSeverity());
	}
	
	void LogPublisher::log(const std::ostream& msg) {
		log(msg,_log.getDefaultSeverity());
	}
	
	void LogPublisher::log(const std::ostream& msg) const {
		log(msg,_log.getDefaultSeverity());
	}
	
	void LogPublisher::log(const std::string& msg, Log::SeverityLevel level) {
		_log.log(msg,level);
	}
	
	void LogPublisher::log(const std::string& msg, Log::SeverityLevel level) const {
		_log.log(msg,level);
	}
	
	void LogPublisher::log(const std::ostream& msg, Log::SeverityLevel level) {
		std::ostringstream string_msg;
		string_msg << msg;
		std::string s = string_msg.str();
		log(s,level);
	}
	
	void LogPublisher::log(const std::ostream& msg, Log::SeverityLevel level) const {
		std::ostringstream string_msg;
		string_msg << msg;
		log(string_msg.str(),level);
	}
	
	void LogPublisher::logDuration(const ptime& from, const std::string& name) const {
		logDuration(from, name, _log.getDefaultSeverity());
	}
	
	void LogPublisher::logDuration(const ptime& from, const std::string& name, Log::SeverityLevel level) const {
		ptime now(microsec_clock::local_time());
		
		std::stringstream entry;
		time_duration elapsed = (now-from);
		
		entry << name << ": ";
		if (elapsed.seconds()>0) entry << elapsed.seconds() << " s ";
		entry << elapsed.fractional_seconds()/1000 << " ms";
		
		log(entry.str());
	}
	
	unsigned int LogPublisher::numberSubscribers() const {
		return _log.numberSubscribers();
	}
}

/******************************************************************************/


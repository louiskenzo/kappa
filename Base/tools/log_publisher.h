/*******************************************************************************
*                                                                              *
*                               Kyoto University                               *
*                               Okuno laboratory                               *
*                                    Kappa                                     *
*                                                                              *
*******************************************************************************/

#ifndef KAPPA_LOG_PUBLISHER_H
#define KAPPA_LOG_PUBLISHER_H

// Standard headers
#include <string>
#include <iostream>
// Boost headers
#include "boost/date_time/posix_time/posix_time.hpp"
// Project headers
#include "log.h"
#include "log_subscriber.h"

using namespace boost::posix_time;

namespace kappa {
	// Forward declarations
	class Log;
	
	class LogPublisher {
		public:
			//_______________ Constructors
			LogPublisher(Log::SeverityLevel level);
			
			//_______________ Destructors
			~LogPublisher();
			
			//_______________ Public methods
			// Bookkeeping for who to publish to
			void publishTo(LogSubscriber* new_subscriber);
			void publishTo(std::ostream*  new_sink);
			void stopPublishingTo(LogSubscriber* old_subscriber);
			void stopPublishingTo(std::ostream*  old_sink);
			// Logging functions from Log through composition
			void log(const std::string&  msg);
			void log(const std::string&  msg) const;
			void log(const std::ostream& msg);
			void log(const std::ostream& msg) const;
			virtual void log(const std::string& msg, Log::SeverityLevel level);
			virtual void log(const std::string& msg, Log::SeverityLevel level) const;
			void log(const std::ostream& msg, Log::SeverityLevel level);
			void log(const std::ostream& msg, Log::SeverityLevel level) const;
			// Additional logging functions
			void logDuration(const ptime& from, const std::string& name) const;
			void logDuration(const ptime& from, const std::string& name, Log::SeverityLevel) const;
			
			unsigned int numberSubscribers() const;
		
		protected:
			//_______________ Fields
			Log _log;
	};
}

#endif

/******************************************************************************/


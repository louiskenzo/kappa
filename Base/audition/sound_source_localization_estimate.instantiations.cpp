/*******************************************************************************
*                                                                              *
*                               Kyoto University                               *
*                               Okuno laboratory                               *
*                                    Kappa                                     *
*                                                                              *
*******************************************************************************/

#include "ppm.h"

namespace kappa {
	template class HalfPlaneSoundSourceLocalizationEstimate<PPM::Point,
	                                                        PPM::Vector,
	                                                        PPM::Plane>;
}

/******************************************************************************/


/*******************************************************************************
*                                                                              *
*                               Kyoto University                               *
*                               Okuno laboratory                               *
*                                    Kappa                                     *
*                                                                              *
*******************************************************************************/

// Standard headers
#include <cmath>
// Boost headers
#include <boost/math/constants/constants.hpp>
// Project headers
#include "sound_source_localization_estimate.h"

using boost::math::constants::pi;

namespace kappa {
	//_______________ Constructors
	template <typename Point,
	          typename Vector,
	          typename Plane>
	HalfPlaneSoundSourceLocalizationEstimate<Point,Vector,Plane>::
	HalfPlaneSoundSourceLocalizationEstimate(Point  auditive_center,
			                                 Vector front,
			                                 Vector up,
			                                 double variance) 
		: _auditive_center(auditive_center),
		  _localization_plane(Plane(_auditive_center,
		                            _auditive_center + front,
		                            _auditive_center + up)),
		  _front(front),
		  _up(up),
		  _likelihood_function(0,variance) {
		
	}
	
	template <typename Point,
	          typename Vector,
	          typename Plane>
	double
	HalfPlaneSoundSourceLocalizationEstimate<Point,Vector,Plane>::
	operator()(const Point& point) const {
		// Compute orthogonal projection of point on localization plane
		Point projection = _localization_plane.projection(point);
		
		// Compute angle of point to localization plane from auditive center
		Vector vector_to_point(_auditive_center,point);
		Vector vector_to_projection(_auditive_center,projection);
		double vector_to_point_length      = std::sqrt(vector_to_point.squared_length());
		double vector_to_projection_length = std::sqrt(vector_to_projection.squared_length());
		
		if (!(vector_to_point_length>0 && vector_to_projection_length>0)) std::cerr << _localization_plane << " | " << projection << std::endl;
		Vector unit_vector_to_point        = vector_to_point / vector_to_point_length;
		Vector unit_vector_to_projection   = vector_to_projection / vector_to_projection_length;
		
		double angle_to_plane = std::acos(std::max(std::min(unit_vector_to_point * unit_vector_to_projection,1.0),-1.0));
		
		// Arccos gives us the angle modulo pi, between 0 and +pi; we have to 
		// unwrap the result to obtain angles both positive and negative in the 
		// point-auditive_center-projection plane. Moreover because the angle is
		// between the point and its projection, it is never more than pi/2; we 
		// have to check if the point is in "opposite direction" of front 
		// relative to auditive_center.
		// The computed angle is always positive between 0 and pi/2.
		if (Vector(_auditive_center,point) * _front < 0) {
			angle_to_plane = pi<double>() - angle_to_plane;
		}
		if (_localization_plane.has_on_positive_side(point)) {
			angle_to_plane = -angle_to_plane;
		}
		
		/*std::cerr << "Angle of " << point << " to localization plane: " 
		          << (angle_to_plane * 180 / pi<double>()) << " degrees" 
		          << std::endl; // TODO TEMP*/
		
		// Compute likelihood of point using normal with zero mean and current
		// model of variance
		double likelihood = pdf(_likelihood_function, angle_to_plane);
		
		return likelihood;
	}
}

#include "sound_source_localization_estimate.instantiations.cpp"

/******************************************************************************/


/*******************************************************************************
*                                                                              *
*                               Kyoto University                               *
*                               Okuno laboratory                               *
*                                    Kappa                                     *
*                                                                              *
*******************************************************************************/

#ifndef KAPPA_SOUND_SOURCE_LOCALIZATION_ESTIMATE_H
#define KAPPA_SOUND_SOURCE_LOCALIZATION_ESTIMATE_H

// Boost headers
#include <boost/math/distributions/normal.hpp>

namespace kappa {
	template <typename Point>
	class SoundSourceLocalizationEstimate {
		public:
			//_______________ Public methods
			virtual double operator()(const Point& point) const = 0;
	};
	
	template <typename Point,
	          typename Vector,
	          typename Plane>
	class HalfPlaneSoundSourceLocalizationEstimate : public SoundSourceLocalizationEstimate<Point> {
		public:
			//_______________ Constructors
			HalfPlaneSoundSourceLocalizationEstimate(Point  auditive_center,
			                                         Vector front,
			                                         Vector up,
			                                         double variance);
			
			//_______________ Public methods
			double operator()(const Point& point) const;
		
		private:
			//_______________ Fields
			Point  _auditive_center;
			Plane  _localization_plane;
			Vector _front;
			Vector _up;
			boost::math::normal_distribution<double> _likelihood_function;
	};
}

#endif

/******************************************************************************/


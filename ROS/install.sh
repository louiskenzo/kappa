#!/bin/bash
################################################################################
#                                                                              #
#                               Kyoto University                               #
#                               Okuno laboratory                               #
#                                                                              #
################################################################################

############################## Printing utilities ##############################
function bold() {
	tput bold;  echo -n $@;tput sgr0
}

function underline() {
	tput smul;  echo -n $@;tput rmul
}

function blue() {
	tput setf 1;echo -n $@;tput sgr0
}

function green() {
	tput setf 2;echo -n $@;tput sgr0
}

function cyan() {
	tput setf 3;echo -n $@;tput sgr0
}

function red() {
	tput setf 4;echo -n $@;tput sgr0
}

function magenta() {
	tput setf 5;echo -n $@;tput sgr0
}

function yellow() {
	tput setf 6;echo -n $@;tput sgr0
}

############################# HARK-ROS installation ############################
# Find the number of cores on the machine for optimal use of make
n_core=$(cat /proc/cpuinfo | grep -m 1 siblings | sed 's/siblings.*: \(.*\)/\1/')

# Check that ROS is installed with the right packages
if [ -z "$(dpkg --list | grep -E " ros-(diamondback|cturtle)-ros ")" -o\
     -z "$(dpkg --list | grep -E " ros-(diamondback|cturtle)-driver-common ")" -o\
     -z "$(dpkg --list | grep -E " ros-(diamondback|cturtle)-openni-kinect ")" -o\
     -z "$(dpkg --list | grep -E " ros-(diamondback|cturtle)-desktop-full ")" -o\
     -z "$(dpkg --list | grep -E " ros-(diamondback|cturtle)-motion-planning-environment ")" -o\
     -z "$(dpkg --list | grep -E " ros-(diamondback|cturtle)-motion-planning-common ")" -o\
     -z "$(dpkg --list | grep -E " ros-(diamondback|cturtle)-arm-navigation ")" -o\
     -z "$(dpkg --list | grep -E " ros-(diamondback|cturtle)-kinematics ")" ]; then 
	echo "Could not find required ROS packages; do you want to install them now?"
	
	select answer in "yes" "no"; do
		case $REPLY in
			1 | yes | y) sudo apt-get install ros-diamondback-ros ros-diamondback-driver-common ros-diamondback-openni-kinect ros-diamondback-desktop-full ros-diamondback-motion-planning-environment ros-diamondback-motion-planning-common ros-diamondback-arm-navigation ros-diamondback-kinematics; break;;
			2 | no  | n) echo "Can't continur in the current state, missing packages; exiting..."; wait 3; exit 1;;
			*)           echo -e "$(red Couldn\'t understand your answer)";;
		esac
	done
fi

# Add the location of the new stack to environment variables
stacks_root="${ROS_ROOT}/../stacks"
# Use the USER_ROS_STACKS_ROOT variable as the stack root if present, or 
# initialize it
if test $USER_ROS_STACKS_ROOT; then
	stacks_root="$USER_ROS_STACKS_ROOT"
else
	echo "export USER_ROS_STACKS_ROOT=$stacks_root" >> ~/.bashrc
	. ~/.bashrc
fi
# Make sure the stacks root it owned by the user
user="$(whoami)"
sudo chown -R $user:$user "$stacks_root"

# Make sure the stacks root is created
if [ ! -d "$stacks_root" ]; then
	mkdir "$stacks_root"
fi

# Make sure the stacks root is in the path for next time
if [ "$(expr "$ROS_PACKAGE_PATH" : ".*$(echo "$stacks_root" | sed "s/\./\\./g").*")" -eq 0 ]; then
	echo "export ROS_PACKAGE_PATH=\${ROS_PACKAGE_PATH}:\$USER_ROS_STACKS_ROOT" >> ~/.bashrc
	. ~/.bashrc
fi

# Create the hark ros stack folder
hark_ros_root="$stacks_root/hark_ros"
if [ ! -d "$hark_ros_root" ]; then
	mkdir "$hark_ros_root"
fi
cd $hark_ros_root

# Add hark repository to APT sources so we can checkout the code
#sudo sh -c 'echo "deb-src http://winnie.kuis.kyoto-u.ac.jp/HARK/harkrepos lucid non-free" > /etc/apt/sources.list.d/hark.list'
#wget http://winnie.kuis.kyoto-u.ac.jp/HARK/harkrepos/public.gpg -O - | sudo apt-key add -
#sudo apt-get update

# Download the source code
#apt-get source hark-ros-stacks
#hark_ros_stacks_dir="$(basename "$(ls -d hark-ros-stacks-*/)")"

# Remove the archive and control files and move everything from the extraction
# folder into the current folder
#rm -f *.dsc *.tar.gz
#mv $hark_ros_stacks_dir/* .
#rm -rf $hark_ros_stacks_dir

# Compile hark ros stack
#./make-all-packages.sh

# Install libharkio
cd $hark_ros_root
svn co https://www.jp.honda-ri.com/harkrepos/trunk/HARK-System/libharkio libharkio
cd libharkio
./configure
make -j $n_core
sudo make install

# Install flowdesigner
cd $hark_ros_root
sudo apt-get install subversion build-essential cmake libperl-dev libqt4-dev libxml2-dev libssl-dev
sudo apt-get install libasound2-dev fftw-dev libcv-dev octave3.2-headers liblapack-dev libgnomeui-dev
svn co https://www.jp.honda-ri.com/harkrepos/trunk/HARK-System/flowdesigner-0.9.1-hark flowdesigner-0.9.1-hark
cd flowdesigner-0.9.1-hark
./configure
make clean
make -j $n_core
sudo make install

# Install hark-ros-stacks
cd $hark_ros_root
svn co https://www.jp.honda-ri.com/harkrepos/trunk/ROS/ros_stack_hri/hark_ros_stacks .
./hark_common_msgs/make-all-packages.sh
./harkd_common_msgs/make-all-packages.sh
./hark_stacks/make-all-packages.sh
./harkd_stacks/make-all-packages.sh

# Install librasp24
cd $hark_ros_root
svn co https://www.jp.honda-ri.com/harkrepos/trunk/HARK-Device/rasp24-1.0.0 rasp24-1.0.0
cd rasp24-1.0.0
./make-all.sh
sudo make install
sudo ln -s /usr/include/librasp24-netapi.h  /usr/include/librasp-netapi.h
sudo ln -s /usr/include/rasp24-netapi-arg.h /usr/include/rasp-netapi-arg.h
sudo ln -s /usr/include/rasp24-netapi-cmd.h /usr/include/rasp-netapi-cmd.h

# Install hark-fd
cd $hark_ros_root
svn co https://www.jp.honda-ri.com/harkrepos/trunk/HARK-System/hark-fd hark-fd
cd hark-fd
./configure --enable-rasp24
make -j $n_core
sudo make install

# Install hark-ros
cd $hark_ros_root
svn co https://www.jp.honda-ri.com/harkrepos/trunk/HARK-System/hark-ros hark-ros
cd hark-ros
./configure --with-hark-inc=$hark_ros_root/hark-fd/include --enable-ros
make -j $n_core
sudo make install

# Install hark-d
cd $hark_ros_root
svn co https://www.jp.honda-ri.com/harkrepos/trunk/HARK-System/hark-d hark-d
cd hark-d
./configure --with-hark-inc=$hark_ros_root/hark-fd/include --with-harkros-inc=$hark_ros_root/hark-ros/include --enable-ros
make -j $n_core
sudo make install

# Install harktool3
cd $hark_ros_root
sudo apt-get install qtcreator libboost-regex-dev libsndfile1-dev libboost-filesystem-dev
svn co https://www.jp.honda-ri.com/harkrepos/trunk/HARK-TOOLS/harktool3 harktool3
cd harktool3
qmake -r prefix=/usr/local
make -j $n_core
make install

# Install tftool
cd $hark_ros_root
svn co https://www.jp.honda-ri.com/harkrepos/trunk/HARK-TOOLS/tftool tftool
cd tftool
make -j $n_core
sudo install tsp2dat3D /usr/local/bin

# Cleanup all the compilation directories
#cd $hark_ros_root
#rm -rf tftool harktool3 hark-fd hark-ros hark-d hark_common_msgs harkd_common_msgs hark_stacks harkd_stacks flowdesigner-0.9.1-hark libharkio

# Compile Kappa-ROS modules
rosmake kappa
#rosmake ppm
#rosmake command_center
rosmake audition
#rosmake blackship
rosmake visualization
rosmake spatialization
#rosmake xsens

################################################################################


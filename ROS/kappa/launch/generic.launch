<!------------------------------------------------------------------------------
|                                                                              |
|                               Kyoto University                               |
|                               Okuno laboratory                               |
|                                    Kappa                                     |
|                                                                              |
------------------------------------------------------------------------------->

<launch>
	<!-- Obtain the name of the desired configuration from the command line, or use the default configuration -->
	<arg name    = "configuration" 
	     default = "HRI"/>
	<arg name    = "cfg-path" 
	     default = "$(find kappa)/launch"/>
	
	<!-- Machine name and connection configuration -->
	<include file="$(find kappa)/launch/machines.launch"/>
	
	<!-- Options -->
	<arg name    = "with-basler"      default = "false"/>
	<arg name    = "with-swissranger" default = "false"/>
	<arg name    = "with-kinect"      default = "false"/>
	<arg name    = "with-bluefox"     default = "false"/>
	
	<arg name    = "with-IMU"                 default = "false"/>
	<arg name    = "with-actuators"           default = "false"/>
	<arg name    = "with-audition"            default = "false"/>
	<arg name    = "with-visual-localization" default = "false"/>
	<arg name    = "with-physical-map"        default = "false"/>
	<arg name    = "with-spatialization"      default = "false"/>
	<arg name    = "with-command-center"      default = "false"/>
	<arg name    = "with-interface"           default = "false"/>
	<arg name    = "with-localization"        default = "false"/>
	<arg name    = "with-visualization"       default = "false"/>
	
	<!-- Node-machine allocation -->
	<arg name    = "basler-machine"      default = "self"/>
	<arg name    = "bluefox-machine"     default = "self"/>
	<arg name    = "swissranger-machine" default = "self"/>
	<arg name    = "kinect-machine"      default = "self"/>
	<arg name    = "IMU-machine"         default = "self"/>
	<arg name    = "rasp-machine"        default = "192.168.33.24"/>
	
	<arg name    = "actuators-machine"   default = "self"/>
	
	<arg name    = "audition-machine"    default = "self"/>
	<arg name    = "vision-machine"      default = "self"/>
	<arg name    = "memory-machine"      default = "self"/>
	
	<arg name    = "command-machine"     default = "self"/>
	<arg name    = "visualization-machine" default = "self"/>
	<arg name    = "interface-machine"   default = "self"/>
	
	<group ns="kappa">
		<!-- Sensors -->
		<group ns="sensors">
			<group ns="basler_cameras">
				<node name    = "left"
					pkg     = "camera1394"
					type    = "camera1394_node"
					machine = "$(arg basler-machine)"
					if      = "$(arg with-basler)">
					<remap from  = "camera" 
						  to   = "left"/>
					<param name  = "guid"
						 value = "3053000140d95f"/>
				</node>
			
				<node name    = "right"
					pkg     = "camera1394"
					type    = "camera1394_node"
					machine = "$(arg basler-machine)"
					if      = "$(arg with-basler)">
					<remap from  = "camera" 
					        to   = "right"/>
					<param name  = "guid"
						 value = "3053000140d95e"/>
				</node>
			</group>
			
			<group ns="bluefox_cameras">
				<node name    = "acquisition"
				      pkg     = "bluefox_camera_talker"
				      type    = "bluefox_camera_talker"
				      machine = "$(arg bluefox-machine)"
				      if      = "$(arg with-bluefox)">
					<!-- Remap left camera topics -->
					<remap from = "/camera/left/camera_info" 
						  to  = "/kappa/sensors/bluefox_cameras/left/camera_info"/>
					<remap from = "/camera/left/image" 
						  to  = "/kappa/sensors/bluefox_cameras/left/image"/>
					<remap from = "/camera/left/image/compressed" 
						  to  = "/kappa/sensors/bluefox_cameras/left/image/compressed"/>
					<remap from = "/camera/left/image/compressed/parameter_descriptions" 
						  to  = "/kappa/sensors/bluefox_cameras/left/image/compressed/parameter_descriptions"/>
					<remap from = "/camera/left/image/compressed/parameter_updates" 
						  to  = "/kappa/sensors/bluefox_cameras/left/image/compressed/parameter_updates"/>
					<remap from = "/camera/left/image/theora" 
						  to  = "/kappa/sensors/bluefox_cameras/left/image/theora"/>
					<remap from = "/camera/left/image/theora/parameter_descriptions" 
						  to  = "/kappa/sensors/bluefox_cameras/left/image/theora/parameter_descriptions"/>
					<remap from = "/camera/left/image/theora/parameter_updates" 
						  to  = "/kappa/sensors/bluefox_cameras/left/image/theora/parameter_updates"/>
						  
					<!-- Remap right camera topics -->
					<remap from = "/camera/right/camera_info" 
						  to  = "/kappa/sensors/bluefox_cameras/right/camera_info"/>
					<remap from = "/camera/right/image" 
						  to  = "/kappa/sensors/bluefox_cameras/right/image"/>
					<remap from = "/camera/right/image/compressed" 
						  to  = "/kappa/sensors/bluefox_cameras/right/image/compressed"/>
					<remap from = "/camera/right/image/compressed/parameter_descriptions" 
						  to  = "/kappa/sensors/bluefox_cameras/right/image/compressed/parameter_descriptions"/>
					<remap from = "/camera/right/image/compressed/parameter_updates" 
						  to  = "/kappa/sensors/bluefox_cameras/right/image/compressed/parameter_updates"/>
					<remap from = "/camera/right/image/theora" 
						  to  = "/kappa/sensors/bluefox_cameras/right/image/theora"/>
					<remap from = "/camera/right/image/theora/parameter_descriptions" 
						  to  = "/kappa/sensors/bluefox_cameras/right/image/theora/parameter_descriptions"/>
					<remap from = "/camera/right/image/theora/parameter_updates" 
						  to  = "/kappa/sensors/bluefox_cameras/right/image/theora/parameter_updates"/>
				</node>
				
				<node name    = "synchronization"
				      pkg     = "triggerboard_handler"
				      type    = "triggerboard_handler"
				      machine = "$(arg bluefox-machine)"
				      if      = "$(arg with-bluefox)">
					<remap from = "synchronization_node" to = "synchronization"/>
				</node>
			</group>
			
			<node name    = "swissranger"
			      pkg     = "swissranger_camera"
			      type    = "swissranger_camera"
			      machine = "$(arg swissranger-machine)"
			      if      = "$(arg with-swissranger)">
				<param name  = "guid"
				       value = "b09de5c0100626"/>
			</node>
			
			<node name    = "kinect"
			      pkg     = "openni_camera"
			      type    = "openni_node"
			      machine = "$(arg kinect-machine)"
			      if      = "$(arg with-kinect)">
				<remap from="camera"      to="kinect"/>
				<remap from="kinect_node" to="kinect"/>
				
				<!--
				<param name="camera_frame_id" value="/range_imaging_sensor" />
				<param name="rgb_frame_id"    value="/range_imaging_sensor" />
				<param name="depth_frame_id"  value="/range_imaging_sensor" />
				-->
			</node>
			
			<node name    = "xsens"
			      pkg     = "xsens"
			      type    = "xsens_node"
			      machine = "$(arg IMU-machine)"
			      if      = "$(arg with-IMU)">
			</node>
		</group>
		
		<!-- Actuators -->
		<group ns="actuators">
			<node name    = "motor"
			      pkg     = "blackship"
			      type    = "blackship_node"
			      machine = "$(arg actuators-machine)"
			      if      = "$(arg with-actuators)">
			</node>
		</group>
		
		<!-- Perception -->
		<group ns="perception">
			<node name    = "audition"
			      pkg     = "audition"
			      type    = "Kappa localization"
			      cwd     = "node"
			      args    = "$(arg rasp-machine)"
			      machine = "$(arg audition-machine)"
			      respawn = "true"
			      if      = "$(arg with-audition)">
				<remap from="/kappa/perception/HarkSource" to="/kappa/perception/auditive_localization/relative"/>
			</node>
			
			<node name    = "visual_localization"
			      pkg     = "vslam_lmgt"
			      type    = "vslam_lmgt"
			      args    = "-cfg=../Bluefox_calibration_20110822.cfg -calibkey=calib0,calib1,calibext -numlvl=4 -motion_thr=5 -max_track_pts=300 -local_bundle.size=7 -local_bundle.max_iter=20 -locrec.vtscore_thr=1.9 -locrec.num_test=10 -locrec.inlier_thr=5 -locrec.max_iter=100 -voctree=./voctree_surf64.bin -global_bundle.step=0 -global_bundle.loop=0 -debug.showfeaturedetails=1 -rectify -detect_max_disp=64 -cornerness_thr=1 -ransac.inlier_thr=5 -locrec.inlier_thr=3 -kf.motion_thr=0.5 -quit_gba=5 -kf.rotation_thr=10 -locrec.min_inlier=25 -debug_offset=30 -debug_offset_x=40 -debug_offset_y=30 -debug_scale=1 -cam_scale=2"
			      cwd     = "node"
			      machine = "$(arg vision-machine)"
			      if      = "$(arg with-visual-localization)"
			      respawn = "true">
				<remap from="/vslam/output"               to="/kappa/perception/visual_localization/relative"/>
				<remap from="/vslam/debug_array"          to="/kappa/perception/visual_localization/relative/debug_array"/>
				<remap from="/vslam/visualizations_array" to="/kappa/perception/visual_localization/relative/visualizations_array"/>
				<remap from="/camera/left/image"          to="/kappa/sensors/bluefox_cameras/left/image"/>
				<remap from="/camera/right/image"         to="/kappa/sensors/bluefox_cameras/right/image"/>
				<remap from="/taginfo/output"             to="/kappa/sensors/HRI_tag_localization_system"/>
			</node>
			
			<group ns="spatialization">
				<node name    = "spatialization"
					pkg     = "spatialization"
					type    = "spatialization_node"
					machine = "$(arg memory-machine)"
					if      = "$(arg with-spatialization)">
					<remap from = "perception/visual_localization/relative"   to = "/kappa/perception/visual_localization/relative"/>
					<remap from = "perception/visual_localization/global"     to = "/kappa/perception/visual_localization/global"/>
					<remap from = "perception/auditive_localization/relative" to = "/kappa/perception/HarkSource"/>
					<remap from = "perception/auditive_localization/global"   to = "/kappa/perception/auditive_localization/global"/>
					<remap from = "sensors/range_imaging"                     to = "/kappa/sensors/kinect/rgb/points"/>
					<remap from = "perception/depth/global"                   to = "/kappa/perception/depth/global"/>
				</node>
				
				<node name="vSLAM_frame_broadcaster"              pkg="tf" type="static_transform_publisher" args="  0.30  0.00 1.04 0 0 0 /global     /vSLAM                    100" />
				<node name="microphone_array_frame_broadcaster"   pkg="tf" type="static_transform_publisher" args="  0.00  0.00 0.35 0 0 0 /robot_base /microphone_array         100" />
				<node name="bluefox_cameras_frame_broadcaster"    pkg="tf" type="static_transform_publisher" args="  0.30  0.00 0.04 0 0 0 /robot_base /bluefox_cameras          100" />
				<node name="range_imaging_frame_broadcaster"      pkg="tf" type="static_transform_publisher" args="  0.30  0.20 0.05 0 0 0 /robot_base /range_imaging_sensor     100" />
				<node name="static_environment_model_broadcaster" pkg="tf" type="static_transform_publisher" args="-11.10 -2.70 0    0 0 0 /global     /static_environment_model 100" />
				
				<node pkg="tf" type="static_transform_publisher" name="kinect_base_link0" args="0  0    0  0    0  0    /range_imaging_sensor /openni_camera              100" />
				<node pkg="tf" type="static_transform_publisher" name="kinect_base_link"  args="0 -0.02 0  0    0  0    /openni_camera        /openni_depth_frame         100" />
				<node pkg="tf" type="static_transform_publisher" name="kinect_base_link1" args="0 -0.04 0  0    0  0    /openni_camera        /openni_rgb_frame           100" />
				<node pkg="tf" type="static_transform_publisher" name="kinect_base_link2" args="0  0    0 -1.57 0 -1.57 /openni_depth_frame   /openni_depth_optical_frame 100" />
				<node pkg="tf" type="static_transform_publisher" name="kinect_base_link3" args="0  0    0 -1.57 0 -1.57 /openni_rgb_frame     /openni_rgb_optical_frame   100" />
			</group>
		</group>
		
		<!-- Internal representation -->
		<group ns="memory">
			<!-- 
			<node name    = "symbolic_map_node"
			      ns      = "symbolic_map"
			      pkg     = ""
			      type    = ""
			      machine = ""
			      if      = "">
				
			</node>
			-->
			
			<node name    = "physical_map"
			      ns      = "physical_map"
			      pkg     = "ppm"
			      type    = "ppm_node"
			      machine = "$(arg memory-machine)"
			      respawn = "true"
			      if      = "$(arg with-physical-map)">
			      <!-- launch-prefix = "xterm -e gdb - -args" -->
				<param name = "node_root" value="$(find kappa)/"/>
				<remap from = "perception/sound/directional"   to = "/kappa/perception/sound/directional/global"/>
				<remap from = "perception/visual_localization" to = "/kappa/perception/visual_localization/global"/>
				<remap from = "perception/depth"               to = "/kappa/perception/depth/global"/>
			</node>
		</group>
		
		<!-- Interface -->
		<group ns="visualization">
			<node name    = "command_center"
			      pkg     = "command_center"
			      type    = "command_center_node"
			      machine = "$(arg command-machine)"
			      if      = "$(arg with-command-center)">
				<param name="node_root" value="$(find kappa)/"/>
			</node>
			
			<node name    = "visualization"
			      pkg     = "visualization"
			      type    = "visualization_node"
			      machine = "$(arg visualization-machine)"
			      if      = "$(arg with-visualization)">
				<remap from = "perception/auditive_localization/relative" to = "/kappa/perception/HarkSource"/>
				<remap from = "static_environment_model/marker"           to = "/kappa/visualization/static_environment_model/marker"/>
				<remap from = "auditive_localization_marker"              to = "/kappa/perception/auditive_localization/marker"/>
				<param name="static_environment_model_file" value="file://$(find visualization)/resources/officemodel/Office9_7/office.dae" />
			</node>
			
			<node name    = "robot_model_joint_state_publisher"
			      pkg     = "joint_state_publisher"
			      type    = "joint_state_publisher"
			      machine = "$(arg visualization-machine)"
			      if      = "$(arg with-visualization)">
			      <remap from="robot_description" to="/kappa/visualization/hearbo_model" />
				<param name="use_gui"     value="true" />
				<param name="source_list" value="" />
			</node>
			
			<node name    = "robot_model_state_publisher"
			      pkg     = "robot_state_publisher"
			      type    = "state_publisher"
			      machine = "$(arg visualization-machine)"
			      if      = "$(arg with-visualization)">
			      <remap from="robot_description" to="/kappa/visualization/hearbo_model" />
				<param name="tf_prefix"         value="/hearbo" />
			</node>
			
			<param name="hearbo_model" textfile="$(find visualization)/resources/hearbo/hearbo.urdf" />
			<node  name="hearbo_model_tf_broadcaster" pkg="tf" type="static_transform_publisher" args="0 0 -0.80 0 0 0 /robot_base /hearbo/cart 100" />
			
			<node name    = "rviz"
			      pkg     = "rviz"
			      type    = "rviz"
			      args    = "--display-config $(find visualization)/resources/RViz.vcg"
			      machine = "$(arg interface-machine)"
			      if      = "$(arg with-interface)">
			</node>
		</group>
	</group>
</launch>

<!----------------------------------------------------------------------------->
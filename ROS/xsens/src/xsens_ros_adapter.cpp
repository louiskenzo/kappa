/*******************************************************************************
*                                                                              *
*                               Kyoto University                               *
*                                Project Kappa                                 *
*                          Louis-Kenzo Furuya Cahier                           *
*                                                                              *
*******************************************************************************/

// Standard headers
#include <string>
#include <cstdlib>
#include <iostream>
#include <pthread.h>
#include <unistd.h>
// ROS headers
#include <ros/ros.h>
#include <ros/console.h>
#include "std_msgs/Empty.h"
#include "sensor_msgs/Imu.h"
// Kappa headers
#include "kappa/log.h"
#include "xsens_ros_adapter.h"

namespace ros_kappa {
	//____________________ Constructors
	XSensAdapter::XSensAdapter(int argc, char* argv[])
		: LogPublisher(kappa::Log::DEBUG),
		  _processing_thread(),
		  _ros_node(),
		  _some_subscription(),
		  _some_publisher() {
		usleep(100000); // Wait for a tenth of a second for rosconsole to setup
	}
	
	//____________________ Destructors
	XSensAdapter::~XSensAdapter() {
		log("XSens adapter shutting down", kappa::Log::INFO);
	}
	
	//____________________ Public methods
	void XSensAdapter::start() {
		log("Starting XSens adapter");
		// Start the processing thread
		log("Starting XSens adapter processing thread");
		pthread_create(&_processing_thread, // Thread structure
		               NULL,                // Thread properties
		               processingThread,    // Function to run
		               static_cast<void*>(this)); // Arguments
	}
	
	//____________________ Helpers
	void* XSensAdapter::processingThread(void* instance) {
		static_cast<XSensAdapter*>(instance)->processing();
		ros::spin();
		
		return EXIT_SUCCESS;
	}
	
	void XSensAdapter::processing() {
		// Subscribe
		_some_subscription = _ros_node.subscribe("/some_topic", 
		                                         1, &XSensAdapter::someHandler, this);
		
		// Advertise
		_some_publisher = _ros_node.advertise<sensor_msgs::Imu>("", 1000);
	}
	
	void XSensAdapter::someHandler(const std_msgs::Empty::ConstPtr& msg) {
		
	}
	
	void XSensAdapter::log(const std::string& msg, kappa::Log::SeverityLevel level) {
		switch (level) {
			case kappa::Log::DEBUG:
				ROS_DEBUG("%s", msg.c_str());
				break;
			case kappa::Log::INFO:
				ROS_INFO("%s", msg.c_str());
				break;
			case kappa::Log::WARN:
				ROS_WARN("%s", msg.c_str());
				break;
			case kappa::Log::ERROR:
				ROS_ERROR("%s", msg.c_str());
				break;
			case kappa::Log::FATAL:
				ROS_FATAL("%s", msg.c_str());
				break;
			default:
				break;
		}
	}
}

/******************************************************************************/


################################################################################
#                                                                              #
#                               Kyoto University                               #
#                               Okuno laboratory                               #
#                                Boomer project                                #
#                                                                              #
################################################################################

# Build in release mode
rosmake --pjobs=8 --threads=8 audition blackship command_center kappa ppm visualization xsens

################################################################################


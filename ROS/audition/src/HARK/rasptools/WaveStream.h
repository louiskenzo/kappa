/** @file WaveStream.h
 *  @brief 
 */
#if !defined(M_WAVE_STREAM_HEADER)
#define M_WAVE_STREAM_HEADER

#include <stdio.h>

typedef struct tag_FormatChunk {
    int chunkID;
    long chunkSize;
    short wFormatTag;
    unsigned short wChannels;
    unsigned long dwSamplesPerSec;
    unsigned long dwAvgBytesPerSec;
    unsigned short wBlockAlign;
    unsigned short wBitsPerSample;
} FormatChunk;

typedef struct tag_FactChunk {
} FactChunk;

typedef struct tag_DataChunk {
    int chunkID;
    long chunkSize;
    long readSize;
    long writeSize;
    fpos_t startPos;
} DataChunk;

typedef struct tag_ListChunk {
} ListChunk;

typedef struct tag_BufferInterface {
    void *misc;
    void (*read)(void *misc, int index, int data);
} BufferInterface;

/** @struct tag_WaveStream
 *  @brief Waveストリーム構造体
 */
typedef struct tag_WaveStream {
    FILE *fp;                /** ファイルポインタ */
    FormatChunk fmtChunk;    /** fmt チャンク構造体 */
    DataChunk dataChunk;     /** data チャンク構造体 */
} WaveStream;

/*------------------------- 関数 --------------------------*/
extern void set_FormatChunk(FormatChunk *pfc, unsigned short wChannels,
			    unsigned long dwSamplePerSec, unsigned short wBitsPerSample);
			
/*==========================================================
 *                    読み込み補助関数
 *=========================================================*/
extern long check_WaveHeader(FILE *fp);
extern int get_ChunkHeader(FILE *fp, char *name, long *chunkSize);
extern int read_FormatChunk(FormatChunk *pfc, FILE *fp, long chunkSize);
extern int read_DataChunk(DataChunk *pdc, FILE *fp, long chunkSize);
extern int get_channel_wave(double *wav_1ch, int n, double *wav_xch, int size, int ch, double scale);
extern int set_channel_wave(double *wav_xch, int n, double *wav_1ch, int size, int ch, double scale);

/*==========================================================
 *                    書き込み補助関数
 *=========================================================*/
extern int write_WaveHeader(FILE *fp, long dataSize);
extern int write_FormatChunk(FormatChunk *pfc, FILE *fp);

/*==========================================================
 *                 WaveStream オブジェクト
 *=========================================================*/
extern WaveStream *new_WaveStream(void);
extern void delete_WaveStream(WaveStream *pws);
extern WaveStream *new4Read_WaveStream(char *filename);
extern WaveStream *new4Write_WaveStream(char *filename, unsigned short wChannels,
			unsigned long dwSamplePerSec, unsigned short wBitsPerSample);
extern int delete4Read_WaveStream(WaveStream *ws);
extern int delete4Write_WaveStream(WaveStream *ws);
/*------------------- メンバアクセス関数 -----------------*/
extern int getChannel_WaveStream(WaveStream *pws);
extern int getSamplesPerSec_WaveStream(WaveStream *pws);
extern int getBitsPerSample_WaveStream(WaveStream *pws);
extern int getSamplePerCh_WaveStream(WaveStream *pws);

/*-------------------- ファイル処理関数 -------------------*/
extern int openRead_WaveStream(WaveStream *pws, char *filename);
extern int openWrite_WaveStream(WaveStream *pws, char *filename, unsigned short wChannels,
				unsigned long dwSamplePerSec, unsigned short wBitsPerSample);
extern int read2ShortArray_WaveStream(WaveStream *pws, short *vec, int vecNum);
extern int read2DoubleArray_WaveStream(WaveStream *pws, double *vec, int vecNum);
extern int write4ShortArray_WaveStream(WaveStream *pws, short *vec, int vecNum);
extern int write4DoubleArray_WaveStream(WaveStream *pws, double *vec, int vecNum);
extern int seek_WaveStream(WaveStream *ws, long offset, int origin);
extern int closeRead_WaveStream(WaveStream *pws);
extern int closeWrite_WaveStream(WaveStream *pws);

#endif

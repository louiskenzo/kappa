/** @file WaveStream.c
 *  @brief wave ファイル処理プログラム。
 *         処理の方針としては、データ部分を取り扱うことをメインにし、
 *         データは適宜ファイルから読み取ってくる。
 *         チャンクなどの情報はfmt以外は格納しない。
 *         途中でシークできるようにしてあるが、データチャンクの
 *         処理が対応してないので注意
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "WaveStream.h"
/*==========================================================
 *
 *              エンディアン変換関数(内部のみで使用)
 *                         ※ 適当
 *
 *=========================================================*/
static long getInt8(char *buf)
{
    return (unsigned char)buf[0];
}

static long getInt16(char *buf)
{
    return (buf[1] << 8) | (unsigned char)buf[0]; 
}

static long getInt24(char *buf)
{
    return (buf[2] << 16) |
	((unsigned char)buf[1] << 8) | (unsigned char)buf[0];
}

static long getInt32(char *buf)
{
    return (buf[3] << 24) | ((unsigned char)buf[2] << 16) |
	((unsigned char)buf[1] << 8) | (unsigned char)buf[0];
}

static void setChar8(char *buf, long data)
{
    buf[0] = ((unsigned int)data & 0x000000ff) >> 0;
}

static void setChar16(char *buf, long data)
{
    buf[0] = ((unsigned int)data & 0x000000ff) >> 0;
    buf[1] = ((unsigned int)data & 0x0000ff00) >> 8;
}

static void setChar24(char *buf, long data)
{
    buf[0] = ((unsigned int)data & 0x000000ff) >> 0;
    buf[1] = ((unsigned int)data & 0x0000ff00) >> 8; 
    buf[2] = ((unsigned int)data & 0x00ff0000) >> 16;
}

static void setChar32(char *buf, long data)
{
    buf[0] = ((unsigned int)data & 0x000000ff) >> 0;
    buf[1] = ((unsigned int)data & 0x0000ff00) >> 8; 
    buf[2] = ((unsigned int)data & 0x00ff0000) >> 16;
    buf[3] = ((unsigned int)data & 0xff000000) >> 24; 
}

/*===========================================================
 *
 *                      FormatChunk処理関数
 *
 *===========================================================*/
/** @fn void set_FormatChunk(FormatChunk *pfc, unsigned short wChannels,
    unsigned long dwSamplePerSec, unsigned short wBitsPerSample)
 *  @brief フォーマットチャンクをセットする
 *  @param pfc FormatChunk構造体へのポインタ
 *  @param wChannels チャンネル数
 *  @param dwSamplePerSec サンプリングレート
 *  @param wBitsPerSample 量子化ビット数
 */
void set_FormatChunk(FormatChunk *pfc, unsigned short wChannels,
		     unsigned long dwSamplePerSec,
		     unsigned short wBitsPerSample)
{
    pfc->chunkSize   = 16;
    pfc->wFormatTag  = 1;
    pfc->wChannels   = wChannels;
    pfc->dwSamplesPerSec = dwSamplePerSec;
    pfc->wBitsPerSample = wBitsPerSample;
    pfc->dwAvgBytesPerSec = dwSamplePerSec * wChannels * (wBitsPerSample / 8);
    pfc->wBlockAlign = wChannels * (wBitsPerSample / 8);
}

/*===========================================================
 *
 *                       読み込み処理関数
 *
 *===========================================================*/
/** @fn long check_WaveHeader(FILE *fp)
 *  @brief Wave ファイルであるかチェックする。異なる場合は-1、
 *         そうである場合は コレ以降のデータのサイズを返す。
 *         ファイルポインタはその分進む。
 *  @param fp ファイルポインタ
 */
long check_WaveHeader(FILE *fp)
{
    char buf[256];
    int readNum;
    long followSize;
    
    /* "RIFF"の文字チェック */
    readNum = fread(buf, sizeof(char), 4, fp);
    if (!(readNum == 4 &&
	  buf[0] == 'R' && buf[1] == 'I' &&
	  buf[2] == 'F' && buf[3] == 'F'))
	return -1;
    
    /* 全体のファイルサイズ */
    readNum = fread(buf, sizeof(char), 4, fp);
    if (!(readNum == 4))
	return -1;
    buf[4] = '\0';
    followSize = getInt32(buf);
    
    /* "WAVE"の文字のチェック */
    readNum = fread(buf, sizeof(char), 4, fp);
    if (!(readNum == 4 &&
	  buf[0] == 'W' && buf[1] == 'A' &&
	  buf[2] == 'V' && buf[3] == 'E'))
	return -1;
    
    return followSize;
}

/** @fn int get_ChunkHeader(FILE *fp, char *name, long *chunkSize)
 *  @brief チャンクの名前とサイズを得る
 *  @param fp ファイルポインタ
 *  @param name 名前格納先
 *  @param chunkSize サイズ格納先
 */
int get_ChunkHeader(FILE *fp, char *name, long *chunkSize)
{
    int readSize;
    char buf[16];
    
    /* チャンクの名前を読み取る */
    readSize = fread(name, sizeof(char), 4, fp);
    if (readSize != 4)
	return -1;
    name[4] = '\0';
    
    /* サイズの読み取り */
    readSize = fread(buf, sizeof(char), 4, fp);
    if (readSize != 4)
	return -1;
    *chunkSize = getInt32(buf);
    
    return 0;
}

/** @fn int read_FormatChunk(FormatChunk *pfc, FILE *fp, long chunkSize)
 *  @brief fmt チャンクを読み取る。ファイルポインタはその分進む。
 *  @param pfc fmt チャンク構造体へのポインタ
 *  @param fp チャンクサイズの後を指すファイルポインタ
 *  @param chunkSize このチャンクのサイズ
 *  @return 読み込み成功のとき 0、失敗の時-1を返す。
 */
int read_FormatChunk(FormatChunk *pfc, FILE *fp, long chunkSize)
{
    int readNum, dataSize;
    char buf[256];
    
    /* データサイズの保存 */
    pfc->chunkSize = chunkSize;
    
    /* フォーマットID の読み込み */
    readNum = fread(buf, sizeof(char), 2, fp);
    if (!(readNum == 2))	return -1;
    pfc->wFormatTag = (short)getInt16(buf);
    
    /* チャンネル数 */
    readNum = fread(buf, sizeof(char), 2, fp);
    if (!(readNum == 2))	return -1;
    pfc->wChannels = (unsigned short)getInt16(buf);
    
    /* サンプリングレート */
    readNum = fread(buf, sizeof(char), 4, fp);
    if (!(readNum == 4))	return -1;
    pfc->dwSamplesPerSec = (unsigned long)getInt32(buf);
   
    /* 1秒あたりのデータレート */
    readNum = fread(buf, sizeof(char), 4, fp);
    if (!(readNum == 4))	return -1;
    pfc->dwAvgBytesPerSec = (unsigned long)getInt32(buf);
    
    /* ブロックサイズ */
    readNum = fread(buf, sizeof(char), 2, fp);
    if (!(readNum == 2))	return -1;
    pfc->wBlockAlign = (unsigned short)getInt16(buf);
    
    /* 量子化ビット数 */
    readNum = fread(buf, sizeof(char), 2, fp);
    if (!(readNum == 2))	return -1;
    pfc->wBitsPerSample = (unsigned short)getInt16(buf);
    
    /* 拡張領域 */
    if (chunkSize > 16) {
    	/* よくワカランので読み飛ばす */
	fseek(fp, chunkSize - 16, SEEK_CUR);
    }
    
    return 0;
}

/** @fn int read_DataChunk(DataChunk *pdc, FILE *fp, long chunkSize)
 *  @brief データチャンクを読み取る。正確には実際にデータは読み込まず、
 *         startPos にデータ開始の位置を記憶するだけである。ファイルポインタは
 *         その分進む。
 *  @param pdc data チャンク構造体へのポインタ
 *  @param fp ファイルポインタ
 *  @param chunkSize チャンクサイズ
 */
int read_DataChunk(DataChunk *pdc, FILE *fp, long chunkSize)
{
    /* サイズをセット */
    pdc->chunkSize = chunkSize;
    
    /* 読み込み量をリセット */
    pdc->readSize = 0;
    
    /* データ開始位置をセット */
    if (fgetpos(fp, &(pdc->startPos)) != 0)
	return -1;
    
    /* 読み飛ばす */
    fseek(fp, chunkSize, SEEK_CUR);
    return 0;
}

/*==========================================================
 *
 *                     書き込み処理関数
 *
 *=========================================================*/
/** @fn int write_WaveHeader(FILE *fp, long dataSize)
 *  @brief 頭の部分だけ書き込む。正確にはWAVEの文字まで。
 *  @param fp ファイルポインタ
 *  @param dataSize データサイズ
 */
int write_WaveHeader(FILE *fp, long dataSize)
{
    char buf[256];
    
    /* "RIFF" の書き込み */
    fwrite("RIFF", sizeof(char), 4, fp);
    
    /* ファイルサイズの書き込み */
    setChar32(buf, dataSize);
    fwrite(buf, sizeof(char), 4, fp);
    
    /* "WAVE" の書き込み */
    fwrite("WAVE", sizeof(char), 4, fp);
    
    return 0;
}

/** @fn int write_FormatChunk(FormatChunk *pfc, FILE *fp)
 *  @brief Formatチャンクを書き込む。ファイルポインタはその分進む。
 *  @param pfc 書き込むfmtチャンク構造体へのポインタ
 *  @param fp ファイルポインタ
 */
int write_FormatChunk(FormatChunk *pfc, FILE *fp)
{
    char buf[256];
	
    /* "fmt " 書き込み*/
    fwrite("fmt ", sizeof(char), 4, fp);
    
    /* サイズの書き込み */
    setChar32(buf, pfc->chunkSize);
    fwrite(buf, sizeof(char), 4, fp);
    
    /* フォーマットIDの書き込み */
    setChar16(buf, pfc->wFormatTag);
    fwrite(buf, sizeof(char), 2, fp);
    
    /* チャンネル数の書き込み */
    setChar16(buf, pfc->wChannels);
    fwrite(buf, sizeof(char), 2, fp);
    
    /* サンプリングレートの書き込み */
    setChar32(buf, pfc->dwSamplesPerSec);
    fwrite(buf, sizeof(char), 4, fp);
    
    /* データレート */
    setChar32(buf, pfc->dwAvgBytesPerSec);
    fwrite(buf, sizeof(char), 4, fp);
    
    /* ブロックサイズ */
    setChar16(buf, pfc->wBlockAlign);
    fwrite(buf, sizeof(char), 2, fp);
    
    /* 量子化ビット数 */
    setChar16(buf, pfc->wBitsPerSample);
    fwrite(buf, sizeof(char), 2, fp);
    
    return 0;
}

/**
 *  @param wav_xch から n チャンネル目を取り出す
 */
int get_channel_wave(double *wav_1ch, int n, double *wav_xch, int size_1ch, int ch, double scale)
{
    int t;

    for (t = 0; t < size_1ch; t++) {
	wav_1ch[t] = wav_xch[n+t*ch] * scale;
    }
    return 0;
}

/**
 *  @param wav_xch の n チャンネル目に格納する
 */
int set_channel_wave(double *wav_xch, int n, double *wav_1ch, int size_1ch, int ch, double scale)
{
    int t;

    for (t = 0; t < size_1ch; t++) {
	wav_xch[n+t*ch] = wav_1ch[t] * scale;
    }
    return 0;
}


/*==========================================================
 *
 *                 WaveStream オブジェクト
 *
 *=========================================================*/
 
/*--------------------- 定義・宣言-------------------------*/
#define CHAR_BUF_SIZE 65536


/*--------------------- 基本関数 --------------------------*/

/** @fn WaveStream *new_WaveStream(void)
 *  @brief WaveStream を生成
 */
WaveStream *new_WaveStream(void)
{
    WaveStream *pws;
    
    pws = (WaveStream *)calloc(1, sizeof(WaveStream));
    
    return pws;
}

/** @fn void delete_WaveStream(WaveStream *pws)
 *  @brief WaveStream を破棄
 *  @param pws 対象オブジェクト
 */
void delete_WaveStream(WaveStream *pws)
{
    free(pws);
}

/** @fn WaveStream *new4Read_WaveStream(char *filename)
 *  @brief 読み込み用のWaveストリームオブジェクトを生成して開く。
 *  @param filename 読み込むWaveファイル
 *  @return ファイル読み込みエラー時にNULLを、
 *          それ以外はWaveStreamオブジェクトを返す。
 */
WaveStream *new4Read_WaveStream(char *filename)
{
    WaveStream *ws;
    
    ws = new_WaveStream();
    if (openRead_WaveStream(ws, filename) == -1)
	return NULL;
    return ws;
}

/** @fn WaveStream *new4Write_WaveStream(
 *   char *filename,
 *   unsigned short wChannels,
 *   unsigned long dwSamplePerSec,
 *   unsigned short wBitsPerSample)
 *  @brief 書き込み用のWaveストリームオブジェクトを生成して開く。
 *  @param filename 書き込み先ファイル名
 *  @param wChannels チャンネル数
 *  @param dwSamplePerSec サンプリングレート
 *  @param wBitsPerSample 量子化ビット数
 */
WaveStream *new4Write_WaveStream(char *filename, unsigned short wChannels,
				 unsigned long dwSamplePerSec,
				 unsigned short wBitsPerSample)
{
    WaveStream *ws;
    
    ws = new_WaveStream();
    if (openWrite_WaveStream(ws, filename, wChannels,
			     dwSamplePerSec,wBitsPerSample) == -1) {
	return NULL;
    }
    return ws;
}

/** @fn int delete4Read_WaveStream(WaveStream *ws)
 *  @brief 読み込み用WaveStreamオブジェクトを閉じて破棄する。
 *  @param ws WaveStream Object
 */
int delete4Read_WaveStream(WaveStream *ws)
{
    closeRead_WaveStream(ws);
    delete_WaveStream(ws);
    return 0;
}

/** @fn int delete4Write_WaveStream(WaveStream *ws)
 *  @brief 書き込み用WaveStream Object を閉じては破棄する。
 *  @param ws WaveStream Object
 */
int delete4Write_WaveStream(WaveStream *ws)
{
    closeWrite_WaveStream(ws);
    delete_WaveStream(ws);
    return 0;
}

/*------------------- メンバアクセス関数 -----------------*/

/** @fn int getChannel_WaveStream(WaveStream *pws)
 *  @brief チャンネル数を得る
 *  @param pws Wave ストリームオブジェクト
 */
int getChannel_WaveStream(WaveStream *pws)
{
    return pws->fmtChunk.wChannels;
}

/** @fn int getSamplesPerSec_WaveStream(WaveStream *pws)
 *  @brief サンプリングレートを得る
 *  @param pws Wave ストリームオブジェクト
 */
int getSamplesPerSec_WaveStream(WaveStream *pws)
{	
    return pws->fmtChunk.dwSamplesPerSec;
}

/** @fn int getBitsPerSample_WaveStream(WaveStream *pws)
 *  @brief 量子化ビット数を得る
 *  @param pws Wave ストリームオブジェクト
 */
int getBitsPerSample_WaveStream(WaveStream *pws)
{
    return pws->fmtChunk.wBitsPerSample;
}

/** @fn int getSamplePerCh_WaveStream(WaveStream *pws)
 *  @brief 単位チャネルあたりのデータサンプル数
 *  @param pws wave ストリームオブジェクト
 */
int getSamplePerCh_WaveStream(WaveStream *pws)
{
    return pws->dataChunk.chunkSize /
	(pws->fmtChunk.wChannels * (pws->fmtChunk.wBitsPerSample / 8));
}

/*-------------------- ファイル処理関数 -------------------*/
/** @fn int openRead_WaveStream(WaveStream *pws, char *filename)
 *  @brief Wave ファイルを読み込み用にオープンする。
 *  @param pws 情報格納先
 *  @param filename オープンするファイル名
 */
int openRead_WaveStream(WaveStream *pws, char *filename)
{
    long chunkSize;
    char chunkName[16];
    
    /* ファイルを開く */
    pws->fp = fopen(filename, "rb");
    if (pws->fp == NULL)
	return -1;
    
    /* ヘッダをチェック */
    if (check_WaveHeader(pws->fp) == -1)
	return -1;
    
    /* チャンクを処理する */
    while (1) {
	if (get_ChunkHeader(pws->fp, chunkName, &chunkSize) == -1) {
	    if (feof(pws->fp) != 0)
		break;
	    else
		return -1;
	}
	/*printf("[%s]: %d\n", chunkName, chunkSize);*/
	if (strcmp(chunkName, "fmt ") == 0) {
	    if (read_FormatChunk(&pws->fmtChunk, pws->fp, chunkSize) == -1)
		return -1;
	}
	else if (strcmp(chunkName, "data") == 0) {
	    if (read_DataChunk(&pws->dataChunk, pws->fp, chunkSize) == -1)
		return -1;
	}
	else {
	    fseek(pws->fp, chunkSize, SEEK_CUR);	
	}
    }
    
    /* データ開始位置にリセット */
    fsetpos(pws->fp, &(pws->dataChunk.startPos));
    
    return 0;
}

/** @fn int openWrite_WaveStream(WaveStream *pws, char *filename,
    unsigned short wChannels,
    unsigned long dwSamplePerSec, unsigned short wBitsPerSample)
 *  @brief Wave ファイルを書き込み用にオープンする
 *  @param pws 情報格納先
 *  @param filename ファイル名
 */
int openWrite_WaveStream(WaveStream *pws, char *filename,
			 unsigned short wChannels,
			 unsigned long dwSamplePerSec,
			 unsigned short wBitsPerSample)
{
    char buf[16];
    
    /* ファイルを開く */
    pws->fp = fopen(filename, "wb");
    if (pws->fp == NULL)
	return -1;
    
    /* 情報をセット */
    set_FormatChunk(&(pws->fmtChunk), wChannels,
		    dwSamplePerSec, wBitsPerSample);
    
    /* data まで書き込み */
    write_WaveHeader(pws->fp, 0);
    write_FormatChunk(&(pws->fmtChunk), pws->fp);
    
    /* dataヘッダの書き込み */
    fwrite("data", sizeof(char), 4, pws->fp);
    
    /* dataヘッダのサイズの書き込み */
    setChar32(buf, 0);
    fwrite(buf, sizeof(char), 4, pws->fp);
    
    /* データ開始位置をセット */
    if (fgetpos(pws->fp, &(pws->dataChunk.startPos)) != 0)
	return -1;
    
    return 0;
}

/** @fn int read2ShortArray_WaveStream(WaveStream *pws, short *vec, int vecNum)
 *  @brief short 型配列にvecNum 分のデータを読み込む。ただし、ファイルのデータ順
 *         に格納していくので、nチャンネルある場合は、1ch, 2ch, ..., nch, 1ch, 2ch,...
 *         と格納されていく。
 *  @param pws Waveストリームオブジェクト
 *  @param vec short型配列
 *  @param vecNum 格納するデータ数
 *  @return 読み込まれたデータ数。エラーなら-2。末端なら-1。
 */ 
int read2ShortArray_WaveStream(WaveStream *pws, short *vec, int vecNum)
{
    char buf[CHAR_BUF_SIZE], *pbuf;
    int i, readSize, readNum, bytes, frameNum, retNum;
    long (*getInt)(char*);
    
    if (pws->dataChunk.chunkSize == pws->dataChunk.readSize)
	return -1;
    
    /* 前処理 */
    bytes = pws->fmtChunk.wBitsPerSample / 8;
    if (bytes == 2) getInt = getInt16;
    else if (bytes == 3) getInt =getInt24;
    else getInt = getInt8;
    
    /* 残りデータが要求されている数よりも少ないとき */
    if ((pws->dataChunk.chunkSize - pws->dataChunk.readSize) / bytes < vecNum)
	vecNum = (pws->dataChunk.chunkSize - pws->dataChunk.readSize) / bytes;
    retNum = vecNum;
    
    readNum = 0;
    while (vecNum > 0) {
	/* char buf のサイズより大きいとき */
	if (vecNum > CHAR_BUF_SIZE / bytes)
	    frameNum = CHAR_BUF_SIZE / bytes;
	else
	    frameNum = vecNum;
	
	/* データを読み取る */
	readSize = fread(buf, sizeof(char), frameNum * bytes, pws->fp);
	if (frameNum * bytes != readSize) {
	    return -2;
	}
	for (i = 0, pbuf = buf; i < frameNum; i++) {
	    vec[readNum++] = (short)getInt(pbuf);
	    pbuf += bytes;
	}
	
	vecNum -= frameNum; 
    }
    
    /* 読み込み量の追加 */
    pws->dataChunk.readSize += retNum * bytes;
    
    return retNum;
}

/** @fn int read2DoubleArray_WaveStream(
 *          WaveStream *pws, double *vec, int vecNum)
 *  @brief double 型配列にvecNum 分のデータを読み込む。ただし、
 *         ファイルのデータ順に格納していくので、nチャンネルある
 *         場合は、1ch, 2ch, ..., nch, 1ch, 2ch,...
 *         と格納されていく。
 *  @param pws Waveストリームオブジェクト
 *  @param vec double型配列
 *  @param vecNum 格納するデータ数
 *  @return 読み込まれたデータ数。エラーなら-2。末端なら-1。
 */ 
int read2DoubleArray_WaveStream(WaveStream *pws, double *vec, int vecNum)
{
    char buf[CHAR_BUF_SIZE], *pbuf;
    int i, readSize, readNum, bytes, frameNum, retNum;
    long (*getInt)(char*);
    
    if (pws->dataChunk.chunkSize == pws->dataChunk.readSize)
	return -1;
    
    /* 前処理 */
    bytes = pws->fmtChunk.wBitsPerSample / 8;
    if (bytes == 2) getInt = getInt16;
    else if (bytes == 3) getInt = getInt24;
    else getInt = getInt8;
    
    /* 残りデータが要求されている数よりも少ないとき */
    if ((pws->dataChunk.chunkSize - pws->dataChunk.readSize) / bytes < vecNum)
	vecNum = (pws->dataChunk.chunkSize - pws->dataChunk.readSize) / bytes;
    retNum = vecNum;
    
    readNum = 0;
    while (vecNum > 0) {
	/* char buf のサイズより大きいとき */
	if (vecNum > CHAR_BUF_SIZE / bytes)
	    frameNum = CHAR_BUF_SIZE / bytes;
	else
	    frameNum = vecNum;
	
	/* データを読み取る */
	readSize = fread(buf, sizeof(char), frameNum * bytes, pws->fp);
	if (frameNum * bytes != readSize) {
	    return -2;
	}
	for (i = 0, pbuf = buf; i < frameNum; i++) {
	    vec[readNum++] = (double)getInt(pbuf);
	    pbuf += bytes;
	}
	
	vecNum -= frameNum; 
    }
    
    /* 読み込み量の追加 */
    pws->dataChunk.readSize += retNum * bytes;
    
    return retNum;
}

/** @fn int write4ShortArray_WaveStream(WaveStream *pws, short *vec, int vecNum)
 *  @brief vecの値をファイルに書き込む。nチャンネルある場合は、必ず1ch, 2ch, ..., nch
 *         のデータ順になっていなければならない。
 *  @param pws Waveストリームオブジェクト
 *  @param vec short型配列
 *  @param vecNum 書き込むデータ数
 *  @return 書き込まれたデータ数。エラーなら-1。
 */
int write4ShortArray_WaveStream(WaveStream *pws, short *vec, int vecNum)
{
    char buf[CHAR_BUF_SIZE], *pbuf;
    int i, writeSize, writeNum, bytes, frameNum, retNum;
    void (*setChar)(char*, long);
    
    /* 前処理 */
    bytes = pws->fmtChunk.wBitsPerSample / 8;
    if (bytes == 2) setChar = setChar16;
    else if (bytes == 3) setChar = setChar24;
    else setChar = setChar8;
    
    retNum = vecNum;
    writeNum = 0;
    while (vecNum > 0) {
	/* char buf のサイズより大きいとき */
	if (vecNum > CHAR_BUF_SIZE / bytes)
	    frameNum = CHAR_BUF_SIZE / bytes;
	else
	    frameNum = vecNum;
	
	/* バイト単位に変換 */
	for (i = 0, pbuf = buf; i < frameNum; i++, pbuf += bytes)
	    setChar(pbuf, (long)vec[writeNum++]);
	
	/* 一気に書き込み */
	fwrite(buf, sizeof(char), frameNum * bytes, pws->fp);
	
	vecNum -= frameNum; 
    }
    
    /* 書き込み量の追加 */
    pws->dataChunk.chunkSize += retNum * bytes;
    
    return retNum;
}

/** @fn int write4DoubleArray_WaveStream(WaveStream *pws, double *vec, int vecNum)
 *  @brief vecの値をファイルに書き込む。nチャンネルある場合は、必ず1ch, 2ch, ..., nch
 *         のデータ順になっていなければならない。
 *  @param pws Waveストリームオブジェクト
 *  @param vec double型配列
 *  @param vecNum 書き込むデータ数
 *  @return 書き込まれたデータ数。エラーなら-1。
 */
int write4DoubleArray_WaveStream(WaveStream *pws, double *vec, int vecNum)
{
    char buf[CHAR_BUF_SIZE], *pbuf;
    int i, writeSize, writeNum, bytes, frameNum, retNum;
    void (*setChar)(char*, long);
    
    /* 前処理 */
    bytes = pws->fmtChunk.wBitsPerSample / 8;
    if (bytes == 2) setChar = setChar16;
    else if (bytes == 3) setChar = setChar24;
    else setChar = setChar8;
    
    retNum = vecNum;
    writeNum = 0;
    while (vecNum > 0) {
	
	/* char buf のサイズより大きいとき */
	if (vecNum > CHAR_BUF_SIZE / bytes)
	    frameNum = CHAR_BUF_SIZE / bytes;
	else
	    frameNum = vecNum;
	
	/* バイト単位に変換 */
	for (i = 0, pbuf = buf; i < frameNum; i++, pbuf += bytes)
	    setChar(pbuf, (long)vec[writeNum++]);
	
	/* 一気に書き込み */
	fwrite(buf, sizeof(char), frameNum * bytes, pws->fp);
	
	vecNum -= frameNum;
	
    }
    
    /* 書き込み量の追加 */
    pws->dataChunk.chunkSize += retNum * bytes;
    
    return retNum;
}

/** @fn	int seek_WaveStream(WaveStream *ws, long offset, int origin)
 *  @brief データに関してシークする。
 *  @param ws WaveStream オブジェクト
 *  @param offset 移動するデータ数
 *  @param origin 検索位置
 */
int seek_WaveStream(WaveStream *ws, long offset, int origin)
{
    switch (origin) {
    case SEEK_SET:
	fsetpos(ws->fp, &(ws->dataChunk.startPos));
	break;
    case SEEK_CUR:
	fseek(ws->fp, offset * (ws->fmtChunk.wBitsPerSample / 8), SEEK_CUR);
	break;
    case SEEK_END:
	fsetpos(ws->fp, &(ws->dataChunk.startPos));
	fseek(ws->fp, ws->dataChunk.chunkSize, SEEK_CUR);
	break;
    }
    return 0;
}

static int read_WaveStream(WaveStream *pws, double *vec, int vecSize)
{
    int i, readSize, readNum = 0;
    char buf[CHAR_BUF_SIZE], *pbuf;
    
    if (vecSize > pws->dataChunk.chunkSize)
	vecSize = pws->dataChunk.chunkSize >> 1;
    readSize = fread(buf, sizeof(char), vecSize * 2, pws->fp);
    if (vecSize * 2 != readSize) {
	return -1;
    }
    for (i = 0, pbuf = buf; i < vecSize; i++) {
	vec[readNum++] = (double)getInt16(pbuf);
	pbuf += 2;
    }
    
    return readNum;
}

static int write_WaveStream(WaveStream *pws, double *vec, int vecSize)
{
    int i, writeSize = 0;
    char buf[65536], *pbuf;
    
    /*  */
    for (i = 0, pbuf = buf; i < vecSize; i++) {
	setChar16(pbuf, (int)vec[i]);
	pbuf += 2;
	writeSize += 2;
    }
    
    fwrite(buf, sizeof(char), writeSize, pws->fp);
    pws->dataChunk.chunkSize += writeSize;
    return 0;
}

/** @fn int closeRead_WaveStream(WaveStream *pws)
 *  @brief 読み込み用のストリームを閉じる
 *  @param ストリーム構造体
 */
int closeRead_WaveStream(WaveStream *pws)
{
    fclose(pws->fp);
    return 0;
}

/** @fn int closeWrite_WaveStream(WaveStream *pws)
 *  @brief 書き込み用のストリームを閉じる
 *  @param pws ストリーム構造体
 */
int closeWrite_WaveStream(WaveStream *pws)
{
    char buf[16];
    
    /* data のチャンクサイズの書き込み */
    fsetpos(pws->fp, &(pws->dataChunk.startPos));
    fseek(pws->fp, -4, SEEK_CUR); 
    setChar32(buf, pws->dataChunk.chunkSize);
    fwrite(buf, sizeof(char), 4, pws->fp);
    
    /* ファイルサイズの書き込み */
    setChar32(buf, pws->dataChunk.chunkSize + 8 + 24 + 4);
    fseek(pws->fp, 4, SEEK_SET);
    fwrite(buf, sizeof(char), 4, pws->fp);
    
    fclose(pws->fp);
    return 0;
}

/*
int main(int argc, char *argv[])
{
    int i, readNum;
    double vec[65536];
	WaveStream *ws, *copy;
	
	ws = new_WaveStream();
	
	
	openRead_WaveStream(ws, "../data/f101b001.wav");
	readNum = read2DoubleArray_WaveStream(ws, vec, 65536);
	//readNum = read_WaveStream(ws, vec, 65536);
	printf("%d %d\n", readNum, ws->dataChunk.readSize);
	for (i = 0; i < 10; i++) {
		printf("%e\n", vec[i]);
	}
	closeRead_WaveStream(ws);
	
	copy = new_WaveStream();
	openWrite_WaveStream(copy, "temp.wav", 1, 16000, 16);
	//write_WaveStream(copy, vec, readNum);
	write4DoubleArray_WaveStream(copy, vec, readNum);
	closeWrite_WaveStream(copy);
	
	delete_WaveStream(ws);
	delete_WaveStream(copy);
	
	return 0;
}
*/
#undef CHAR_BUF_SIZE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "rasplib.h"
#include "WaveStream.h"

#define SEGMENT_SIZE (512*16)

int parse_channel(int *ext_ch, char *str);

int main(int argc, char *argv[])
{
    int i, j, ch_mode, ch_skip, ch, freq, tmp, status, ext_ch[32];
    int ret_size = 0, write_size, dif_sample, stack_sample, rec_sample, i_time = 1;
    short int *wave_buf, *read_buf;
    float sec;
    char *hostname, *filename;
    WaveStream *pOutput;
    Rasp2 *pRasp2;

    if (argc < 6) {
	puts("Usage: rasprec HOSTNAME WAVEFILE FREQ SECOND CHANNEL \n"
	     "\n"
	     "  HOSTNAME   host address [char]\n"
	     "  WAVEFILE   output wave format file name [char]\n"
	     "  FREQ       sampling frequency [Hz]\n"
	     "  SECOND     recording time [sec]\n"
	     "  CHANNEL    target microphone channels [char]\n"
	     "             example: 0-1,3,5-8");
	return -1;
    }

    //---- 引き数
    hostname = argv[1];
    filename = argv[2];
    freq = atoi(argv[3]);
    sec  = atof(argv[4]);
    ch   = parse_channel(ext_ch, argv[5]);
    
    //---- チャンネル確認
    for (i = 0; i < ch; i++) {
	if (ext_ch[i] > 7) break;
    }
    if (i == ch ) {
	ch_mode = AD_8CH_MODE;
	ch_skip = 8;
    }
    else {
        ch_mode = AD_16CH_MODE;
	ch_skip = 16;
    }

    printf("Record as %d mode.\n", ch_skip);
    printf("Number of channel %d: ", ch);
    for (i = 0; i < ch; i++) {
	printf("%d ", ext_ch[i]);
    }
    printf("\n");

    //---- Rasp 生成
    if ((pRasp2 = new_Rasp2(RASP2_AD, hostname)) == NULL) {
	printf("cannot connect to '%s'.\n", hostname);
	return -1;
    }

    //---- wavefile 生成
    pOutput = new4Write_WaveStream(filename, ch, freq, 16);
    if (pOutput == NULL) {
	printf("no such file '%s'.\n", filename);
	return -1;
    }
    
    //---- メモリ確保
    read_buf = (short int *)calloc(sizeof(short int), SEGMENT_SIZE);
    wave_buf = (short int *)calloc(sizeof(short int), SEGMENT_SIZE);

    //---- 録音サンプル数
    rec_sample   = freq * sec;
    stack_sample = 0;

    //---- 録音
    if (startRec_Rasp2(pRasp2, freq, ch_mode) == 0) {
	while (stack_sample < rec_sample) {
	    //---- 指定サンプル数までのサンプル数
	    dif_sample = rec_sample - stack_sample;
	    if (dif_sample > SEGMENT_SIZE/ch_skip - 1)
		dif_sample = SEGMENT_SIZE/ch_skip - 1;
	    
	    //---- ブロック読み込み
	    ret_size = readSegment_RaspAD(&(pRasp2->ad_sock), read_buf, SEGMENT_SIZE);
	    //printf("%d\n", ret_size);
	    if (ret_size < 0) break;
	    stack_sample += ret_size/ch_skip - 1;
	    
	    //---- Wave 形式でコピー
	    write_size = 0;
	    for (i = 0; i < dif_sample; i++) {
		for (j = 0; j < ch; j++) {
		    wave_buf[write_size++] = read_buf[ext_ch[j] + (i+1) * ch_skip];
		}
	    }
	    write4ShortArray_WaveStream(pOutput, wave_buf, write_size);
	    
	    //---- 進捗の表示
	    if ((int)(stack_sample/(double)freq) >= i_time) {
		i_time++;
		printf(".");
		fflush(stdout);
	    }
	}
	printf("\n");
	//---- AD ストップ
	stop_Rasp2(pRasp2);
    }
    
    //---- 後片づけ
    free(read_buf);
    free(wave_buf);
    delete4Write_WaveStream(pOutput);
    delete_Rasp2(pRasp2);

    return 0;
}

// 簡易パーサ
int parse_channel(int *ext_ch, char *str)
{
    int i, j;
    int ch_num = 0, len, ch1, ch2;
    char l_str[1024], parse[256], *s_ptr;

    len = strlen(str);
    strcpy(l_str, str);

    s_ptr = l_str;
    
    for (i = 0; i < len+1; i++) {
	if (l_str[i] == '\0' || l_str[i] == ',') {
	    l_str[i] = '\0';
	    if (strlen(s_ptr) <= 2 && strlen(s_ptr) >= 1) {
		ext_ch[ch_num++] = atoi(s_ptr);
	    }
	    else if (strlen(s_ptr) > 2) {
		sscanf(s_ptr, "%d-%d", &ch1, &ch2);
		for (j = ch1; j <= ch2; j++) {
		    ext_ch[ch_num++] = j;
		}
	    }
	    else {
		break;
	    }
	    i++;
	    s_ptr = &l_str[i];
	}
    }
    
    return ch_num;
}

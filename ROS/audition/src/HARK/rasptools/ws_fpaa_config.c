/************************************************************
**
**     Object name : 
**
**     Public      : 
**
**     Private     : 
**
**     Description
**     -- -- -- -- -- -- -- --
**     WirelessSystemにおける制御クライアント
**     FPAAコンフィグサーバと通信
**
**     History
**     -- -- -- -- -- -- -- --
**     2007.01.09 K.Takagi FPAA_CONFIG_BUFFER_SIZEをFPAA_CONFIG_DATA_SIZEに修正
**     2006.12.25 K.Takagi inet_addrをinet_atonに変更
**                         入力引数の解析を修正
**     2006.12.08 K.Takagi 新規作成
**
**     Copyright (c) 2006
**     JEOL System Technology Co.,Ltd
**
*/


/*=========================================================*/
/*               Special Macro Definition                  */
/*=========================================================*/


/*=========================================================*/
/*                   Include Files                         */
/*=========================================================*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/io.h>
#include <netinet/in.h>
#include <arpa/inet.h>


#include "../rasptools/ws_common.h"


/*=========================================================*/
/*                   Macro Definition                      */
/*=========================================================*/

#ifdef DEBUG

#define DEBUG_PRINT printf

#else

#define DEBUG_PRINT(...)  

#endif


/*=========================================================*/
/*                 Structure Declaration                   */
/*=========================================================*/

/*=========================================================*/
/*                    Private Valiable                     */
/*=========================================================*/

/*=========================================================*/
/*                    Public Valiable                      */
/*=========================================================*/




/************************************************************
**
**  Public  : 
**
**  History : 2006.12.08 K.Takagi 新規作成
**
**  Description
**  -----------
**  サーバと通信してFPAAをコンフィグする
**
*/

int main(int argc, char *argv[])
{
    int  count;

    int  cmd;
    int  status;
    char buffer[CMD_STRING_LENGTH];

    char *fpaa_config_file;
    FILE *fpaa_fp;
    char *fpaa_config_buffer;
    int   fpaa_config_size;

    char *ip_addr;
    struct sockaddr_in srv_addr;
    int socket_addr_size;
    int socket_fd;

    struct in_addr server_ip_addr;
    

    /* 引数の解析 */
    ip_addr          = NULL;
    fpaa_config_file = NULL;
    for(count=1; count<argc-1; count++)
    {
        if(argv[count][0] == '-')
        {
            switch( argv[count][1] )
            {
                
                /* ヘルプ表示 */
                case 'h':
                {
                    printf("Usage : %s -i <Server IP> -f <FPAA Config Data File>\n", argv[0]);
                    return 0;
                }
                break;
                
                /* サーバのIPアドレス */
                case 'i':
                {
                    ip_addr = argv[count+1];
                    count++;
                }
                break;
                
                /* FPAAコンフィグ用ファイル */
                case 'f':
                {
                    fpaa_config_file = argv[count+1];
                    count++;
                }
                break;
                
                default:
                {
                }
                break;
                
            } /* End of switch() */

        }

    }

    /* 引数を確認 */
    if((ip_addr == NULL) || (fpaa_config_file == NULL))
    {
        printf("Usage : %s -i <Server IP> -f <FPAA Config Data File>\n", argv[0]);
        return -1;
    }
    

    /* 制御サーバのIPアドレスを変換 */
    status = inet_aton(ip_addr, &server_ip_addr);
    if(status == 0)
    {
        printf("Server IP Address Error.\n");
        printf("  %s ??\n", ip_addr);
        return -1;
    }


    /* コンフィグデータファイルを開く */
    fpaa_fp = fopen(fpaa_config_file, "rb");
    if(fpaa_fp == NULL)
    {
        printf("FPAA Config File Open Error.\n");
        return -1;
    }

    /* コンフィグデータ用のバッファを確保 */
    fpaa_config_buffer = (char *)malloc(FPAA_CONFIG_DATA_SIZE * sizeof(char));
    if(fpaa_config_buffer == NULL)
    {
        printf("FPAA Config Data Buffer Allocate Error.\n");
        fclose(fpaa_fp);

        return -1;
    }
    memset(fpaa_config_buffer, 0, FPAA_CONFIG_DATA_SIZE * sizeof(char));
    
    
    /* コンフィグデータ読み込み */
    fpaa_config_size = fread(fpaa_config_buffer, sizeof(char), FPAA_CONFIG_DATA_SIZE, fpaa_fp);
    
    fclose(fpaa_fp);

    if(fpaa_config_size <= 0)
    {
        printf("FPAA Config Data Read Error.\n");
        free(fpaa_config_buffer);
        
        return -1;
    }
    printf("FPAA Config Data Size = %d\n", fpaa_config_size);
    fflush(stdout);


    /* ソケットの作成 */
    socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if(socket_fd == -1)
    {
        printf("socket() Error.\n");
        free(fpaa_config_buffer);

        return -1;
    }


    /*  ソケットの構造体を初期化 */
    socket_addr_size = sizeof(struct sockaddr_in);

    srv_addr.sin_family      = AF_INET;
    srv_addr.sin_port        = htons(WS_FPAA_PORT);
    srv_addr.sin_addr.s_addr = server_ip_addr.s_addr;
    
    
    /* サーバに接続 */
    status = connect(socket_fd, (struct sockaddr *)&srv_addr, socket_addr_size);
    if(status == -1)
    {
        printf("connect() Error.\n");
        free(fpaa_config_buffer);
        close(socket_fd);

        return -1;
    }
    printf("Connect FPAA Config Server.\n");
    fflush(stdout);



    /*▼▼▼ サーバにコンフィグデータサイズを通知 ここから ▼▼▼*/
    printf("Send FPAA Config Data Size.\n");
    fflush(stdout);

    /* コンフィグデータサイズを通知 */
    sprintf(buffer, "0x%08x %d", WS_CMD_SET_FPAA_CFG_DATA_SIZE, fpaa_config_size);
    write(socket_fd, buffer, strlen(buffer)+1);
    
    DEBUG_PRINT("Send Msg    : %s\n", buffer);
    
    /* ステータス受信 */
    read(socket_fd, buffer, CMD_STRING_LENGTH);
    sscanf(buffer, "0x%x %d", &cmd, &status);
    
    DEBUG_PRINT("Recieve Msg : %s\n", buffer);
    
    if(status != STATUS_OK)
    {
        printf("WS_CMD_SET_FPAA_CFG_DATA_SIZE Error [0x%08x].\n", status);
        goto FPAA_CFG_END;
    }

    /*▲▲▲ サーバにコンフィグデータサイズを通知 ここまで ▲▲▲*/

    
    /*▼▼▼ サーバにコンフィグデータを転送 ここから ▼▼▼*/
    printf("Send FPAA Config Data.\n");
    fflush(stdout);

    /* コンフィグデータ送信 */
    write(socket_fd, fpaa_config_buffer, fpaa_config_size);
    
    /* ステータス受信 */
    read(socket_fd, buffer, CMD_STRING_LENGTH);
    sscanf(buffer, "0x%x %d", &cmd, &status);
    
    DEBUG_PRINT("Recieve Msg : %s\n", buffer);
    
    if(status != STATUS_OK)
    {
        printf("FPAA Config Data Send Error [0x%08x].\n", status);
        goto FPAA_CFG_END;
    }
    
    /*▲▲▲ サーバにコンフィグデータを転送 ここまで ▲▲▲*/


    /*▼▼▼ サーバにコンフィグ命令を通知 ここから ▼▼▼*/
    printf("FPAA Config Start.\n");
    fflush(stdout);

    /* コンフィグ命令 */
    sprintf(buffer, "0x%08x", WS_CMD_FPAA_CFG);
    write(socket_fd, buffer, strlen(buffer)+1);
    
    DEBUG_PRINT("Send Msg    : %s\n", buffer);
    
    /* ステータス受信 */
    read(socket_fd, buffer, CMD_STRING_LENGTH);
    sscanf(buffer, "0x%x %d", &cmd, &status);
    
    DEBUG_PRINT("Recieve Msg : %s\n", buffer);
    
    if(status != STATUS_OK)
    {
        printf("WS_CMD_FPAA_CFG Error [0x%08x].\n", status);
        goto FPAA_CFG_END;
    }
    printf("FPAA Config Done.\n");
    fflush(stdout);

    /*▲▲▲ サーバにコンフィグ命令を通知 ここまで ▲▲▲*/

    
 FPAA_CFG_END:

    /* 通信の終了処理 */
    close(socket_fd);

    free(fpaa_config_buffer);

    return 0;
}


/* EOF */

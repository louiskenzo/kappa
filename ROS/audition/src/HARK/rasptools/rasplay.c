#include <stdio.h>
#include <stdlib.h>
#include "rasplib.h"
#include "WaveStream.h"

#define BUF_SIZE 8192

int main(int argc, char *argv[])
{
    char *wavefile, *hostname;
    int freq, ch, ret_size, status, i;
    short int *wave_buf;
    WaveStream *pInput;
    Rasp2 *pRasp2;

    //---- 引き数
    if (argc < 3) {
	puts("Usage: rasplay HOSTNAME WAVEFILE\n"
	     "");
	return -1;
    }
    hostname = argv[1];
    wavefile = argv[2];

    //----- Rasp2 生成
    if ((pRasp2 = new_Rasp2(RASP2_DA, hostname)) == NULL) {
	printf("cannot connect to '%s'.\n", hostname);
	return -1;
    }

    //----- wavefile 読み込み
    pInput = new4Read_WaveStream(wavefile);
    if (pInput == NULL) {
	printf("no such file '%s'\n", wavefile);
	return -1;
    }
    freq = getSamplesPerSec_WaveStream(pInput);
    ch   = getChannel_WaveStream(pInput);

    //----- バッファ確保 と 8192byte 先読み
    wave_buf = (short int *)calloc(sizeof(short int), BUF_SIZE);
    ret_size = read2ShortArray_WaveStream(pInput, wave_buf, BUF_SIZE);

    //----- 再生
    if (ret_size == BUF_SIZE && startPly_Rasp2(pRasp2, freq, wave_buf, BUF_SIZE) == 0) {
	while (1) {
	    ret_size = read2ShortArray_WaveStream(pInput, wave_buf, BUF_SIZE);
	    if (ret_size <= 0) break;
	    write_Rasp2(pRasp2, wave_buf, ret_size * sizeof(short int));
	}
	stop_Rasp2(pRasp2);
    }
    
    //----- 後処理
    free(wave_buf);
    delete4Read_WaveStream(pInput);
    delete_Rasp2(pRasp2);

    return 0;
}

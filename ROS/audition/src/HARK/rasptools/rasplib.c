#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/uio.h>
#include <sys/io.h>
#include <sys/ioctl.h>
#include <sys/select.h>
#include <unistd.h>
#include <sys/param.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "rasplib.h"

/************************************************************
 *
 *                          汎関数
 *
 ************************************************************/
int connect_to_rasp2(unsigned short port, char *host)
{
    int c_sock;
    struct hostent *servhost; 
    struct sockaddr_in server;

    servhost = gethostbyname(host);
    if (servhost == NULL ){
        fprintf(stderr, "[%s] is not found.\n", host);
        return 0;
    }
    
    bzero(&server, sizeof(server));
    
    bcopy(servhost->h_addr, &server.sin_addr, servhost->h_length);
    server.sin_family = AF_INET;
    server.sin_port   = htons(port);
    
    if ((c_sock = socket(AF_INET, SOCK_STREAM, 0)) < 0 ) {
        fprintf(stderr, "failed in creating socket.\n");
        return -1;
    }
    
    if (connect(c_sock, (struct sockaddr *)&server, sizeof(server)) == -1) {
        fprintf(stderr, "failed in connecting server\n");
        return -1;
    }

    return c_sock;
}

int ctrl_rasp2(int socket_fd, int cmd, int *arg)
{
    int  status;
    int  value;
    char buffer[CMD_STRING_LENGTH*2];
    
    value = 0;
    if(arg != NULL) {
        value = *arg;
    }

    sprintf(buffer, "0x%08x %d", cmd, value);
    //printf("Send: %s\n", buffer);
    //DEBUG_PRINT("Send : %s\n", buffer);

    write(socket_fd, buffer, strlen(buffer)+1);

    read(socket_fd, buffer, CMD_STRING_LENGTH);
    
    sscanf(buffer, "%*s %d %d", &status, &value);
    
    if(arg != NULL) {
        *arg = value;
    }
    //printf("Recv: %s\n", buffer);
    //DEBUG_PRINT("Recv : %s\n", buffer);

    return status;
}

int ctrl2_rasp2(int socket_fd, int cmd, int *arg)
{
    int  status, last_status;
    int  value;
    char buffer[CMD_STRING_LENGTH*2];
    fd_set fd_mask;
    
    value = 0;
    if(arg != NULL) {
        value = *arg;
    }
    FD_ZERO(&fd_mask);
    FD_SET(0, &fd_mask);
    FD_SET(socket_fd, &fd_mask);

    sprintf(buffer, "0x%08x %d", cmd, value);

    write(socket_fd, buffer, strlen(buffer)+1);

    select(socket_fd+1, (fd_set*)&fd_mask, NULL, NULL, NULL);
    if (FD_ISSET(0, &fd_mask)) {
	exit(0);
    }

    read(socket_fd, buffer, CMD_STRING_LENGTH);
    
    sscanf(buffer, "%*s %d %d", &status, &value);
    
    if(arg != NULL) {
        *arg = value;
    }
    //printf("Recv: %s\n", buffer);
    //DEBUG_PRINT("Recv : %s\n", buffer);

    return status;
}

/**
 *  @brief port は WS_FPAA_PORT
 */
int set_fpaa_config(unsigned int port, char *hostname, char *filename)
{
    int  cmd, status, fpaa_config_size, socket_fd;
    char buffer[CMD_STRING_LENGTH], fpaa_config_buffer[FPAA_CONFIG_DATA_SIZE*4];
    FILE *pFpaa;

    //------
    pFpaa = fopen(filename, "rb");
    if (pFpaa == NULL) {
	printf("cannot open file '%s'.\n", filename);
	return -1;
    }

    fpaa_config_size = fread(fpaa_config_buffer, sizeof(char), FPAA_CONFIG_DATA_SIZE, pFpaa);
    fclose(pFpaa);
    
    if(fpaa_config_size > FPAA_CONFIG_DATA_SIZE) {
        printf("FPAA Config Data Size Error.\n");
        return -1;
    }

    //-------
    socket_fd = connect_to_rasp2(port, hostname);
    if (socket_fd < 0) {
	printf("cannot connect to '%s'\n", hostname);
	return -1;
    }
    
    sprintf(buffer, "0x%08x %d", WS_CMD_SET_FPAA_CFG_DATA_SIZE, fpaa_config_size);
    write(socket_fd, buffer, strlen(buffer)+1);
    
    read(socket_fd, buffer, CMD_STRING_LENGTH);
    sscanf(buffer, "0x%x %d", &cmd, &status);
    
    if(status != STATUS_OK) {
        printf("WS_CMD_SET_FPAA_CFG_DATA_SIZE Error [0x%08x].\n", status);
	close(socket_fd);
        return -1;
    }
    
    write(socket_fd, fpaa_config_buffer, fpaa_config_size);
    read(socket_fd, buffer, CMD_STRING_LENGTH);
    sscanf(buffer, "0x%x %d", &cmd, &status);
    
    if(status != STATUS_OK) {
        printf("FPAA Config Data Send Error [0x%08x].\n", status);
	close(socket_fd);
        return -1;
    }
    
    sprintf(buffer, "0x%08x", WS_CMD_FPAA_CFG);
    write(socket_fd, buffer, strlen(buffer)+1);

    read(socket_fd, buffer, CMD_STRING_LENGTH);
    sscanf(buffer, "0x%x %d", &cmd, &status);
    
    if(status != STATUS_OK) {
        printf("WS_CMD_FPAA_CFG Error [0x%08x].\n", status);
	close(socket_fd);
        return -1;
    }
    close(socket_fd);

    return 0;
}

/***********************************************************
 *
 *                         RaspSock
 *
 ***********************************************************/
int initialize_RaspSocket(RaspSocket *p_sock,
			  unsigned int ctrl_port, unsigned int data_port, char *host)
{
    int t_sock;

    // 制御用ソケット
    if ((t_sock = connect_to_rasp2(ctrl_port, host)) == -1) {
	return -1;
    }
    else {
	p_sock->sock_ctrl = t_sock;
    }
    
    // データ用ソケット
    if ((t_sock = connect_to_rasp2(data_port, host)) == -1) {
	close(p_sock->sock_ctrl);
	return -1;
    }
    else {
	p_sock->sock_data = t_sock;
    }
    
    return 0;
}

int finalize_RaspSocket(RaspSocket *p_sock)
{
    ctrl_rasp2(p_sock->sock_ctrl,
	       WS_CMD_SYSTEM_END, NULL);
    close(p_sock->sock_ctrl);
    close(p_sock->sock_data);
    return 0;
}

/***********************************************************
 *                        A/D 処理
 ***********************************************************/
int connect_RaspAD(RaspAD *p_sock, char *host)
{
    return initialize_RaspSocket(p_sock,
				 WS_AD_CTRL_PORT,
				 WS_AD_DATA_PORT,
				 host);
}

int init_RaspAD(RaspAD *p_sock, int ad_freq, int ch_mode)
{
    int status, tmp;

    setFreq_RaspAD(p_sock, ad_freq);
    getFreq_RaspAD(p_sock, &tmp);
    printf("Sampling Rate = %d [Hz]\n", tmp);
    
    setChannelMode_RaspAD(p_sock, ch_mode);
    getChannelMode_RaspAD(p_sock, &tmp);
    printf("Get Channel Mode = %d\n", tmp);

    return 0;
}

int disconnect_RaspAD(RaspAD *p_sock)
{
    return finalize_RaspSocket(p_sock);
}

int setFreq_RaspAD(RaspAD *p_sock, int freq)
{
    int status;
    
    status  = ctrl_rasp2(p_sock->sock_ctrl, WS_CMD_SET_FREQ, &freq);
    return status;
}

int getFreq_RaspAD(RaspAD *p_sock, int *freq)
{
    int status;

    status  = ctrl_rasp2(p_sock->sock_ctrl, WS_CMD_GET_FREQ, freq);
    return status;
}

int setChannelMode_RaspAD(RaspAD *p_sock, int channel_mode)
{
    int status;

    status  = ctrl_rasp2(p_sock->sock_ctrl, WS_CMD_SET_CHANNEL, &channel_mode);
    return status;
}

int getChannelMode_RaspAD(RaspAD *p_sock, int *channel_mode)
{
    int status;

    status  = ctrl_rasp2(p_sock->sock_ctrl, WS_CMD_GET_CHANNEL, channel_mode);
    return status;
}

int start_RaspAD(RaspAD *p_sock)
{
    int status;

    status = ctrl_rasp2(p_sock->sock_ctrl, WS_CMD_CONTINUATION_AD_START, NULL);
    if(status != 0) {
        printf("AD Start Error[0x%08x].\n", status);
        return -1;
    }
    
    return 0;
}

int stop_RaspAD(RaspAD *p_sock)
{
    int status, last_status;
    int io_stat, rest_size, read_size;
    char dummy[1024];
    
    last_status = 0;
    status  = ctrl_rasp2(p_sock->sock_ctrl, WS_CMD_AD_STOP, &last_status);
    
    io_stat = ioctl(p_sock->sock_data, FIONREAD, &rest_size);
    while((io_stat != -1) && (rest_size > 0)) {
        read_size = read(p_sock->sock_data, dummy, 1024);
        if(read_size < 0) {
            break;
        }
        io_stat = ioctl(p_sock->sock_ctrl, FIONREAD, &rest_size);
    }
    
    return last_status;
}

int startDuplex_RaspAD(RaspAD *p_sock)
{
    int status;

    status  = ctrl_rasp2(p_sock->sock_ctrl, WS_CMD_AD_DA_START, NULL);
    return status;
}

int readSint8_RaspAD(RaspAD *p_sock, char *buf, size_t size)
{
    int read_size;

    read_size = read(p_sock->sock_data, buf, size * sizeof(char));
    return read_size;
}

int readSint16_RaspAD(RaspAD *p_sock, short int *buf, size_t size)
{
    int read_size;

    read_size = read(p_sock->sock_data, buf, size * sizeof(short int));
    return read_size/sizeof(short int);
}

int readSegment_RaspAD(RaspAD *p_sock, short int *buf, size_t size)
{
    unsigned int stack = 0, diff = size * sizeof(short int), ret;
    unsigned char *pbuf = (unsigned char *)buf;
    
    while (stack < size * sizeof(short int)) {
        if ((ret = readSint8_RaspAD(p_sock, &pbuf[stack], diff)) <= 0)
            return stack;
        stack += ret;
	diff  -= ret;
    }
    return stack/sizeof(short int);
}

/***********************************************************
 *                        D/A 処理
 ***********************************************************/
int connect_RaspDA(RaspDA *p_sock, char *host)
{
    return initialize_RaspSocket(p_sock,
				 WS_DA_CTRL_PORT,
				 WS_DA_DATA_PORT,
				 host);
}

int init_RaspDA(RaspDA *p_sock, int da_freq,
		short int *init_buf, size_t buf_size)
{
    int status;

    status = setFreq_RaspDA(p_sock, da_freq);
    if(status != 0) {
        printf("DA Start Error[0x%08x].\n", status);
	return -1;
    }
    sendSint16_RaspDA(p_sock, init_buf, buf_size);
    return 0;
}

int disconnect_RaspDA(RaspDA *p_sock)
{
    return finalize_RaspSocket(p_sock);
}

int setFreq_RaspDA(RaspDA *p_sock, int freq)
{
    return ctrl_rasp2(p_sock->sock_ctrl,
		      WS_CMD_SET_DA_FREQ, &freq);
}

int getStatus_RaspDA(RaspDA *p_sock)
{
    int get_val = 0;
    
    ctrl_rasp2(p_sock->sock_ctrl,
	       WS_CMD_GET_DA_STATUS, &get_val);
    return get_val;
}

int ready_RaspDA(RaspDA *p_sock)
{
    int status;

    status = ctrl_rasp2(p_sock->sock_ctrl,
			WS_CMD_DA_READY, NULL);
    if(status != 0) {
        printf("DA Start Error[0x%08x].\n", status);
	return -1;
    }
    return 0;
}

int start_RaspDA(RaspDA *p_sock)
{
    int status;

    status = ctrl_rasp2(p_sock->sock_ctrl,
			WS_CMD_DA_START, NULL);
    return status;
}


int stop_RaspDA(RaspDA *p_sock)
{
    int status;
    int last_status;
    
    last_status = 0;
    status = ctrl_rasp2(p_sock->sock_ctrl,
			WS_CMD_DA_STOP, &last_status);
    return last_status;
}

int flush_RaspDA(RaspDA *p_sock)
{
    int status;
    int last_status;
    
    last_status = 0;
    status = ctrl_rasp2(p_sock->sock_ctrl,
			WS_CMD_DA_STOP_WAIT, &last_status);
    return last_status;
}

int flush2_RaspDA(RaspDA *p_sock)
{
    int status;
    int last_status;
    
    last_status = 0;
    status = ctrl2_rasp2(p_sock->sock_ctrl,
			WS_CMD_DA_STOP_WAIT, &last_status);
    return last_status;
}

int sendSint8_RaspDA(RaspDA *p_sock, char *buf, size_t size)
{
    int ret_size;

    ret_size = write(p_sock->sock_data,
		     buf, size * sizeof(char));
    return ret_size;
}

int sendSint16_RaspDA(RaspDA *p_sock, short int *buf, size_t size)
{
    int ret_size;

    ret_size = write(p_sock->sock_data, buf, size * sizeof(short int));
    return ret_size;
}

int setTimeout_RaspDA(RaspDA *p_sock, int timeout_sec)
{
    int status;
    struct timeval tv;
    
    if(timeout_sec < 0) {
        return -1;
    }
    
    tv.tv_sec  = timeout_sec;
    tv.tv_usec = 0;
    
    status = setsockopt(p_sock->sock_data, SOL_SOCKET,
                        SO_SNDTIMEO, (char *)&tv,
			sizeof(tv));
    return 0;
}

/******************************************************
 *
 *                  Rasp2
 *
 ******************************************************/
Rasp2 *new_Rasp2(unsigned int mode, char *hostname)
{
    Rasp2 dRasp2, *pRasp2;

    dRasp2.mode = mode;

    // AD の接続を確立
    if (mode & RASP2_AD) {
	if (connect_RaspAD(&(dRasp2.ad_sock), hostname) != 0) {
	    return NULL;
	}
    }
    // DA の接続を確立
    if (mode & RASP2_DA) {
	if (connect_RaspDA(&(dRasp2.da_sock), hostname) != 0) {
	    return NULL;
	}
    }

    // メモリの確保
    pRasp2  = (Rasp2 *)calloc(sizeof(Rasp2), 1);
    (*pRasp2) = dRasp2;
    
    return pRasp2;
}

int init_Rasp2(Rasp2 *pRasp2, int ad_freq, int ad_chmode,
	       int da_freq, short int *init_buf, size_t buf_size)
{
    int status;

    // DA 初期化
    if (pRasp2->mode & RASP2_DA) {
	if (init_RaspDA(&(pRasp2->da_sock), da_freq,
			init_buf, buf_size) != 0) {
	    return -1;
	}
    }
    // AD/DA 時の待機処理
    if (pRasp2->mode == RASP2_DUP) {
	status = ready_RaspDA(&(pRasp2->da_sock));
	if(status != 0) {
	    printf("DA Ready Error[0x%08x].\n", status);
	    return -1;
	}
    }
    // AD 初期化
    if (pRasp2->mode & RASP2_AD) {
	init_RaspAD(&(pRasp2->ad_sock), ad_freq, ad_chmode);
    }
    
    return 0;
}

int start_Rasp2(Rasp2 *pRasp2)
{
    int status;

    switch (pRasp2->mode) { 
    case RASP2_AD:  // AD
	status = start_RaspAD(&(pRasp2->ad_sock));
	if(status != 0) {
	    printf("AD Start Error[0x%08x].\n", status);
	    return -1;
	}
	break;
    case RASP2_DA:  // DA
	status = start_RaspDA(&(pRasp2->da_sock));    
	if(status != 0) {
	    printf("DA Start Error[0x%08x].\n", status);
	    return -1;
	}
	break;
    case RASP2_DUP: // AD & DA
	status = startDuplex_RaspAD(&(pRasp2->ad_sock));
	if(status != 0) {
	    printf("AD & DA Start Error[0x%08x].\n", status);
	    return -1;
	}
	break;
    }
    
    return 0;
}

int startRec_Rasp2(Rasp2 *pRasp2, int freq, int chmod)
{
    init_Rasp2(pRasp2, freq, chmod, 0, NULL, 0);
    return start_Rasp2(pRasp2);
}

int startPly_Rasp2(Rasp2 *pRasp2, int freq, short int *buf, size_t size)
{
    if (init_Rasp2(pRasp2, 0, 0, freq, buf, size) != 0)
	return -1;
    return start_Rasp2(pRasp2);
}

int startDup_Rasp2(Rasp2 *pRasp2, int freq_ad, int chmod,
		   int freq_da, short int *buf, size_t size)
{
    if (init_Rasp2(pRasp2, freq_ad, chmod, freq_da, buf, size) != 0)
	return -1;
    return start_Rasp2(pRasp2);
}

int stop_Rasp2(Rasp2 *pRasp2)
{
    int status;

    if (pRasp2->mode & RASP2_AD) {
	status = stop_RaspAD(&(pRasp2->ad_sock));
    }
    if (pRasp2->mode & RASP2_DA) {
	status = flush_RaspDA(&(pRasp2->da_sock));
    }
    
    return 0;
}

int read_Rasp2(Rasp2 *pRasp2, void *buf, size_t byte)
{
    return readSint8_RaspAD(&(pRasp2->ad_sock), (char *)buf, byte);
}

int write_Rasp2(Rasp2 *pRasp2, void *buf, size_t byte)
{
    return sendSint8_RaspDA(&(pRasp2->da_sock), (char *)buf, byte);
}

void delete_Rasp2(Rasp2 *pRasp2)
{
    if (pRasp2->mode & RASP2_AD)
	disconnect_RaspAD(&(pRasp2->ad_sock));
    if (pRasp2->mode & RASP2_DA)
	disconnect_RaspDA(&(pRasp2->da_sock));
    free(pRasp2);
}


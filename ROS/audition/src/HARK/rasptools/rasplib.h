#if !defined(__M_RASPLIB_HEADER__)
#define __M_RASPLIB_HEADER__

/*=========================================================*/
/*                   Macro Definition                      */
/*=========================================================*/

/* */
#define WS_AD_CTRL_PORT         52920
#define WS_AD_DATA_PORT        (WS_AD_CTRL_PORT+1)
#define WS_FPAA_PORT           (WS_AD_CTRL_PORT+2)
#define WS_DA_CTRL_PORT        (WS_AD_CTRL_PORT+3)
#define WS_DA_DATA_PORT        (WS_AD_CTRL_PORT+4)

#define WS_CONTROL_SERVER_MAX_CONNECTION    3
#define WS_DATA_SERVER_MAX_CONNECTION       3
#define WS_FPAA_SERVER_MAX_CONNECTION       3
#define WS_DA_CONTROL_SERVER_MAX_CONNECTION 3
#define WS_DA_DATA_SERVER_MAX_CONNECTION    3

/* */
#define SET_FLAG         0x00000000
#define GET_FLAG         0x10000000
#define MISC_FLAG        0x20000000
#define SYS_FLAG         0x40000000

#define WS_CMD_BASE      0x00219700

/* A/D */
#define WS_CMD_CONTINUATION_AD_START  (SET_FLAG | WS_CMD_BASE | 0x01)
#define WS_CMD_ONE_SHOT_AD_START      (SET_FLAG | WS_CMD_BASE | 0x02)
#define WS_CMD_AD_STOP                (SET_FLAG | WS_CMD_BASE | 0x03)
#define WS_CMD_AD_DA_START            (SET_FLAG | WS_CMD_BASE | 0x04)

#define WS_CMD_SET_FREQ               (SET_FLAG | WS_CMD_BASE | 0x11)
#define WS_CMD_GET_FREQ               (GET_FLAG | WS_CMD_BASE | 0x11)

#define WS_CMD_SET_CHANNEL            (SET_FLAG | WS_CMD_BASE | 0x12)
#define WS_CMD_GET_CHANNEL            (GET_FLAG | WS_CMD_BASE | 0x12)

#define WS_CMD_SET_SAMPLE_COUNT       (SET_FLAG | WS_CMD_BASE | 0x13)
#define WS_CMD_GET_SAMPLE_COUNT       (GET_FLAG | WS_CMD_BASE | 0x13)


/* D/A */
#define WS_CMD_DA_START               (SET_FLAG | WS_CMD_BASE | 0x31)
#define WS_CMD_DA_READY               (SET_FLAG | WS_CMD_BASE | 0x32)
#define WS_CMD_DA_STOP                (SET_FLAG | WS_CMD_BASE | 0x33)
#define WS_CMD_DA_STOP_WAIT           (SET_FLAG | WS_CMD_BASE | 0x34)

#define WS_CMD_GET_DA_STATUS          (GET_FLAG | WS_CMD_BASE | 0x39)

#define WS_CMD_SET_DA_FREQ            (SET_FLAG | WS_CMD_BASE | 0x11)
#define WS_CMD_GET_DA_FREQ            (GET_FLAG | WS_CMD_BASE | 0x11)

/* FPAA */
#define WS_CMD_SET_FPAA_CFG_DATA_SIZE (SET_FLAG  | WS_CMD_BASE | 0x71)
#define WS_CMD_FPAA_CFG_DATA_SIZE_CHK (MISC_FLAG | WS_CMD_BASE | 0x72)
#define WS_CMD_FPAA_CFG               (MISC_FLAG | WS_CMD_BASE | 0x73)


/* */
#define WS_CMD_CLEAR_LAST_ERROR       (SYS_FLAG | WS_CMD_BASE | 0x90)
#define WS_CMD_GET_LAST_ERROR         (GET_FLAG | WS_CMD_BASE | 0x90)
#define WS_CMD_SYSTEM_END             (SYS_FLAG | WS_CMD_BASE | 0x99)



/* */
#define STATUS_OK      0x00000000
#define STATUS_WARNING 0x80000000
#define STATUS_ERROR   0xE0000000

#define WRN_UNKNOWN_CMD              (STATUS_WARNING | 0x01)
#define ERR_UNKNOWN_CMD              (STATUS_ERROR   | 0x01)
#define ERR_CMD_FORMAT               (STATUS_ERROR   | 0x02)
#define ERR_BAD_PARAMETER            (STATUS_ERROR   | 0x03)
#define ERR_CREATE_TASK              (STATUS_ERROR   | 0x04)
#define ERR_AD_STARTED               (STATUS_ERROR   | 0x05)
#define ERR_FPAA_NO_CONFIG           (STATUS_ERROR   | 0x06)
#define ERR_BUFFER_OVER_FLOW         (STATUS_ERROR   | 0x07)
#define ERR_BRAM_OVER_FLOW           (STATUS_ERROR   | 0x08)
#define ERR_LOCAL_BUFFER_OVER_FLOW   (STATUS_ERROR   | 0x09)

#define ERR_DA_STARTED               (STATUS_ERROR   | 0x21)
#define ERR_DA_BRAM_UNDER_RUN        (STATUS_ERROR   | 0x22)

#define ERR_FPAA_CFG_SIZE            (STATUS_ERROR   | 0x71)
#define ERR_FPAA_CFG_DATA            (STATUS_ERROR   | 0x72)
#define ERR_FPAA_CFG                 (STATUS_ERROR   | 0x73)

/* */
#define CMD_STRING_LENGTH       64
#define DATA_BUFFER_SIZE        (64 * 1024)
#define FPAA_CONFIG_DATA_SIZE   ( 3 * 1024)

#define AD_16CH_MODE  0
#define AD_8CH_MODE   1

/****************************************************
 *
 ****************************************************/
typedef struct tag_RaspSocket {
    int sock_ctrl;  // 制御ソケット
    int sock_data;  // データ転送ソケット
} RaspSocket;

typedef RaspSocket RaspDA;
typedef RaspSocket RaspAD;

#define RASP2_AD     0x0001
#define RASP2_DA     0x0002
#define RASP2_DUP    0x0003

typedef struct tag_Rasp2 {
    // モード
    unsigned int mode;
    
    // ソケット
    RaspAD ad_sock;  // AD
    RaspDA da_sock;  // DA

    // 内部バッファ
    unsigned char *ring_buf;
    unsigned int r_mask;
    int t_pos;
    int b_pos;
} Rasp2;

//-----
extern int connect_to_rasp2(unsigned short port, char *host);
extern int ctrl_rasp2(int socket_fd, int cmd, int *arg);
extern int set_fpaa_config(unsigned int port, char *hostname, char *filename);

//------
extern int initialize_RaspSocket(RaspSocket *p_sock, unsigned int ctrl_port, unsigned int data_port, char *host);
extern int finalize_RaspSocket(RaspSocket *p_sock);

//------
extern int connect_RaspAD(RaspAD *p_sock, char *host);
extern int init_RaspAD(RaspAD *p_sock, int ad_freq, int ch_mode);
extern int disconnect_RaspAD(RaspAD *p_sock);
extern int setFreq_RaspAD(RaspAD *p_sock, int freq);
extern int getFreq_RaspAD(RaspAD *p_sock, int *freq);
extern int setChannelMode_RaspAD(RaspAD *p_sock, int channel_mode);
extern int getChannelMode_RaspAD(RaspAD *p_sock, int *channel_mode);
extern int start_RaspAD(RaspAD *p_sock);
extern int stop_RaspAD(RaspAD *p_sock);
extern int startDuplex_RaspAD(RaspAD *p_sock);
extern int readSint8_RaspAD(RaspAD *p_sock, char *buf, size_t size);
extern int readSint16_RaspAD(RaspAD *p_sock, short int *buf, size_t size);
extern int readSegment_RaspAD(RaspAD *p_sock, short int *buf, size_t size);

//------
extern int connect_RaspDA(RaspDA *p_sock, char *host);
extern int init_RaspDA(RaspDA *p_sock, int da_freq,
		       short int *init_buf, size_t buf_size);
extern int disconnect_RaspDA(RaspDA *p_sock);
extern int setFreq_RaspDA(RaspDA *p_sock, int freq);
extern int getStatus_RaspDA(RaspDA *p_sock);
extern int ready_RaspDA(RaspDA *p_sock);
extern int start_RaspDA(RaspDA *p_sock);
extern int stop_RaspDA(RaspDA *p_sock);
extern int flush_RaspDA(RaspDA *p_sock);
extern int flush2_RaspDA(RaspDA *p_sock);
extern int sendSint8_RaspDA(RaspDA *p_sock, char *buf, size_t size);
extern int sendSint16_RaspDA(RaspDA *p_sock, short int *buf, size_t size);
extern int setTimeout_RaspDA(RaspDA *p_sock, int timeout_sec);

//-------
extern Rasp2 *new_Rasp2(unsigned int mode, char *hostname);
extern int init_Rasp2(Rasp2 *pRasp2, int ad_freq, int ad_chmode,
		      int da_freq, short int *init_buf, size_t buf_size);
extern int start_Rasp2(Rasp2 *pRasp2);
extern int startRec_Rasp2(Rasp2 *pRasp2, int freq, int chmod);
extern int startPly_Rasp2(Rasp2 *pRasp2, int freq, short int *buf, size_t size);
extern int startDup_Rasp2(Rasp2 *pRasp2, int freq_ad, int chmod,
		   int freq_da, short int *buf, size_t size);
extern int stop_Rasp2(Rasp2 *pRasp2);
extern void delete_Rasp2(Rasp2 *pRasp2);
extern int read_Rasp2(Rasp2 *pRasp2, void *buf, size_t byte);
extern int write_Rasp2(Rasp2 *pRasp2, void *buf, size_t byte);


#endif

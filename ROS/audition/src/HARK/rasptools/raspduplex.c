#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "rasplib.h"
#include "WaveStream.h"

#define BUF_SIZE (8192*2)
#define SEGMENT_SIZE (512*16)

// 簡易パーサ
int parse_channel(int *ext_ch, char *str)
{
    int i, j;
    int ch_num = 0, len, ch1, ch2;
    char l_str[1024], parse[256], *s_ptr;

    len = strlen(str);
    strcpy(l_str, str);

    s_ptr = l_str;
    
    for (i = 0; i < len+1; i++) {
	if (l_str[i] == '\0' || l_str[i] == ',') {
	    l_str[i] = '\0';
	    if (strlen(s_ptr) <= 2 && strlen(s_ptr) >= 1) {
		ext_ch[ch_num++] = atoi(s_ptr);
	    }
	    else if (strlen(s_ptr) > 2) {
		sscanf(s_ptr, "%d-%d", &ch1, &ch2);
		for (j = ch1; j <= ch2; j++) {
		    ext_ch[ch_num++] = j;
		}
	    }
	    else {
		break;
	    }
	    i++;
	    s_ptr = &l_str[i];
	}
    }
    
    return ch_num;
}

int main(int argc, char *argv[])
{
    int i, j, ch_mode, ch_skip, ch_ad, freq_ad, tmp, status, ext_ch[32];
    int ch_da, freq_da;
    int ret_size = 0, write_size, dif_sample, stack_sample, rec_sample, i_time = 1;
    int flag_ad = 0, flag_da = 0;
    short int *wave_buf, *read_buf, *write_buf;
    float sec;
    char *hostname, *infile, *outfile;
    WaveStream *pOutput, *pInput;
    Rasp2 *pRasp2;

    if (argc < 6) {
	puts("Usage: rasprec HOSTNAME INFILE OUTFILE FREQ SECOND CHANNEL \n"
	     "\n"
	     "  HOSTNAME   host address [char]\n"
	     "  INEFILE    input wave format file name [char]\n"
	     "  OUTFILE    output wave format file name [char]\n"
	     "  FREQ       sampling frequency [Hz]\n"
	     "  SECOND     recording time [sec]\n"
	     "  CHANNEL    target microphone channels [char]\n"
	     "             example: 0-1,3,5-8");
	return -1;
    }

    //---- 引数
    hostname = argv[1];
    infile   = argv[2];
    outfile  = argv[3];
    freq_ad  = atoi(argv[4]);
    sec      = atof(argv[5]);
    ch_ad    = parse_channel(ext_ch, argv[6]);
    
    //---- チャンネル番号の確認
    for (i = 0; i < ch_ad; i++) {
	if (ext_ch[i] > 7) break;
    }
    
    if (i == ch_ad) {
	ch_mode = AD_8CH_MODE;
	ch_skip = 8;
    }
    else {
        ch_mode = AD_16CH_MODE;
	ch_skip = 16;
    }

    printf("Record as %d mode.\n", ch_skip);
    printf("Number of channel %d: ", ch_ad);
    for (i = 0; i < ch_ad; i++) {
	printf("%d ", ext_ch[i]);
    }
    printf("\n");

    //----- Rasp2 生成
    if ((pRasp2 = new_Rasp2(RASP2_DUP, hostname)) == NULL) {
	printf("cannot connect to '%s'.\n", hostname);
	return -1;
    }

    //----- 再生 wave
    pInput = new4Read_WaveStream(infile);
    if (pInput == NULL) {
	printf("no such file '%s'\n", infile);
	return -1;
    }
    freq_da = getSamplesPerSec_WaveStream(pInput);
    ch_da   = getChannel_WaveStream(pInput);
    
    if (sec < 0) rec_sample = getSamplePerCh_WaveStream(pInput);
    else         rec_sample = freq_ad * sec;
    
    //----- 出力 wave
    pOutput = new4Write_WaveStream(outfile, ch_ad, freq_ad, 16);
    if (pOutput == NULL) {
	printf("cannot open file '%s'.\n", outfile);
	return -1;
    }

    //---- メモリ確保
    read_buf  = (short int *)calloc(sizeof(short int), BUF_SIZE);
    wave_buf  = (short int *)calloc(sizeof(short int), BUF_SIZE);
    write_buf = (short int *)calloc(sizeof(short int), BUF_SIZE);

    //---- 8192点先読み
    ret_size = read2ShortArray_WaveStream(pInput, wave_buf, BUF_SIZE);
    
    if (ret_size == BUF_SIZE &&
	startDup_Rasp2(pRasp2, freq_ad, ch_mode, freq_da, wave_buf, ret_size) == 0) {

	//---- 録音&再生
	stack_sample = 0;
	
	while (stack_sample < rec_sample) {
	    
	    //---- 指定サンプル数までのサンプル数
	    dif_sample = rec_sample - stack_sample;
	    if (dif_sample > SEGMENT_SIZE/ch_skip - 1)
		dif_sample = SEGMENT_SIZE/ch_skip - 1;
	    
	    //---- ブロック読み込み
	    ret_size = readSegment_RaspAD(&(pRasp2->ad_sock), read_buf, SEGMENT_SIZE);
	    if (ret_size < 0) break;
	    stack_sample += ret_size/ch_skip - 1;
	    
	    //---- Wave 形式でコピー
	    write_size = 0;
	    for (i = 0; i < dif_sample; i++) {
		for (j = 0; j < ch_ad; j++) {
		    wave_buf[write_size++] = read_buf[ext_ch[j] + (i+1) * ch_skip];
		}
	    }
	    write4ShortArray_WaveStream(pOutput, wave_buf, write_size);
	    
	    //---- 進捗の表示
	    if ((int)(stack_sample/(double)freq_ad) >= i_time) {
		i_time++;
		printf(".");
		fflush(stdout);
	    }

	    //---- 書き込むデータ
	    if (flag_da == 0) {
		ret_size = read2ShortArray_WaveStream(pInput, write_buf, SEGMENT_SIZE-ch_skip);

		if (ret_size <= 0) flag_da = 1;
		else write_Rasp2(pRasp2, write_buf, ret_size*sizeof(short int));
	    }
	    
	}
	printf("\n");
	
	//---- ストップ
	status = stop_Rasp2(pRasp2);
    }
    
    //---- 後片づけ
    free(read_buf);
    free(wave_buf);
    free(write_buf);
    delete4Write_WaveStream(pOutput);
    delete4Read_WaveStream(pInput);
    delete_Rasp2(pRasp2);
    
    return 0;
}

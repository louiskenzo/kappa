/************************************************************
 **
 **     Object name : common.h
 **
 **     Description
 **     -- -- -- -- -- -- -- --
 **     WirelessSystem用のソケットサーバー＆クライアント
 **     共通ヘッダファイル
 **
 **     History
 **     -- -- -- -- -- -- -- --
 **     [ver2系の履歴]
 **     2007.05.31 K.Takagi D/Aステータス取得を追加
 **     2007.05.14 K.Takagi D/A関係を追加
 **
 **     [ver1系の履歴]
 **     2007.01.12 K.Takagi エラーコード追加
 **     2006.12.08 K.Takagi FPAA関係を追加
 **     2006.11.07 K.Takagi データバッファサイズを変更
 **                         16ch x 2Byte x 512 = 16KByte
 **                         FPGA内部バッファは32KByte
 **                         1/2溜ると読み出す
 **     2006.10.24 K.Takagi 新規作成
 **                         暫定のマクロコードを設定
 **
 **     Copyright (c) 2006-2007
 **     JEOL System Technology Co.,Ltd
 **
 */


/*=========================================================*/
/*               Special Macro Definition                  */
/*=========================================================*/

#ifndef _INCLUDE_WS_COMMON_H_
#define _INCLUDE_WS_COMMON_H_

/*=========================================================*/
/*                   Include Files                         */
/*=========================================================*/

/*=========================================================*/
/*                   Macro Definition                      */
/*=========================================================*/

/* 通信で使用するポート番号 */
#define WS_CONTROL_PORT     52920
#define WS_DATA_PORT        (WS_CONTROL_PORT+1)
#define WS_FPAA_PORT        (WS_CONTROL_PORT+2)
#define WS_DA_CONTROL_PORT  (WS_CONTROL_PORT+3)
#define WS_DA_DATA_PORT     (WS_CONTROL_PORT+4)


#define WS_CONTROL_SERVER_MAX_CONNECTION    3
#define WS_DATA_SERVER_MAX_CONNECTION       3
#define WS_FPAA_SERVER_MAX_CONNECTION       3
#define WS_DA_CONTROL_SERVER_MAX_CONNECTION 3
#define WS_DA_DATA_SERVER_MAX_CONNECTION    3



/* 通信で使用するコマンド */
#define SET_FLAG         0x00000000
#define GET_FLAG         0x10000000
#define MISC_FLAG        0x20000000
#define SYS_FLAG         0x40000000

#define WS_CMD_BASE      0x00219700


/* A/D関係 */
#define WS_CMD_CONTINUATION_AD_START  (SET_FLAG | WS_CMD_BASE | 0x01)
#define WS_CMD_ONE_SHOT_AD_START      (SET_FLAG | WS_CMD_BASE | 0x02)
#define WS_CMD_AD_STOP                (SET_FLAG | WS_CMD_BASE | 0x03)
#define WS_CMD_AD_DA_START            (SET_FLAG | WS_CMD_BASE | 0x04)

#define WS_CMD_SET_FREQ               (SET_FLAG | WS_CMD_BASE | 0x11)
#define WS_CMD_GET_FREQ               (GET_FLAG | WS_CMD_BASE | 0x11)

#define WS_CMD_SET_CHANNEL            (SET_FLAG | WS_CMD_BASE | 0x12)
#define WS_CMD_GET_CHANNEL            (GET_FLAG | WS_CMD_BASE | 0x12)

#define WS_CMD_SET_SAMPLE_COUNT       (SET_FLAG | WS_CMD_BASE | 0x13)
#define WS_CMD_GET_SAMPLE_COUNT       (GET_FLAG | WS_CMD_BASE | 0x13)


/* D/A関係 */
#define WS_CMD_DA_START               (SET_FLAG | WS_CMD_BASE | 0x31)
#define WS_CMD_DA_READY               (SET_FLAG | WS_CMD_BASE | 0x32)
#define WS_CMD_DA_STOP                (SET_FLAG | WS_CMD_BASE | 0x33)
#define WS_CMD_DA_STOP_WAIT           (SET_FLAG | WS_CMD_BASE | 0x34)

#define WS_CMD_GET_DA_STATUS          (GET_FLAG | WS_CMD_BASE | 0x39)

#define WS_CMD_SET_DA_FREQ            (SET_FLAG | WS_CMD_BASE | 0x11)
#define WS_CMD_GET_DA_FREQ            (GET_FLAG | WS_CMD_BASE | 0x11)


/* FPAA関係 */
#define WS_CMD_SET_FPAA_CFG_DATA_SIZE (SET_FLAG  | WS_CMD_BASE | 0x71)
#define WS_CMD_FPAA_CFG_DATA_SIZE_CHK (MISC_FLAG | WS_CMD_BASE | 0x72)
#define WS_CMD_FPAA_CFG               (MISC_FLAG | WS_CMD_BASE | 0x73)


/* 共通部 */
#define WS_CMD_CLEAR_LAST_ERROR       (SYS_FLAG | WS_CMD_BASE | 0x90)
#define WS_CMD_GET_LAST_ERROR         (GET_FLAG | WS_CMD_BASE | 0x90)
#define WS_CMD_SYSTEM_END             (SYS_FLAG | WS_CMD_BASE | 0x99)



/* ステータスコード */
#define STATUS_OK      0x00000000
#define STATUS_WARNING 0x80000000
#define STATUS_ERROR   0xE0000000

#define WRN_UNKNOWN_CMD              (STATUS_WARNING | 0x01)
#define ERR_UNKNOWN_CMD              (STATUS_ERROR   | 0x01)
#define ERR_CMD_FORMAT               (STATUS_ERROR   | 0x02)
#define ERR_BAD_PARAMETER            (STATUS_ERROR   | 0x03)
#define ERR_CREATE_TASK              (STATUS_ERROR   | 0x04)
#define ERR_AD_STARTED               (STATUS_ERROR   | 0x05)
#define ERR_FPAA_NO_CONFIG           (STATUS_ERROR   | 0x06)
#define ERR_BUFFER_OVER_FLOW         (STATUS_ERROR   | 0x07)
#define ERR_BRAM_OVER_FLOW           (STATUS_ERROR   | 0x08)
#define ERR_LOCAL_BUFFER_OVER_FLOW   (STATUS_ERROR   | 0x09)

#define ERR_DA_STARTED               (STATUS_ERROR   | 0x21)
#define ERR_DA_BRAM_UNDER_RUN        (STATUS_ERROR   | 0x22)

#define ERR_FPAA_CFG_SIZE            (STATUS_ERROR   | 0x71)
#define ERR_FPAA_CFG_DATA            (STATUS_ERROR   | 0x72)
#define ERR_FPAA_CFG                 (STATUS_ERROR   | 0x73)


/* 通信で使用するバッファサイズ */
#define CMD_STRING_LENGTH       64
#define DATA_BUFFER_SIZE        (64 * 1024)
#define FPAA_CONFIG_DATA_SIZE   ( 3 * 1024)


/*=========================================================*/
/*                 Structure Declaration                   */
/*=========================================================*/

/*=========================================================*/
/*                    Private Valiable                     */
/*=========================================================*/

/*=========================================================*/
/*                    Public Valiable                      */
/*=========================================================*/


#endif /* _INCLUDE_WS_COMMON_H_ */

/* EOF */

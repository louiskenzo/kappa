#!/usr/bin/env batchflow

<?xml version="1.0"?>
<Document>
	<!-- Main loop network -->
	<Network type="subnet" name="MAIN">
		<!-- Network parameters -->
		<NetOutput name="Main loop output" node="Main loop"          terminal="Loop output" object_type="any" description="No description available"/>
		<NetOutput name="VALUE"            node="ROS node generator" terminal="VALUE"       object_type="int" description="int parameter"/>
		<Note x="0" y="0" visible="0" text="Created with FlowDesigner 0.9.1"/>
		
		<!-- Parameter setting node, launches the sound-source localizatio node -->
		<Node name="Main loop" 
		      type="Sound source localization with ROS publishing" 
		      x="53.000000" y="72.000000">
			<Parameter name="LENGTH"         type="int"          value="512"   description="The length of a frame in one channel (in samples)."/>
			<Parameter name="SAMPLING_RATE"  type="int"          value="16000" description="Sampling Rate (Hz)."/>
			<Parameter name="A_MATRIX"       type="subnet_param" value="ARG1"  description="Filename of a transfer function matrix."/>
			<Parameter name="ADVANCE"        type="subnet_param" value="160"   description="The shift length beween adjacent frames (in samples)."/>
			<Parameter name="RASP_IP"        type="subnet_param" value="ARG2"  description="The IP address of the RASP unit."/>
			<Parameter name="DOWHILE"        type="bool"         value=""      description=""/>
		</Node>
		
		<!-- ROS Hark node creation -->
		<Node name="ROS node generator" 
		      type="RosNodeGenerator" 
		      x="-103.000000" y="283.000000">
			<Parameter name="NODE_NAME" type="string" value="HARK_ROS" description="Node name for ROS"/>
		</Node>
	</Network>
	
	<!-- Sound-source localization network with publishing to ROS through ROS-HARK -->
	<Network type="iterator" name="Sound source localization with ROS publishing">
		<!-- Network parameters -->
		<NetCondition name="CONDITION"   node="Microphone acquisition" terminal="NOT_EOF"/>
		<NetOutput    name="Loop output" node="ROS publisher"          terminal="OUTPUT" object_type="any" description="The same as input."/>
		<Note x="0" y="0" visible="0" text="Created with FlowDesigner 0.9.1"/>
		
		<!-- Audio signal acquisition nodes -->
		<Node name="Microphone acquisition" 
		      type="AudioStreamFromMic" 
		      x="-100.000000" y="98.000000">
			<Parameter name="LENGTH"        type="subnet_param" value="LENGTH"         description="The length of a frame in one channel (in samples)."/>
			<Parameter name="ADVANCE"       type="subnet_param" value="ADVANCE"        description="The shift length beween adjacent frames (in samples)."/>
			<Parameter name="CHANNEL_COUNT" type="int"          value="16"             description="The number of channels."/>
			<Parameter name="SAMPLING_RATE" type="subnet_param" value="SAMPLING_RATE"  description="Sampling rate (Hz)."/>
			<Parameter name="DEVICETYPE"    type="string"       value="RASP24-32"      description="Device type (ALSA, RASP, WS, TDBD16ADUSB, RASP24-16, RASP24-32, KINECT)."/>
			<Parameter name="DEVICE"        type="subnet_param" value="RASP_IP"        description="The name of device."/>
		</Node>
		
		<!-- FFT computation nodes -->
		<!--
		<Node name="Channel selector" type="ChannelSelector" x="202.000000" y="91.000000">
			<Parameter name="SELECTOR" type="object" value="&lt;Vector&lt;int&gt; 0 1 2 3 8 9 10 11 &gt;" description="Flags for channel selection. When the first three channels are selected from four channels, the flags should be set to &lt;Vector&lt;int&gt; 0 1 2&gt;."/>
		</Node>
		-->
		<Node name="Multi channel FFT" 
		      type="MultiFFT" 
		      x="327.000000" y="91.000000">
			<Parameter name="LENGTH"        type="subnet_param" value="LENGTH"  description="The length of FFT."/>
			<Parameter name="WINDOW"        type="string"       value="HAMMING" description="A window function for FFT. WINDOW should be CONJ, HAMMING, or RECTANGLE."/>
			<Parameter name="WINDOW_LENGTH" type="subnet_param" value="LENGTH"  description="The length of the window."/>
		</Node>
		
		<!-- Sound-source localization nodes -->
		<Node name="MUSIC localization" 
		      type="LocalizeGEVD3D" 
		      x="54.000000" y="253.000000">
			<Parameter name="MUSIC_ALGORITHM" type="string"        value="SEVD"          description="Sound Source Localization Algorithm. If SEVD, NOISECM will be ignored"/>
			<Parameter name="NB_CHANNELS"     type="int"           value="16"            description="The number of input channels."/>
			<Parameter name="LENGTH"          type="subnet_param"  value="LENGTH"        description="The length of a frame (per channel)."/>
			<Parameter name="SAMPLING_RATE"   type="subnet_param"  value="SAMPLING_RATE" description="Sampling Rate (Hz)."/>
			<Parameter name="A_MATRIX"        type="subnet_param"  value="A_MATRIX"      description="Filename of a transfer function matrix."/>
			<Parameter name="WINDOW"          type="int"           value="50"            description="The number of frames used for calculating a correlation function."/>
			<Parameter name="PERIOD"          type="int"           value="50"            description="The period in which the source localization is processed."/>
			<Parameter name="NUM_SOURCE"      type="int"           value="3"             description="Number of sources, which should be less than number of channels."/>
			<Parameter name="MIN_DEG"         type="int"           value="-180"          description="source direction (lower)."/>
			<Parameter name="MAX_DEG"         type="int"           value="180"           description="source direction (higher)."/>
			<Parameter name="LOWER_BOUND_FREQUENCY" type="int"     value="500"           description="Lower bound of frequency (Hz) used for correlation function calculation."/>
			<Parameter name="UPPER_BOUND_FREQUENCY" type="int"     value="2800"          description="Upper bound of frequency (Hz) used for correlation function calculation."/>
			<Parameter name="SPECTRUM_SPLINE_WEIGHT" type="string" value="Uniform"       description="MUSIC spectrum weight for each frequency bin."/>
			<Parameter name="A_CHAR_SCALING"  type="float"         value="1.0"           description="Scaling factor of the A-Weight with respect to frequency"/>
			<Parameter name="MANUAL_WEIGHT"   type="object"        value="&lt;Matrix&lt;float&gt; &lt;rows 2&gt; &lt;cols 5&gt; &lt;data 0.0 2000.0 4000.0 6000.0 8000.0 1.0 1.0 1.0 1.0 1.0&gt; &gt;" description="MUSIC spectrum weight for each frequency bin. This is a 2 by M matrix. The first row represents the frequency, and the second row represents the weight gain. &quot;M&quot; represents the number of key points for the spectrum weight. The frequency range between M key points will be interpolated by spline manner. The format is &quot;&lt;Matrix&lt;int&gt; &lt;rows 2&gt; &lt;cols 2&gt; &lt;data 1 2 3 4&gt; &gt;&quot;."/>
			<Parameter name="DEBUG"           type="bool"         value="false"         description="Debug option. If the parameter is true, this node outputs sound localization results to a standard output."/>
		</Node>
		
		<!-- Sound-source tracker -->
		<Node name="Sound-source tracker" type="SourceTracker" x="261.000000" y="238.000000">
			<Parameter name="THRESH"           type="float" value="23"    description="Power threshold for localization results. A localization result with higher power than THRESH is tracked, otherwise ignored."/>
			<Parameter name="PAUSE_LENGTH"     type="float" value="800"   description="Life duration of source in ms. When any localization result for a source is found for more than PAUSE_LENGTH / 10 iterations, the source is terminated. [default: 800]"/>
			<Parameter name="MIN_SRC_INTERVAL" type="float" value="20"    description="Source interval threshold in degree. When the angle between a localization result and a source is smaller than MIN_SRC_INTERVAL, the same ID is given to the localization result. [default: 20]"/>
			<Parameter name="MIN_ID"           type="int"   value="0"     description="Minimum ID of source locations. MIN_ID should be greater than 0 or equal."/>
			<Parameter name="DEBUG"            type="bool"  value="false" description="Output debug information if true [default: false]"/>
		</Node>
		
		<!-- ROS Hark publisher -->
		<Node name="ROS publisher" type="RosHarkMsgsPublisher" x="642.000000" y="255.000000">
			<Parameter name="ADVANCE"                      type="int"    value="160"               description="Shift sample number for sliding spectrum analysis."/>
			<Parameter name="ENABLE_DEBUG"                 type="bool"   value="false"             description="print debug message of this module in case of true."/>
			<Parameter name="TOPIC_NAME_HARKWAVE"          type="string" value="HarkWave"          description="Published topic name for ROS (HarkWave type message)"/>
			<Parameter name="TOPIC_NAME_HARKFFT"           type="string" value="HarkFFT"           description="Published topic name for ROS (HarkFFT type message)"/>
			<Parameter name="TOPIC_NAME_HARKFEATURE"       type="string" value="HarkFeature"       description="Published topic name for ROS (HarkFeature type message)"/>
			<Parameter name="TOPIC_NAME_HARKSOURCE"        type="string" value="HarkSource"        description="Published topic name for ROS (HarkSource type message)"/>
			<Parameter name="TOPIC_NAME_HARKSRCWAVE"       type="string" value="HarkSrcWave"       description="Published topic name for ROS (HarkSrcWave type message)"/>
			<Parameter name="TOPIC_NAME_HARKSRCFFT"        type="string" value="HarkSrcFFT"        description="Published topic name for ROS (HarkSrcFFT type message)"/>
			<Parameter name="TOPIC_NAME_HARKSRCFEATURE"    type="string" value="HarkSrcFeature"    description="Published topic name for ROS (HarkSrcFeature type message)"/>
			<Parameter name="TOPIC_NAME_HARKSRCFEATUREMFM" type="string" value="HarkSrcFeatureMFM" description="Published topic name for ROS (HarkSrcFeatureMFM type message)"/>
			<Parameter name="BUFFER_NUM"                   type="int"    value="100"               description="Buffer size for a ROS published message"/>
			<Parameter name="ROS_LOOP_RATE"                type="float"  value="100000"            description="This allows you to specify a frequency that you would like to loop at [Hz]. Keep this value large. (If ROS interval is shorter than HARK interval, ROS interval is overwritten.)"/>
		</Node>
		
		<!-- Node links -->
		<Link from="Microphone acquisition" output="AUDIO"  to="Multi channel FFT"    input="INPUT"/>
		<!--
		<Link from="Microphone acquisition" output="AUDIO"  to="Channel selector"     input="INPUT"/>
		<Link from="Channel selector"       output="OUTPUT" to="Multi channel FFT"    input="INPUT"/>
		-->
		<Link from="Multi channel FFT"      output="OUTPUT" to="MUSIC localization"   input="INPUT">359.5 91 384 91 384 141 -199 141 -199 176 -170.5 176 </Link>
		<!--
		<Link from="Microphone acquisition" output="AUDIO"  to="ROS publisher"        input="MIC_WAVE"/>
		<Link from="Multi channel FFT"      output="OUTPUT" to="ROS publisher"        input="MIC_FFT"/>
		-->
		<Link from="MUSIC localization"     output="OUTPUT" to="Sound-source tracker" input="INPUT"/>
		<Link from="Sound-source tracker"   output="OUTPUT" to="ROS publisher"        input="SRC_INFO"/>
	</Network>
	
	<!-- WTF? -->
	<Parameter name="ARG1" type="" value=""/>
	<Parameter name="160"  type="" value=""/>
</Document>

<!-- ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– -->

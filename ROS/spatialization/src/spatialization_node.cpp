/*******************************************************************************
*                                                                              *
*                               Kyoto University                               *
*                                Project Kappa                                 *
*                          Louis-Kenzo Furuya Cahier                           *
*                                                                              *
*******************************************************************************/

#include <cstdlib>
#include "ros/ros.h"
#include "spatialization_ros_adapter.h"

int main(int argc, char *argv[]) {
	// Initialize the ROS node
	ros::init(argc, argv, "spatialization_node");
	
	// Start the spatialization adapter, which has its own node handle
	ros_kappa::SpatializationAdapter spatialization_adapter(argc, argv);
	spatialization_adapter.start(); // Non-blocking, starts a new thread
	
	// Loop to process the messages and call callbacks
	ros::spin();
}

/******************************************************************************/


/*******************************************************************************
*                                                                              *
*                               Kyoto University                               *
*                                Project Kappa                                 *
*                          Louis-Kenzo Furuya Cahier                           *
*                                                                              *
*******************************************************************************/

#ifndef KAPPA_SPATIALIZATION_ROS_ADAPTER
#define KAPPA_SPATIALIZATION_ROS_ADAPTER

// Standard headers
#include <pthread.h>
// ROS headers
#include <ros/ros.h>
#include "std_msgs/Empty.h"
#include "geometry_msgs/Pose.h"
#include "sensor_msgs/PointCloud2.h"
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
// HARK-ROS headers
#include "hark_msgs/HarkSource.h"
// Project headers
//#include "kappa/log.h"
//#include "kappa/log_publisher.h"
//#include "kappa/spatialization.h"
//#include "kappa/spatialization_adapter.h"
#include "spatialization_ros_adapter.h"

namespace ros_kappa {
	class SpatializationAdapter //: public         ::kappa::SpatializationAdapter, 
	                            //  public virtual ::kappa::LogPublisher
	{
		public:
			//____________________ Constructors
			SpatializationAdapter(int argc, char* argv[]);
			
			//____________________ Destructors
			~SpatializationAdapter();
			
			//____________________ Public methods
			void start();
			//void onLog(const std::string& msg, kappa::Log::SeverityLevel level);
		
		private:
			//____________________ Helpers
			//using ::kappa::LogPublisher::log;
			//void log(const std::string& msg, kappa::Log::SeverityLevel level); // from LogSubscriber
			static void* processingThread(void* instance);
			void processing();
			// Handlers for subscriptions
			void visualLocalizationEstimateHandler(const geometry_msgs::Pose::ConstPtr& msg);
			void auditiveLocalizationEstimateHandler(const hark_msgs::HarkSource::ConstPtr& msg);
			void rangeImagingHandler(const sensor_msgs::PointCloud2::ConstPtr& msg);
			
			//____________________ Private fields
			// Threads
			pthread_t       _processing_thread; ///< Thread for PPM operations
			// ROS interface
			ros::NodeHandle _ros_node;          ///< Interface object to ROS
			// Subscriptions
			ros::Subscriber _visual_localization_subscription;   ///< ROS subscription for incoming visual localization estimates
			ros::Subscriber _auditive_localization_subscription; ///< ROS subscription for incoming auditive localization estimates
			ros::Subscriber _range_imaging_subscription;         ///< ROS subscription for incoming range imaging data
			tf::TransformListener    _transform_listener;
			// Publications
			ros::Publisher           _global_depth_perception_publisher;
			ros::Publisher           _auditive_localization_marker_publisher;
			tf::TransformBroadcaster _transform_broadcaster;
	};
}

#endif

/******************************************************************************/


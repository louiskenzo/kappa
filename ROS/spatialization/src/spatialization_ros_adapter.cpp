/*******************************************************************************
*                                                                              *
*                               Kyoto University                               *
*                                Project Kappa                                 *
*                          Louis-Kenzo Furuya Cahier                           *
*                                                                              *
*******************************************************************************/

// STL headers
#include <vector>
#include <string>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <algorithm>
// System headers
#include <pthread.h>
#include <unistd.h>
// ROS headers
#include <ros/ros.h>
#include <ros/console.h>
#include "std_msgs/Empty.h"
#include "geometry_msgs/Pose.h"
#include "geometry_msgs/Point.h"
#include "sensor_msgs/PointCloud2.h"
#include <pcl_ros/point_cloud.h>
#include <pcl_ros/transforms.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_datatypes.h>
#include <visualization_msgs/Marker.h>
// HARK-ROS headers
#include "hark_msgs/HarkSource.h"
// Kappa headers
//#include "kappa/log.h"
//#include "kappa/spatialization.h"
//#include "kappa/spatialization_adapter.h"
#include "spatialization_ros_adapter.h"

namespace ros_kappa {
	//____________________ Constructors
	SpatializationAdapter::SpatializationAdapter(int argc, char* argv[])
		: //LogPublisher(kappa::Log::DEBUG),
		  _processing_thread(),
		  _ros_node(),
		  _visual_localization_subscription(),
		  _transform_listener(),
		  _auditive_localization_marker_publisher(),
		  _transform_broadcaster() {
		usleep(100000); // Wait for a tenth of a second for rosconsole to setup
	}
	
	//____________________ Destructors
	SpatializationAdapter::~SpatializationAdapter() {
		//log("Spatialization adapter shutting down", kappa::Log::INFO);
	}
	
	//____________________ Public methods
	void SpatializationAdapter::start() {
		//log("Starting spatialization adapter");
		// Start the processing thread
		//log("Starting spatialization adapter processing thread");
		pthread_create(&_processing_thread, // Thread structure
		               NULL,                // Thread properties
		               processingThread,    // Function to run
		               static_cast<void*>(this)); // Arguments
	}
	/*
	void SpatializationAdapter::onLog(const std::string& msg, kappa::Log::SeverityLevel level) {
		log(msg,level);
	}
	*/
	//____________________ Helpers
	void* SpatializationAdapter::processingThread(void* instance) {
		static_cast<SpatializationAdapter*>(instance)->processing();
		
		// Loop to process the messages and call callbacks
		//ros::spin();
		ros::MultiThreadedSpinner spinner;
		spinner.spin();
		
		return EXIT_SUCCESS;
	}
	
	void SpatializationAdapter::processing() {
		// Subscribe
		//log("PerceptualMap adapter subscribing to visual localization topics");
		_visual_localization_subscription 
		= _ros_node.subscribe("perception/visual_localization/relative", 
		                      1, 
		                      &SpatializationAdapter::visualLocalizationEstimateHandler, 
		                      this);
		_auditive_localization_subscription 
		= _ros_node.subscribe("perception/auditive_localization/relative", 
		                      1, 
		                      &SpatializationAdapter::auditiveLocalizationEstimateHandler, 
		                      this);
		
		_range_imaging_subscription
		= _ros_node.subscribe("sensors/range_imaging",
		                      1,
		                      &SpatializationAdapter::rangeImagingHandler, 
		                      this);
		
		// Advertise
		_auditive_localization_marker_publisher 
		= _ros_node.advertise<visualization_msgs::Marker>("auditive_localization_marker", 1);
		
		_global_depth_perception_publisher 
		= _ros_node.advertise<sensor_msgs::PointCloud2>("perception/depth/global", 1);
	}
	
	void SpatializationAdapter::visualLocalizationEstimateHandler(const geometry_msgs::Pose::ConstPtr& msg) {
		/*{
		std::stringstream entry;
			entry << "Received pose estimate for relative visual localization :" 
				<< "(" << msg->position.x << "," 
				       << msg->position.y << "," 
				       << msg->position.z 
				<< "|" << msg->orientation.x << "," 
				       << msg->orientation.y << "," 
				       << msg->orientation.z << "," 
				       << msg->orientation.w << ")";
			log(entry.str());
		}*/
		
		// Convert the message to a tf pose so we can transform it
		//geometry_msgs::Pose adjusted_msg = *msg;
		//adjusted_msg.position.x = -msg->position.y;
		//adjusted_msg.position.y = -msg->position.z;
		//adjusted_msg.position.z = -msg->position.y;
		//adjusted_msg.orientation.x = msg->orientation.y;
		//adjusted_msg.orientation.y = msg->orientation.x;
		//adjusted_msg.orientation.z =  msg->orientation.x;
		//adjusted_msg.orientation.w = -msg->orientation.w;
		tf::Pose vSLAM_relative_pose;
		tf::poseMsgToTF(*msg, vSLAM_relative_pose);
		
		// Compute the transform from range imaging sensor to robot base
		tf::StampedTransform vslam_to_global;
		tf::StampedTransform visual_cameras_to_robot_base;
		try {
			_transform_listener.lookupTransform("global","vSLAM", 
			                                    ros::Time(0), 
			                                    vslam_to_global);
			_transform_listener.lookupTransform("bluefox_cameras","robot_base", 
			                                    ros::Time(0), 
			                                    visual_cameras_to_robot_base);
		} catch (tf::TransformException e) {
			std::stringstream entry;
			entry << e.what();
			//log(entry.str());
			
			return;
		}
		
		// TODO TEMPORARY Cancels out the rotation around the x and z axes from incoming poses
		tf::Vector3 cancel_rotation_1_axis(0,1,0);
		double      cancel_rotation_1_angle = -M_PI/2;
		tf::Vector3 cancel_rotation_2_axis(0,0,1);
		double      cancel_rotation_2_angle = M_PI/2;
		
		tf::Transform cancel_rotation_1(tf::Quaternion(cancel_rotation_1_axis, cancel_rotation_1_angle));
		tf::Transform cancel_rotation_2(tf::Quaternion(cancel_rotation_2_axis, cancel_rotation_2_angle));
		vSLAM_relative_pose = cancel_rotation_2 * cancel_rotation_1 * vSLAM_relative_pose;
		
		geometry_msgs::Pose adjusted_pose;
		tf::poseTFToMsg(vSLAM_relative_pose, adjusted_pose);
		double position_x = adjusted_pose.position.x;
		double position_y = adjusted_pose.position.y;
		double position_z = adjusted_pose.position.z;
		double orientation_x = adjusted_pose.orientation.x;
		double orientation_y = adjusted_pose.orientation.y;
		double orientation_z = adjusted_pose.orientation.z;
		adjusted_pose.position.x = position_z;
		adjusted_pose.position.y = position_x;
		adjusted_pose.position.z = position_y;
		adjusted_pose.orientation.x =  orientation_x;
		adjusted_pose.orientation.y = -orientation_z;
		adjusted_pose.orientation.z = -orientation_y;
		tf::poseMsgToTF(adjusted_pose, vSLAM_relative_pose);
		
		tf::Transform vSLAM_relative_pose_transform(vSLAM_relative_pose);
		_transform_broadcaster.sendTransform(tf::StampedTransform(vSLAM_relative_pose_transform, ros::Time::now(), "global", "vSLAM_relative_pose"));
		
		// Transform the pose in range imaging coordinates to pose in robot base coordinates
		tf::Pose visual_cameras_global_pose =              vslam_to_global * vSLAM_relative_pose;
		tf::Pose     robot_base_global_pose = visual_cameras_to_robot_base * visual_cameras_global_pose;
		
		// Publish the transform for the kappa node, which changes all daughter nodes
		tf::Transform robot_base_global_transform(robot_base_global_pose);
		_transform_broadcaster.sendTransform(tf::StampedTransform(robot_base_global_transform, ros::Time::now(), "global", "robot_base"));
	}
	
	void SpatializationAdapter::auditiveLocalizationEstimateHandler(const hark_msgs::HarkSource::ConstPtr& msg) {
		
	}
	
	void SpatializationAdapter::rangeImagingHandler(const sensor_msgs::PointCloud2::ConstPtr& relative_point_cloud) {
		sensor_msgs::PointCloud2 global_point_cloud;
		//pcl_ros::transformPointCloud("/global", *relative_point_cloud, global_point_cloud, _transform_listener);
		_global_depth_perception_publisher.publish(global_point_cloud);
	}
	/*
	void SpatializationAdapter::log(const std::string& msg, kappa::Log::SeverityLevel level) {
		//std::cerr << msg << std::endl;
		switch (level) {
			case kappa::Log::DEBUG:
				ROS_DEBUG("%s", msg.c_str());
				break;
			case kappa::Log::INFO:
				ROS_INFO("%s", msg.c_str());
				break;
			case kappa::Log::WARN:
				ROS_WARN("%s", msg.c_str());
				break;
			case kappa::Log::ERROR:
				ROS_ERROR("%s", msg.c_str());
				break;
			case kappa::Log::FATAL:
				ROS_FATAL("%s", msg.c_str());
				break;
			default:
				break;
		}
	}*/
}

/******************************************************************************/


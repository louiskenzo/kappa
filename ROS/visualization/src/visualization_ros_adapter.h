/*******************************************************************************
*                                                                              *
*                               Kyoto University                               *
*                                Project Kappa                                 *
*                          Louis-Kenzo Furuya Cahier                           *
*                                                                              *
*******************************************************************************/

#ifndef KAPPA_VISUALIZATION_ROS_ADAPTER
#define KAPPA_VISUALIZATION_ROS_ADAPTER

// STL headers
#include <vector>
// ROS headers
#include <ros/ros.h>
#include <tf/transform_listener.h>
// HARK-ROS headers
#include "hark_msgs/HarkSource.h"

namespace ros_kappa {
	class VisualizationAdapter {
		public:
			//____________________ Constants and enumerations
			static double min_shaft_size;
			static double max_shaft_size;
			static double min_tip_size;
			static double max_tip_size;
			
			//____________________ Constructors
			VisualizationAdapter(int argc, char *argv[]);
			
			//____________________ Destructors
			~VisualizationAdapter();
			
			//____________________ Public methods
			void start();
		
		private:
			//____________________ Helpers
			static void* setupThread(void* instance);
			void setup();
			static void* staticEnvironmentDisplayThread(void* instance);
			void displayStaticEnvironmentModel();
			// Handlers for subscriptions
			void auditiveLocalizationEstimateHandler(const hark_msgs::HarkSource::ConstPtr& msg);
			
			//____________________ Private fields
			// Variables for the visualization of auditive localization
			double _min_observed_power;
			double _max_observed_power;
			std::vector< std::vector<double> > _arrow_color_cycle;
			
			// Threads
			pthread_t _setup_thread; ///< Thread for setting up the visualizations
			pthread_t _static_environment_display_thread; ///< Thread for continuously maintaining the static environment display
			// ROS interface
			ros::NodeHandle _ros_node;     ///< Interface object to ROS
			// Subscriptions
			ros::Subscriber _auditive_localization_subscription; ///< ROS subscription for incoming auditive localization estimates
			tf::TransformListener _transform_listener;
			// Publications
			ros::Publisher  _static_environment_model_marker_publisher;
			ros::Publisher  _auditive_localization_marker_publisher;
	};
}

#endif

/******************************************************************************/


/*******************************************************************************
*                                                                              *
*                               Kyoto University                               *
*                                Project Kappa                                 *
*                          Louis-Kenzo Furuya Cahier                           *
*                                                                              *
*******************************************************************************/

#include <cstdlib>
#include <ros/ros.h>
#include "visualization_ros_adapter.h"

int main(int argc, char *argv[]) {
	// Initialize the ROS node
	ros::init(argc, argv, "visualization_node");
	
	// Start the visualization adapter, which has its own ROS node handle
	ros_kappa::VisualizationAdapter visualization_adapter(argc, argv);
	visualization_adapter.start(); // Non-blocking, starts a new thread
	
	// Loop to process the messages and call callbacks
	ros::spin();
	
	return EXIT_SUCCESS;
}

/******************************************************************************/


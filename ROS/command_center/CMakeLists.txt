cmake_minimum_required(VERSION 2.4.6)
include($ENV{ROS_ROOT}/core/rosbuild/rosbuild.cmake)

# Set the build type.  Options are:
#  Coverage       : w/ debug symbols, w/o optimization, w/ code-coverage
#  Debug          : w/ debug symbols, w/o optimization
#  Release        : w/o debug symbols, w/ optimization
#  RelWithDebInfo : w/ debug symbols, w/ optimization
#  MinSizeRel     : w/o debug symbols, w/ optimization, stripped binaries
#set(ROS_BUILD_TYPE RelWithDebInfo)

rosbuild_init()

#set the default path for built executables to the "bin" directory
set(EXECUTABLE_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/bin)
#set the default path for built libraries to the "lib" directory
set(LIBRARY_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/lib)

#uncomment if you have defined messages
#rosbuild_genmsg()
#uncomment if you have defined services
#rosbuild_gensrv()

#common commands for building c++ executables and libraries
#rosbuild_add_library(${PROJECT_NAME} src/example.cpp)
#target_link_libraries(${PROJECT_NAME} another_library)
#rosbuild_add_boost_directories()
#rosbuild_link_boost(${PROJECT_NAME} thread)
#rosbuild_add_executable(example examples/example.cpp)
#target_link_libraries(example ${PROJECT_NAME})

# Add current directory to CMake module path so it can find custom finders
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/CMake finders")

# Call the Kappa finder, and include the header and library directories found
find_package(Kappa)
include_directories(${Kappa_INCLUDE_DIR})
link_directories(${Kappa_LIBRARY_DIR})
# Call the GTKmm finder, and include the header and library directories found
find_package(PkgConfig)
pkg_check_modules(GTKMM gtkmm-2.4)
include_directories(${GTKMM_INCLUDE_DIRS})
# Add include and library directories for osgGtk
find_package(osgGtk)
include_directories(${libosgGtk_INCLUDE_DIRS})
link_directories(${libosgGtk_LIBRARY_DIR})
# Call the Threads finder, and include the header and library directories found
find_package(Threads)
include_directories(${Threads_INCLUDE_DIRS})
link_directories(${Threads_LIBRARY_DIRS})

# Compile the Kappa visualizer's ROS adapter into a library
rosbuild_add_library(command_center_adapter src/command_center_ros_adapter.cpp)
target_link_libraries(command_center_adapter kappa-command-center)
target_link_libraries(command_center_adapter pthread)

# Build the visualization node and link to the visualizer adapter
rosbuild_add_executable(command_center_node src/command_center_node.cpp)
target_link_libraries(command_center_node command_center_adapter)


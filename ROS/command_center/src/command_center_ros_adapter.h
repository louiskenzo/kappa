/*******************************************************************************
*                                                                              *
*                               Kyoto University                               *
*                                Project Kappa                                 *
*                          Louis-Kenzo Furuya Cahier                           *
*                                                                              *
*******************************************************************************/

#ifndef KAPPA_COMMAND_CENTER_ROS_ADAPTER
#define KAPPA_COMMAND_CENTER_ROS_ADAPTER

#include <pthread.h>
#include <ros/ros.h>
#include "kappa/command_center.h"

namespace ros_kappa {
	class CommandCenterAdapter {
		public:
			//____________________ Constructors
			CommandCenterAdapter(int argc, char* argv[]);
			
			//____________________ Public methods
			void start();
		
		private:
			//____________________ Helpers
			static void* threadStarter(void* instance);
			static void  onCommandCenterClose();
			
			//____________________ Private fields
			kappa::CommandCenter _command_center;
			pthread_t            _thread;
			ros::NodeHandle      _ros_node;
	};
}

#endif

/******************************************************************************/


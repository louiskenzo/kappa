/*******************************************************************************
*                                                                              *
*                               Kyoto University                               *
*                                Project Kappa                                 *
*                          Louis-Kenzo Furuya Cahier                           *
*                                                                              *
*******************************************************************************/

#include <cstdlib>
#include <pthread.h>
#include <ros/ros.h>
#include <ros/console.h>
#include "kappa/command_center.h"
#include "command_center_ros_adapter.h"

namespace ros_kappa {
	//____________________ Constructors
	CommandCenterAdapter::CommandCenterAdapter(int argc, char* argv[])
		: _command_center(argc,argv),
		  _thread(),
		  _ros_node() {
		_command_center.onClose(onCommandCenterClose);
	}
	
	//____________________ Public methods
	void CommandCenterAdapter::start() {
		pthread_create(&_thread,      // Thread structure
		               NULL,          // Thread properties
		               threadStarter, // Function to run
		               static_cast<void*>(this)); // Arguments
	}
	
	//____________________ Helpers
	void* CommandCenterAdapter::threadStarter(void* instance) {
		static_cast<CommandCenterAdapter*>(instance)->_command_center.start();
		return EXIT_SUCCESS;
	}
	
	void CommandCenterAdapter::onCommandCenterClose() {
		ros::shutdown();
	}
}

/******************************************************************************/


/*******************************************************************************
*                                                                              *
*                               Kyoto University                               *
*                                Project Kappa                                 *
*                          Louis-Kenzo Furuya Cahier                           *
*                                                                              *
*******************************************************************************/

#include <cstdlib>
#include <ros/ros.h>
#include <ros/console.h>
#include "command_center_ros_adapter.h"

int main(int argc, char *argv[]) {
	// Initialize the ROS node
	ros::init(argc, argv, "command_center_node");
	
	// Start the command center, which has its own node handle
	ros_kappa::CommandCenterAdapter command_center(argc,argv);
	command_center.start(); // Non-blocking, starts a new thread
	
	// Loop to process the messages and call callbacks
	ros::spin();
	
	return EXIT_SUCCESS;
}

/******************************************************************************/


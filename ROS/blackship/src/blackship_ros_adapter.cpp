/*******************************************************************************
*                                                                              *
*                               Kyoto University                               *
*                                Project Kappa                                 *
*                          Louis-Kenzo Furuya Cahier                           *
*                                                                              *
*******************************************************************************/

// Standard headers
#include <string>
#include <cstdlib>
#include <iostream>
#include <pthread.h>
#include <unistd.h>
// ROS headers
#include <ros/ros.h>
#include <ros/console.h>
#include "std_msgs/Empty.h"
// Kappa headers
#include "kappa/log.h"
#include "blackship_ros_adapter.h"

namespace ros_kappa {
	//____________________ Constructors
	BlackshipAdapter::BlackshipAdapter(int argc, char* argv[])
		: LogPublisher(kappa::Log::DEBUG),
		  _processing_thread(),
		  _ros_node(),
		  _some_subscription(),
		  _some_publisher() {
		usleep(100000); // Wait for a tenth of a second for rosconsole to setup
	}
	
	//____________________ Destructors
	BlackshipAdapter::~BlackshipAdapter() {
		log("Blackship adapter shutting down", kappa::Log::INFO);
	}
	
	//____________________ Public methods
	void BlackshipAdapter::start() {
		log("Starting PPM adapter");
		// Start the processing thread
		log("Starting PPM adapter processing thread");
		pthread_create(&_processing_thread, // Thread structure
		               NULL,                // Thread properties
		               processingThread,    // Function to run
		               static_cast<void*>(this)); // Arguments
	}
	
	//____________________ Helpers
	void* BlackshipAdapter::processingThread(void* instance) {
		static_cast<BlackshipAdapter*>(instance)->processing();
		ros::spin();
		
		return EXIT_SUCCESS;
	}
	
	void BlackshipAdapter::processing() {
		// Subscribe
		_some_subscription = _ros_node.subscribe("/some_blackship_topic", 
		                                         1, &BlackshipAdapter::someHandler, this);
		
		// Advertise
		_some_publisher = _ros_node.advertise<std_msgs::Empty>("some_blackship_publiser", 1000);
	}
	
	void BlackshipAdapter::someHandler(const std_msgs::Empty::ConstPtr& msg) {
		
	}
	
	void BlackshipAdapter::log(const std::string& msg, kappa::Log::SeverityLevel level) {
		switch (level) {
			case kappa::Log::DEBUG:
				ROS_DEBUG("%s", msg.c_str());
				break;
			case kappa::Log::INFO:
				ROS_INFO("%s", msg.c_str());
				break;
			case kappa::Log::WARN:
				ROS_WARN("%s", msg.c_str());
				break;
			case kappa::Log::ERROR:
				ROS_ERROR("%s", msg.c_str());
				break;
			case kappa::Log::FATAL:
				ROS_FATAL("%s", msg.c_str());
				break;
			default:
				break;
		}
	}
}

/******************************************************************************/


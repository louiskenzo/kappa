/*******************************************************************************
*                                                                              *
*                               Kyoto University                               *
*                                Project Kappa                                 *
*                          Louis-Kenzo Furuya Cahier                           *
*                                                                              *
*******************************************************************************/

#include <cstdlib>
#include "ros/ros.h"
#include "blackship_ros_adapter.h"

int main(int argc, char *argv[]) {
	// Initialize the ROS node
	ros::init(argc, argv, "ppm_node");
	
	// Start the Blackship adapter, which has its own node handle
	ros_kappa::BlackshipAdapter blackship_adapter(argc, argv);
	blackship_adapter.start(); // Non-blocking, starts a new thread
	
	// Loop to process the messages and call callbacks
	ros::spin();
}

/******************************************************************************/


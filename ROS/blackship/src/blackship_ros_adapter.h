/*******************************************************************************
*                                                                              *
*                               Kyoto University                               *
*                                Project Kappa                                 *
*                          Louis-Kenzo Furuya Cahier                           *
*                                                                              *
*******************************************************************************/

#ifndef KAPPA_BLACKSHIP_ROS_ADAPTER
#define KAPPA_BLACKSHIP_ROS_ADAPTER

// Standard headers
#include <pthread.h>
#include <ros/ros.h>
// ROS headers
#include "std_msgs/Empty.h"
// Project headers
#include "kappa/log_publisher.h"

namespace ros_kappa {
	class BlackshipAdapter : public virtual ::kappa::LogPublisher
	{
		public:
			//____________________ Types
			using ::kappa::LogPublisher::log;
			
			//____________________ Constructors
			BlackshipAdapter(int argc, char* argv[]);
			
			//____________________ Destructors
			~BlackshipAdapter();
			
			//____________________ Public methods
			void start();
		
		private:
			//____________________ Helpers
			// Thread entry points and associated member function (1 each per 
			// pthread_t member)
			static void* processingThread(void* instance);
			void         processing();
			// Handlers for subscriptions (1 per ros::Subscriber member)
			void someHandler(const std_msgs::Empty::ConstPtr& msg);
			
			// From LogSubscriber
			void log(const std::string& msg, kappa::Log::SeverityLevel level);
			
			//____________________ Private fields
			// Threads
			pthread_t       _processing_thread; ///< Thread for general processing
			// ROS interface
			ros::NodeHandle _ros_node; ///< Interface object to ROS
			// Subscriptions
			ros::Subscriber _some_subscription; ///< ROS subscription (PLACEHOLDER)
			// Publications
			ros::Publisher  _some_publisher; ///< ROS publisher (PLACEHOLDER)
	};
}

#endif

/******************************************************************************/


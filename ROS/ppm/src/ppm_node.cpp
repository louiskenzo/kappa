/*******************************************************************************
*                                                                              *
*                               Kyoto University                               *
*                                Project Kappa                                 *
*                          Louis-Kenzo Furuya Cahier                           *
*                                                                              *
*******************************************************************************/

#include <cstdlib>
#include "ros/ros.h"
#include "ppm_ros_adapter.h"

int main(int argc, char *argv[]) {
	// Initialize the ROS node
	ros::init(argc, argv, "ppm_node");
	
	// Start the command center, which has its own node handle
	ros_kappa::PPMAdapter ppm_adapter(argc, argv);
	ppm_adapter.start(); // Non-blocking, starts a new thread
	
	// Loop to process the messages and call callbacks
	ros::spin();
}

/******************************************************************************/


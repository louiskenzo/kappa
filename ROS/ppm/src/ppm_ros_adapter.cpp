/*******************************************************************************
*                                                                              *
*                               Kyoto University                               *
*                                Project Kappa                                 *
*                          Louis-Kenzo Furuya Cahier                           *
*                                                                              *
*******************************************************************************/

// Standard headers
#include <vector>
#include <string>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <pthread.h>
#include <unistd.h>
// Boost headers
#include <boost/math/constants/constants.hpp>
// ROS headers
#include <ros/ros.h>
#include <ros/console.h>
#include "sensor_msgs/PointCloud2.h"
#include "std_msgs/UInt32.h"
#include "std_msgs/Empty.h"
#include "geometry_msgs/Pose.h"
// HARK-ROS headers
#include "hark_msgs/HarkSource.h"
// Kappa headers
#include "kappa/log.h"
#include "kappa/ppm.h"
#include "kappa/ppm_visualization.h"
#include "kappa/ppm_estimates.h"
#include "kappa/ppm_processing.h"
#include "kappa/sound_source_localization_estimate.h"
#include "ppm_ros_adapter.h"

using boost::math::constants::pi;

namespace ros_kappa {
	//____________________ Constructors
	PPMAdapter::PPMAdapter(int argc, char* argv[])
		: LogPublisher(kappa::Log::DEBUG),
		  _ppm(),
		  _processing_thread(),
		  _display_thread(),
		  _ros_node(),
		  _scan_subscription(),
		  _clear_subscription(),
		  _sound_source_localization_estimate_subscription(),
		  _transform_listener(),
		  _vertex_count_publisher(),
		  _halfedge_count_publisher(),
		  _face_count_publisher(),
		  _command_center(argc,argv) {
		usleep(100000); // Wait for a tenth of a second for rosconsole to setup
		log("Setting up PPM adapter");
		_ppm.publishTo(this);
		_command_center.publishTo(this);
		log("PPMAdapter subscribing to PPM callbacks");
		_ppm.subscribeOnVertexCountChange(this);
		_ppm.subscribeOnHalfedgeCountChange(this);
		_ppm.subscribeOnFaceCountChange(this);
	}
	
	//____________________ Destructors
	PPMAdapter::~PPMAdapter() {
		log("PPM adapter shutting down", kappa::Log::INFO);
	}
	
	//____________________ Public methods
	void PPMAdapter::start() {
		log("Starting PPM adapter");
		// Start the processing thread
		log("Starting PPM adapter processing thread");
		pthread_create(&_processing_thread, // Thread structure
		               NULL,                // Thread properties
		               processingThread,    // Function to run
		               static_cast<void*>(this)); // Arguments
		
		// Start the display thread
		log("Starting PPM adapter display thread");
		pthread_create(&_display_thread,    // Thread structure
		               NULL,                // Thread properties
		               displayThread,       // Function to run
		               static_cast<void*>(this)); // Arguments
	}
	
	void PPMAdapter::onPPMChange() {
		log("PPM adapter reacting to PPM change");
	}
	
	void PPMAdapter::onPPMVertexCountChange(unsigned int new_vertex_count) {
		std_msgs::UInt32 msg;
		msg.data = new_vertex_count;
		_vertex_count_publisher.publish(msg);
		
		std::stringstream entry;
		entry << "Vertex count changed; publishing new count: " << new_vertex_count;
		log(entry.str());
	}
	
	void PPMAdapter::onPPMHalfedgeCountChange(unsigned int new_halfedge_count) {
		std_msgs::UInt32 msg;
		msg.data = new_halfedge_count;
		_halfedge_count_publisher.publish(msg);
		
		std::stringstream entry;
		entry << "Halfedge count changed; publishing new count: " << new_halfedge_count;
		log(entry.str());
	}
	
	void PPMAdapter::onPPMFaceCountChange(unsigned int new_face_count) {
		std_msgs::UInt32 msg;
		msg.data = new_face_count;
		_face_count_publisher.publish(msg);
		
		std::stringstream entry;
		entry << "Face count changed; publishing new count: " << new_face_count;
		log(entry.str());
	}
	
	void PPMAdapter::onLog(const std::string& msg, kappa::Log::SeverityLevel level) {
		log(msg,level);
	}
	
	//____________________ Helpers
	void* PPMAdapter::processingThread(void* instance) {
		static_cast<PPMAdapter*>(instance)->processing();
		
		// Loop to process the messages and call callbacks
		//ros::spin();
		ros::MultiThreadedSpinner spinner;
		spinner.spin();
		
		return EXIT_SUCCESS;
	}
	
	void* PPMAdapter::displayThread(void* instance) {
		PPMAdapter* ppm_adapter = static_cast<PPMAdapter*>(instance);
		ppm_adapter->_command_center.addVisualization(std::string("PPM"), static_cast<kappa::Visualization*>(new kappa::PPMVisualization(&ppm_adapter->_ppm)));
		ppm_adapter->_command_center.start();
		
		// Loop to process the messages and call callbacks
		ros::spin();
		
		return EXIT_SUCCESS;
	}
	
	void PPMAdapter::processing() {
		// Subscribe
		log("PPM adapter subscribing to point cloud and clear command topics");
		_scan_subscription = _ros_node.subscribe("perception/depth", 
		                                         1, 
		                                         &PPMAdapter::scanHandler, 
		                                         this);
		_clear_subscription = _ros_node.subscribe("do_clear", 
		                                          1, 
		                                          &PPMAdapter::clearHandler, 
		                                          this);
		_sound_source_localization_estimate_subscription 
		= _ros_node.subscribe("perception/auditive_localization/relative", 
		                      1, 
		                      &PPMAdapter::soundSourceLocalizationEstimateHandler, 
		                      this);
		
		// Advertise
		log("PPM adapter setting up advertising of vertex, halfedge and face count changes");
		_vertex_count_publisher   = _ros_node.advertise<std_msgs::UInt32>("vertex_count",   1000);
		_halfedge_count_publisher = _ros_node.advertise<std_msgs::UInt32>("halfedge_count", 1000);
		_face_count_publisher     = _ros_node.advertise<std_msgs::UInt32>("face_count",     1000);
	}
	
	void PPMAdapter::scanHandler(const sensor_msgs::PointCloud2::ConstPtr& msg) {
		log("PPM adapter received new scan");
		
		{
			std::stringstream entry;
			entry << "Point cloud has size " << msg->width << " px by " << msg->height << "px; each point takes " << msg->point_step << " bytes; " << (msg->is_dense?"no points are missing":"some points are missing") << "; fields are:";
			log(entry.str());
		}
		
		// Obtain or detemine the pose of the ranging device
		/*
		tf::StampedTransform kinect_pose;
		_transform_listener.lookupTransform("range_imaging_sensor", "world", ros::Time(0), kinect_pose);
		
		{
			std::stringstream entry;
			entry << "Obtained ranging device pose for new PPM scan integration: " << kinect_pose.getOrigin();
			log(entry.str());
		}*/
		
		bool x_available          = false;
		bool y_available          = false;
		bool z_available          = false;
		bool rgb_available        = false;
		bool confidence_available = false;
		bool intensity_available  = false;
		
		unsigned int x_offset          = 0;
		unsigned int y_offset          = 0;
		unsigned int z_offset          = 0;
		unsigned int rgb_offset        = 0;
		unsigned int confidence_offset = 0;
		unsigned int intensity_offset  = 0;
		unsigned int x_datatype          = 0;
		unsigned int y_datatype          = 0;
		unsigned int z_datatype          = 0;
		unsigned int rgb_datatype        = 0;
		unsigned int confidence_datatype = 0;
		unsigned int intensity_datatype  = 0;
		
		for (unsigned int f=0 ; f < msg->fields.size() ; ++f) {
			if(msg->fields[f].name == "x") {
				x_available = true;
				x_offset    = msg->fields[f].offset;
				x_datatype  = static_cast<unsigned int>(msg->fields[f].datatype);
			}
			if(msg->fields[f].name == "y") {
				y_available = true;
				y_offset = msg->fields[f].offset;
				y_datatype  = static_cast<unsigned int>(msg->fields[f].datatype);
			}
			if(msg->fields[f].name == "z") {
				z_available = true;
				z_offset = msg->fields[f].offset;
				z_datatype  = static_cast<unsigned int>(msg->fields[f].datatype);
			}
			if(msg->fields[f].name == "rgb") {
				rgb_available = true;
				rgb_offset    = msg->fields[f].offset;
				rgb_datatype  = static_cast<unsigned int>(msg->fields[f].datatype);
			}
			if(msg->fields[f].name == "confidence") {
				confidence_available = true;
				confidence_offset    = msg->fields[f].offset;
				confidence_datatype  = static_cast<unsigned int>(msg->fields[f].datatype);
			}
			if(msg->fields[f].name == "intensity") {
				intensity_available = true;
				intensity_offset    = msg->fields[f].offset;
				intensity_datatype  = static_cast<unsigned int>(msg->fields[f].datatype);
			}
			
			std::stringstream entry;
			entry << msg->fields[f].name << " of type " << static_cast<int>(msg->fields[f].datatype) << " at offset " << msg->fields[f].offset << " with count " << msg->fields[f].count;
			log(entry.str());
		}
		
		if(! (x_available && y_available && z_available)) {
			log("No 3D point data in the scan; discarding", kappa::Log::WARN);
			return;
		}
		
		if(!msg->height==1) {
			log("Scan doesn't have a 2D structure, discarding", kappa::Log::WARN);
			return;
		}
		
		std::vector<kappa::PPM::Point>* points     = new std::vector<kappa::PPM::Point>();
		std::vector<double>*            confidence = new std::vector<double>();
		std::vector<double>*            intensity  = new std::vector<double>();
		std::vector<unsigned int>*      rgb        = new std::vector<unsigned int>();
		unsigned int width  = msg->width;
		unsigned int height = msg->height;
		unsigned int downsize_factor = 1;
		
		if (msg->width  == 640 && 
		    msg->height == 480) {
			downsize_factor = 4;
			width  /= downsize_factor;
			height /= downsize_factor;
		}
		
		// Determine the final estimated optical center position estimation and sensor orientation estimations; compute the homogeneous coordinates transformation (rotation and translation) to apply to the raw data.
				
		//kappa::PPM::AffineTransformation estimated_camera_pose();
//		Aff_transformation_3<Kernel> t (	 Kernel::RT m00,
//		Kernel::RT m01,
//		Kernel::RT m02,
//		Kernel::RT m03,
//		Kernel::RT m10,
//		Kernel::RT m11,
//		Kernel::RT m12,
//		Kernel::RT m13,
//		Kernel::RT m20,
//		Kernel::RT m21,
//		Kernel::RT m22,
//		Kernel::RT m23,
//		Kernel::RT hw = RT(1));
		
		// Read the data and transform it in the estimated reference frame
		for (unsigned int r=0 ; r < msg->height ; r+=downsize_factor) {
			for (unsigned int c=0 ; c < msg->width ; c+=downsize_factor) {
				unsigned int point_offset = r*msg->row_step + 
				                            c*msg->point_step;
				const unsigned char* point_pointer = &msg->data[point_offset];
				
				kappa::PPM::Point point(*reinterpret_cast<const float*>(point_pointer + x_offset),
				                        *reinterpret_cast<const float*>(point_pointer + y_offset),
				                        *reinterpret_cast<const float*>(point_pointer + z_offset));
				// 
				//point.
				
				// Record the adjusted point
				points->push_back(point);
				
				if (confidence_available) {
					confidence->push_back(*reinterpret_cast<const float*>(point_pointer + confidence_offset));
				}
				if (intensity_available) {
					intensity->push_back(*reinterpret_cast<const float*>(point_pointer + intensity_offset));
				}
				if (rgb_available) {
					rgb->push_back(*reinterpret_cast<const unsigned int*>(point_pointer + rgb_offset));
				}
			}
		}
		
		kappa::PPM::Estimates* estimates 
		= new kappa::PPM::Estimates(kappa::PPM::Point(0,0,0),
		                            points, width, height);
		if (confidence_available) estimates->add_confidence_data(confidence);
		if (intensity_available)  estimates->add_intensity_data(intensity);
		if (rgb_available)        estimates->add_color_data(rgb);
		
		// Perform actual processing
		kappa::PPM::Processing* processing_DS = _ppm.addEstimates(estimates);
		
		log("Added new estimates to PPM", kappa::Log::INFO);
	}
	
	void PPMAdapter::clearHandler(const std_msgs::Empty::ConstPtr& msg) {
		log("PPM adapter received clear command");
		_ppm.clear();
		log("PPM cleared", kappa::Log::INFO);
	}
	
	void PPMAdapter::soundSourceLocalizationEstimateHandler(const hark_msgs::HarkSource::ConstPtr& msg) {
		// Process all sound source localization estimate returned by HARK
		for (unsigned int i=0 ; i<msg->src.size() ; ++i) {
			// Optionally filter based on power, or track
			// TODO
			
			std::stringstream message;
			message << "PPM adapter received new sound source localization estimate: " << msg->src[i].theta << " degrees";
			log(message.str());
			
			// Obtain parameters of auditive system
			double azimuth = - msg->src[i].theta * pi<double>() / 180;
			kappa::PPM::Point  auditive_center(0.0,0.0,0.0); // TODO assumes auditive center same as optical center
			kappa::PPM::Vector front(std::cos(azimuth),0.0,std::sin(azimuth));
			kappa::PPM::Vector up(0.0,1.0,0.0);
			double sound_source_localization_variance = 0.03; // variance in rad^2
			std::cerr << "Angle: " << msg->src[i].theta << "; front vector:" << front << std::endl;
			
			// Build localization object with auditive system parameters
			kappa::SoundSourceLocalizationEstimate<kappa::PPM::Point>*
			new_sound_source_localization_estimate = new 
			kappa::HalfPlaneSoundSourceLocalizationEstimate<kappa::PPM::Point,
				                                            kappa::PPM::Vector,
				                                            kappa::PPM::Plane>(auditive_center,
				                                                               front,
				                                                               up,
				                                                               sound_source_localization_variance);
		
			// Add estimate
			_ppm.addSoundSourceLocalizationEstimate(new_sound_source_localization_estimate);
		}
		
		log("Added new sound source localization estimate to PPM", kappa::Log::INFO);
	}
	
	void PPMAdapter::log(const std::string& msg, kappa::Log::SeverityLevel level) {
		//std::cerr << msg << std::endl;
		switch (level) {
			case kappa::Log::DEBUG:
				ROS_DEBUG("%s", msg.c_str());
				break;
			case kappa::Log::INFO:
				ROS_INFO("%s", msg.c_str());
				break;
			case kappa::Log::WARN:
				ROS_WARN("%s", msg.c_str());
				break;
			case kappa::Log::ERROR:
				ROS_ERROR("%s", msg.c_str());
				break;
			case kappa::Log::FATAL:
				ROS_FATAL("%s", msg.c_str());
				break;
			default:
				break;
		}
	}
}

/******************************************************************************/


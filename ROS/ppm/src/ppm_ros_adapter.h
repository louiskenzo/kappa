/*******************************************************************************
*                                                                              *
*                               Kyoto University                               *
*                                Project Kappa                                 *
*                          Louis-Kenzo Furuya Cahier                           *
*                                                                              *
*******************************************************************************/

#ifndef KAPPA_PPM_ROS_ADAPTER
#define KAPPA_PPM_ROS_ADAPTER

// Standard headers
#include <pthread.h>
#include <ros/ros.h>
// ROS headers
#include "sensor_msgs/PointCloud2.h"
#include "std_msgs/Empty.h"
#include "geometry_msgs/Pose.h"
#include <tf/transform_listener.h>
// HARK-ROS headers
#include "hark_msgs/HarkSource.h"
// Project headers
#include "kappa/log.h"
#include "kappa/command_center.h"
#include "kappa/ppm.h"
#include "kappa/ppm_adapter.h"

namespace ros_kappa {
	class PPMAdapter : public         ::kappa::PPMAdapter, 
	                   public virtual ::kappa::LogPublisher
	{
		public:
			//____________________ Constructors
			PPMAdapter(int argc, char* argv[]);
			
			//____________________ Destructors
			~PPMAdapter();
			
			//____________________ Public methods
			void start();
			void onPPMChange();
			void onPPMVertexCountChange(  unsigned int new_vertex_count);
			void onPPMHalfedgeCountChange(unsigned int new_halfedge_count);
			void onPPMFaceCountChange(    unsigned int new_face_count);
			void onLog(const std::string& msg, kappa::Log::SeverityLevel level);
		
		private:
			//____________________ Helpers
			using ::kappa::LogPublisher::log;
			void log(const std::string& msg, kappa::Log::SeverityLevel level); // from LogSubscriber
			static void* processingThread(void* instance);
			static void* displayThread(   void* instance);
			void processing();
			// Handlers for subscriptions
			void scanHandler( const sensor_msgs::PointCloud2::ConstPtr& msg);
			void clearHandler(const std_msgs::Empty::ConstPtr& msg);
			void soundSourceLocalizationEstimateHandler(const hark_msgs::HarkSource::ConstPtr& msg);
			
			//____________________ Private fields
			kappa::PPM      _ppm; ///< Wrapped PPM object
			geometry_msgs::Pose _latest_visual_ego_localization;
			// Threads
			pthread_t       _processing_thread; ///< Thread for PPM operations
			pthread_t       _display_thread;    ///< Thread for PPM display
			// ROS interface
			ros::NodeHandle _ros_node;          ///< Interface object to ROS
			// Subscriptions
			ros::Subscriber _scan_subscription;        ///< ROS subscription for incoming scans
			ros::Subscriber _clear_subscription;       ///< ROS subscription for clear messages
			ros::Subscriber _sound_source_localization_estimate_subscription; ///< ROS subscription for sound source localization estimates
			tf::TransformListener _transform_listener; ///< 
			// Publications
			ros::Publisher  _vertex_count_publisher;   ///< ROS publisher for number vertices
			ros::Publisher  _halfedge_count_publisher; ///< ROS publisher for number edges
			ros::Publisher  _face_count_publisher;     ///< ROS publisher for number faces
			// GUI
			kappa::CommandCenter _command_center; ///< GUI for visualizing PPM
	};
}

#endif

/******************************************************************************/


cmake_minimum_required(VERSION 2.4.6)
include($ENV{ROS_ROOT}/core/rosbuild/rosbuild.cmake)

# Set the build type.  Options are:
#  Coverage       : w/ debug symbols, w/o optimization, w/ code-coverage
#  Debug          : w/ debug symbols, w/o optimization
#  Release        : w/o debug symbols, w/ optimization
#  RelWithDebInfo : w/ debug symbols, w/ optimization
#  MinSizeRel     : w/o debug symbols, w/ optimization, stripped binaries
set(ROS_BUILD_TYPE Release)

rosbuild_init()

set(CMAKE_CXX_COMPILER colorgcc)
add_definitions(-std=c++03)
add_definitions(-Wall -Wno-deprecated)
add_definitions(-frounding-math)
#set the default path for built executables to the "bin" directory
set(EXECUTABLE_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/bin)
#set the default path for built libraries to the "lib" directory
set(LIBRARY_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/lib)

#uncomment if you have defined messages
#rosbuild_genmsg()
#uncomment if you have defined services
#rosbuild_gensrv()

#common commands for building c++ executables and libraries
#rosbuild_add_library(${PROJECT_NAME} src/example.cpp)
#target_link_libraries(${PROJECT_NAME} another_library)
#rosbuild_add_boost_directories()
#rosbuild_link_boost(${PROJECT_NAME} thread)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/CMake finders")

# Call the GTKmm finder, and include the header and library directories found
find_package(PkgConfig)
pkg_check_modules(GTKMM gtkmm-2.4)
include_directories(${GTKMM_INCLUDE_DIRS})
link_directories(${GTKMM_LIBRARY_DIRS})
# Add include and library directories for osgGtk
find_package(osgGtk REQUIRED)
include_directories(${osgGtk_INCLUDE_DIRS})
link_directories(${osgGtk_LIBRARY_DIR})
# Add include and library directories for OpenSceneGraph
find_package(OpenSceneGraph REQUIRED)
include_directories(${OpenSceneGraph_INCLUDE_DIRS})
link_directories(${OpenSceneGraph_LIBRARY_DIR})
# Call the Threads finder, and include the header and library directories found
find_package(Threads REQUIRED)
include_directories(${Threads_INCLUDE_DIRS})
link_directories(${Threads_LIBRARY_DIRS})
# Include project headers
find_package(Kappa REQUIRED)
include_directories(${Kappa_INCLUDE_DIRS})
link_directories(${Kappa_LIBRARY_DIRS})

set(CMAKE_VERBOSE_MAKEFILE  ON)
# Compile the PPM module's ROS adapter into a library
rosbuild_add_library( ppm_adapter src/ppm_ros_adapter.cpp)
target_link_libraries(ppm_adapter kappa-audition
                                  kappa-command-center
                                  kappa-ppm
                                  kappa-tools
                                  pthread
                                  osgGtkmm
                                  ${OpenSceneGraph_LIBRARIES})

#rosbuild_add_compile_flags(ppm_adapter
#                           -DCGAL_CFG_NO_CPP0X_TUPLE  #| Necessary to prevent clashes 
#                           -DCGAL_CFG_NO_TR1_TUPLE    #| between boost, TR1 and gcc 
#                           -DCGAL_CFG_NO_CPP0X_ARRAY  #| implementations of C++0x
#                           -DCGAL_CFG_NO_TR1_ARRAY    #| 
#                           )

# Compile the PPM ros node and link with the adapter library
find_package(CGAL QUIET COMPONENTS Core)
if ( CGAL_FOUND )
	include( ${CGAL_USE_FILE} )
	rosbuild_add_executable(ppm_node src/ppm_node.cpp)
	target_link_libraries(ppm_node kappa-ppm
		                           ppm_adapter
                                   osg)
#	rosbuild_add_compile_flags(ppm_node
#		                       -DCGAL_CFG_NO_CPP0X_TUPLE  #| Necessary to prevent clashes 
#		                       -DCGAL_CFG_NO_TR1_TUPLE    #| between boost, TR1 and gcc 
#		                       -DCGAL_CFG_NO_CPP0X_ARRAY  #| implementations of C++0x
#		                       -DCGAL_CFG_NO_TR1_ARRAY    #| 
#		                       )
else()
    message(STATUS "This program requires the CGAL library, and will not be compiled.")
endif()

